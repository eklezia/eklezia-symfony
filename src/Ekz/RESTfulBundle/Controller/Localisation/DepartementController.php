<?php

namespace Ekz\RESTfulBundle\Controller\Localisation;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class DepartementController extends Controller {

    public function datatableAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $oRepository = $this->getDoctrine()->getRepository('EkzLocalisationBundle:Departement');

            $aEntity = [];
            foreach ($oRepository->findByDatatable($request) as $entity) {
                $aEntity[] = [
                    'id' => $entity->getId(),
                    'nom' => $entity->getNom(),
                    'region' => $entity->getRegion()->getNom(),
                    'pays' => $entity->getRegion()->getPays()->getNom()
                ];
            }
            
            return new JsonResponse([
                "draw" => $request->get('draw'),
                "recordsTotal" => $oRepository->count(),
                "recordsFiltered" => $oRepository->countByDatatable($request),
                "data" => $aEntity
            ]);
        }
        throw $this->createNotFoundException("Impossible de charger les départements");
    }
}
