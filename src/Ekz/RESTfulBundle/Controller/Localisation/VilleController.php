<?php

namespace Ekz\RESTfulBundle\Controller\Localisation;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class VilleController extends Controller {

    public function termAction($term) {
        $oRepository = $this->getDoctrine()->getRepository('EkzLocalisationBundle:Ville');
        $aVille = $oRepository->findByTerm($term);
        return new JsonResponse($aVille);
    }

    public function datatableAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $oRepository = $this->getDoctrine()->getRepository('EkzLocalisationBundle:Ville');

            $aEntity = [];
            foreach ($oRepository->findByDatatable($request) as $entity) {
                $aEntity[] = [
                    'id' => $entity->getId(),
                    'nom' => $entity->getNom(),
                    'departement' => $entity->getDepartement()->getNom(),
                    'region' => $entity->getRegion()->getNom(),
                ];
            }
            
            return new JsonResponse([
                "draw" => $request->get('draw'),
                "recordsTotal" => $oRepository->count(),
                "recordsFiltered" => $oRepository->countByDatatable($request),
                "data" => $aEntity
            ]);
        }
        throw $this->createNotFoundException("Impossible de charger les villes");
    }
}
