<?php

namespace Ekz\RESTfulBundle\Controller\Localisation;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class LocalisationController extends Controller {

    public function termAction($term) {
        $oVilleRepository = $this->getDoctrine()->getRepository('EkzLocalisationBundle:Ville');
        $oDepartementRepository = $this->getDoctrine()->getRepository('EkzLocalisationBundle:Departement');
        $oRegionRepository = $this->getDoctrine()->getRepository('EkzLocalisationBundle:Region');

        $aVille = $oVilleRepository->findByTerm($term);
        $aDepartement = $oDepartementRepository->findByTerm($term);
        $aRegion = $oRegionRepository->findByTerm($term);

        return new JsonResponse([
            'villes' => $aVille,
            'departements' => $aDepartement,
            'regions' => $aRegion
        ]);
    }

}
