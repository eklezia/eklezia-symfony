<?php

namespace Ekz\RESTfulBundle\Controller\Maintenance;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class LogController extends Controller {

    public function datatableAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $mode = $this->container->get('kernel')->getEnvironment();
            $path = $this->get('kernel')->getRootDir() . '/logs/' . $mode . '.log';

            $iLineTotal = 0;
            $iLineDisplayed = 0;
            $iLineFiltered = 0;
            $aLog = [];

            if (file_exists($path)) {
                foreach (array_reverse(file($path)) as $line) {
                    $log = strpos($line, '{') ? trim(substr($line, 0, strpos($line, '{'))) : trim($line);

                    preg_match_all('/^\[(?<date>[0-9 \-\:]{19})\](?<type>[^:]*): (?<message>.*)/', $log, $aMatch);

                    $date = $aMatch['date'][0];
                    $type = $aMatch['type'][0];
                    $message = $aMatch['message'][0];

                    if (empty($request->request->get('message')) || strpos($message, $request->request->get('message'))) {
                        if (empty($request->request->get('type')) || strpos($type, $request->request->get('type'))) {
                            if (true && $iLineDisplayed < $request->request->get('length')) {
                                $iLineDisplayed++;

                                $aLog[] = [
                                    'date' => $date,
                                    'type' => $type,
                                    'message' => $message,
                                ];
                            }
                            $iLineFiltered++;
                        }
                    }
                    $iLineTotal++;
                }
            }

            return new JsonResponse([
                "draw" => $request->get('draw'),
                "recordsTotal" => $iLineTotal,
                "recordsFiltered" => $iLineFiltered,
                "data" => $aLog
            ]);
        }
        throw $this->createNotFoundException("Impossible de charger les logs");
    }

}
