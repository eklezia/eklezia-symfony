<?php

namespace Ekz\RESTfulBundle\Controller\Utilisateur;

use AppBundle\Twig\Functions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller {

    public function indexAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $oRepository = $this->getDoctrine()->getRepository('EkzUtilisateurBundle:Utilisateur');

            $aEntity = [];
            foreach ($oRepository->findByDatatable($request) as $entity) {
                
                $villePrincipal = null !== $entity->getVillePrincipal()
                        ? $entity->getVillePrincipal()->getNom()
                        : null;
                
                $region = null !== $entity->getVillePrincipal()
                        ? $entity->getVillePrincipal()->getRegion()->getNom()
                        : null;
                
                $aEntity[] = [
                    'id' => $entity->getId(),
                    'nom' => $entity->getNom(),
                    'prenom' => $entity->getPrenom(),
                    'villePrincipal' => $villePrincipal,
                    'region' => $region,
                    'role' => Functions::userRole($entity),
                    'lastActivity' => $entity->getLastActivity()->format('d/m/Y H:i:s'),
                ];
            }
            
            return new JsonResponse([
                "draw" => $request->get('draw'),
                "recordsTotal" => $oRepository->count(),
                "recordsFiltered" => $oRepository->countByDatatable($request),
                "data" => $aEntity
            ]);
        }
        throw $this->createNotFoundException("Impossible de charger les utilisateurs");
    }
}
