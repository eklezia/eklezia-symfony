<?php

namespace Ekz\RESTfulBundle\Controller\Projet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FavorisController extends Controller {

    public function indexAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
            $oProjet = $oProjetRepository->findOneBySlug($request->get('slug'));

            $oUser = $this->getUser();

            $bInFavoris = null;
            if (!$oUser->getProjetsEnFavoris()->contains($oProjet)) {
                $oUser->addProjetEnFavoris($oProjet);
                $bInFavoris = true;
            } else {
                $oUser->removeProjetEnFavoris($oProjet);
                $bInFavoris = false;
            }

            $this->getDoctrine()->getManager()->persist($oUser);
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse([
                'favoris' => $bInFavoris
            ]);
        }
        throw $this->createNotFoundException("Impossible d'ajouter ou de supprimer ce projet");
    }

}
