<?php

namespace Ekz\RESTfulBundle\Controller\Projet;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListController extends Controller {

    public function indexAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $oRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');

            $aEntity = [];
            foreach ($oRepository->findByDatatable($request) as $entity) {
                $aEntity[] = [
                    'id' => $entity->getId(),
                    'titre' => $entity->getTitre(),
                    'thematique' => $entity->getThematique()->getNom(),
                    'createur' => $entity->getCreateur()->getPublicName(),
                    'etat' => $this->get('translator')->trans($entity->getEtat()),
                    'date_butoire' => $entity->getDateButoire()->format('d/m/Y H:i:s'),
                    'localisation' => (string) $entity->getLocalisation(),
                    'votes_pour' => count($entity->getVotesPour()),
                    'votes_contre' => count($entity->getVotesContre()),
                    'commentaires' => count($entity->getCommentaires()),
                    'date_publication' => $entity->getDatePublication()->format('d/m/Y H:i:s'),
                ];
            }
            
            return new JsonResponse([
                "draw" => $request->get('draw'),
                "recordsTotal" => $oRepository->count(),
                "recordsFiltered" => $oRepository->countByDatatable($request),
                "data" => $aEntity
            ]);
        }
        throw $this->createNotFoundException("Impossible de charger les projets");
    }
    
    public function aModererAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            $oRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
            
            $aWhere = [
                ['p.isModerated = 0']
            ];
            
            $aEntity = [];
            foreach ($oRepository->findByDatatable($request, $aWhere) as $entity) {
                $aEntity[] = [
                    'id' => $entity->getId(),
                    'titre' => $entity->getTitre(),
                    'thematique' => $entity->getThematique()->getNom(),
                    'createur' => $entity->getCreateur()->getPublicName(),
                    'etat' => $this->get('translator')->trans($entity->getEtat()),
                    'date_butoire' => $entity->getDateButoire()->format('d/m/Y H:i:s'),
                    'localisation' => (string) $entity->getLocalisation(),
                    'votes_pour' => count($entity->getVotesPour()),
                    'votes_contre' => count($entity->getVotesContre()),
                    'commentaires' => count($entity->getCommentaires()),
                    'date_publication' => $entity->getDatePublication()->format('d/m/Y H:i:s'),
                ];
            }
            
            return new JsonResponse([
                "draw" => $request->get('draw'),
                "recordsTotal" => $oRepository->count(),
                "recordsFiltered" => $oRepository->countByDatatable($request),
                "data" => $aEntity
            ]);
        }
        throw $this->createNotFoundException("Impossible de charger les projets");
    }
}
