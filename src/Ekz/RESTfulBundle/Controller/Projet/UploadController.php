<?php

namespace Ekz\RESTfulBundle\Controller\Projet;

use Ekz\ProjetBundle\Entity\Projet;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends Controller {

    public function imageUneAction(Request $request) {
        if ($request->isXmlHttpRequest()) {

            /* @var $file UploadedFile */
            $file = current($request->files->all());

            $oEntity = new Projet();
            $oEntity->setImage($file);

            $errors = $this->get('validator')->validate($oEntity, null, ['preview']);

            if (count($errors) == 0) {
                $this->get('upload.image.preview')->upload($file);

                $sName = utf8_encode($file->getClientOriginalName());
                
                return new JsonResponse([
                    'success' => true,
                    'path' => 'temp' . '/' . $this->getUser()->getId() . '/' . $sName,
                    'getClientMimeType' => $file->getClientMimeType(),
                    'getClientMimeType' => $file->getClientMimeType(),
                    'getClientOriginalExtension' => $file->getClientOriginalExtension(),
                    'getClientOriginalName' => $sName,
                    'getClientSize' => $file->getClientSize(),
                    'getError' => $file->getError(),
                    'getFileInfo' => $file->getFileInfo(),
                    'getMaxFilesize' => $file->getMaxFilesize(),
                    'getPath' => $file->getPath(),
                ]);
            }
            
            return new JsonResponse([
                'success' => false,
                'message' => $errors[0]->getMessage()
            ]);
        }
        throw $this->createNotFoundException("Impossible d'uploader une image");
    }

}
