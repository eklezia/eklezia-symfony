<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use AppBundle\Enum\Data\RoleEnum;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\UtilisateurBundle\Entity\Utilisateur;

/**
 * Description of UtilisateurFixture
 *
 * @author gwennael
 */
class UtilisateurFixture extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->getData() as $data) {
            $entity = new Utilisateur();

            $entity->setNom($data['nom']);
            $entity->setPrenom($data['prenom']);
            $entity->setPseudo($data['pseudo']);
            $entity->setEmail($data['email']);
            $entity->setPassword($data['password']);
            $entity->setSalt($data['salt']);
            $entity->setFacebookId($data['facebook_id']);
            $entity->setIsActif($data['is_actif']);
            $entity->setImage($data['image']);
            $entity->setVillePrincipal($data['ville']);
            $entity->setTokenInscription($data['token_inscription']);
            $entity->setRoles($data['roles']);

            $this->setReference('utilisateur-' . $entity->getNom(), $entity);

            $manager->persist($entity);
            $manager->flush();
        }
    }

    public function getOrder() {
        return OrderFixtures::UTILISATEUR;
    }

    private function getData() {
        return array_merge([
            $this->userGwennael(),
            $this->userAubin(),
            $this->userEdgard()
        ], $this->userDefault());
    }

    private function userGwennael() {
        return [
            'nom' => 'Jean',
            'prenom' => 'Gwennael',
            'pseudo' => null,
            'email' => 'gwennael.jean@gmail.com',
            'password' => 'U4EzKyyGn106zH2pFD5Qhsd4qZMC/ibSZydxS+/hL7pJ2FgcjBOefU9bzh9RP3/g94DWQ6yGU8oQSOFL6q+pTw==',
            'salt' => '16343762195696b452bfbff',
            'roles' => [RoleEnum::ROLE_SUPER_ADMIN],
            'facebook_id' => null,
            'is_actif' => true,
            'image' => '/img/ek-user.jpg',
            'ville' => $this->getReference("ville-Rouen"),
            'token_inscription' => null,
        ];
    }

    private function userAubin() {
        return [
            'nom' => 'Chauveau',
            'prenom' => 'Aubin',
            'pseudo' => null,
            'email' => 'aubin.chauveau.perso@gmail.com',
            'password' => 'S8vjmjclKkyUdxI49j8PKOejQ7Tex06SjpS3x4qG+e2r19yQBH2wpdG5xtg1LPtXV19M0Qpj96WzYTEwwclCmw==',
            'salt' => '2120911877569d54d83bcd0',
            'roles' => [RoleEnum::ROLE_ADMIN],
            'facebook_id' => null,
            'is_actif' => true,
            'image' => '/img/ek-user.jpg',
            'ville' => $this->getReference("ville-Paris"),
            'token_inscription' => null,
        ];
    }

    private function userEdgard() {
        return [
            'nom' => 'Chauveau',
            'prenom' => 'Edgard',
            'pseudo' => null,
            'email' => 'edgard.chauveau@gmail.com',
            'password' => 'OdX50sT/COy1gsV/WMjLKPXVrJp+F0sKYMxRmukijR4RKr4GEFAMf9g+EnQzuOakt4fEQVhFnjESwOalPEzLjQ==',
            'salt' => '1668199172569e96ee1f625',
            'roles' => [RoleEnum::ROLE_ADMIN],
            'facebook_id' => null,
            'is_actif' => true,
            'image' => '/img/ek-user.jpg',
            'ville' => $this->getReference("ville-Paris"),
            'token_inscription' => null,
        ];
    }

    private function userDefault() {
        return [
            [
                'nom' => 'Paris 1',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-paris-1@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Paris"),
                'token_inscription' => null,
            ], [
                'nom' => 'Paris 2',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-paris-2@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Paris"),
                'token_inscription' => null,
            ], [
                'nom' => 'Paris 3',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-paris-3@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Paris"),
                'token_inscription' => null,
            ], [
                'nom' => 'Rouen 1',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-rouen-1@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Rouen"),
                'token_inscription' => null,
            ], [
                'nom' => 'Rouen 2',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-rouen-2@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Rouen"),
                'token_inscription' => null,
            ], [
                'nom' => 'Iville 1',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-iville-1@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Iville"),
                'token_inscription' => null,
            ], [
                'nom' => 'Iville 2',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-iville-2@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Iville"),
                'token_inscription' => null,
            ], [
                'nom' => 'Elbeuf 1',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-elbeuf-1@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Elbeuf"),
                'token_inscription' => null,
            ], [
                'nom' => 'Elbeuf 2',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-elbeuf-2@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Elbeuf"),
                'token_inscription' => null,
            ], [
                'nom' => 'Nantes 1',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-nantes-1@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Nantes"),
                'token_inscription' => null,
            ], [
                'nom' => 'Nantes 2',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-nantes-2@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Nantes"),
                'token_inscription' => null,
            ], [
                'nom' => 'Bordeaux 1',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-bordeaux-1@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Bordeaux"),
                'token_inscription' => null,
            ], [
                'nom' => 'Bordeaux 2',
                'prenom' => 'utilisateur',
                'pseudo' => null,
                'email' => 'utilisateur-bordeaux-2@mail.test',
                'password' => 'RJGPloJNfZWypEMIROyUET44ToFUPjDtjm/mmrbGIYsxvj6zHo3VMjtxfSKjua+jmnsgvQwd9zVqmsQNHdwMlQ==',
                'salt' => '16343762195696b452bfbff',
                'roles' => [RoleEnum::ROLE_USER],
                'facebook_id' => null,
                'is_actif' => true,
                'image' => '/img/ek-user.jpg',
                'ville' => $this->getReference("ville-Bordeaux"),
                'token_inscription' => null,
            ]
        ];
    }

}
