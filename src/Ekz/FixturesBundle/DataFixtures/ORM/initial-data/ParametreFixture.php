<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\ParametreBundle\Entity\Parametre;

/**
 * Description of UtilisateurFixture
 *
 * @author gwennael
 */
class ParametreFixture extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->getData() as $data) {
            $entity = new Parametre();
            $entity->setNom($data['nom']);
            $entity->setType($data['type']);
            $entity->setValeur($data['valeur']);

            $manager->persist($entity);
            $manager->flush();
        }
    }

    public function getOrder() {
        return OrderFixtures::PARAMETRE;
    }

    private function getData() {
        return [
            [
                'nom' => 'blacklist_mot_regex',
                'type' => 'string',
                'valeur' => 'con|connard'
            ],
            [
                'nom' => 'projet_moderation',
                'type' => 'boolean',
                'valeur' => true
            ],
            [
                'nom' => 'projet_date_butoire_min',
                'type' => 'string',
                'valeur' => '+3 weeks'
            ]
        ];
    }

}
