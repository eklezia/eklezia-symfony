<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\InformationBundle\Entity\Page;

/**
 * Description of UtilisateurFixture
 *
 * @author gwennael
 */
class PageFixture extends AbstractFixture implements OrderedFixtureInterface {
    
    public function load(ObjectManager $manager) {
        foreach ($this->getData() as $data) {
            $entity = new Page();
            $entity->setTitre($data['titre']);
            $entity->setSlug($data['slug']);
            $entity->setConnexion($data['connexion']);
            $entity->setContenu($data['contenu']);

            $manager->persist($entity);
            $manager->flush();
        }
    }

    public function getOrder() {
        return OrderFixtures::PAGE;
    }

    private function getData() {
        return [
            [
                'titre' => 'FAQ',
                'slug' => 'faq',
                'connexion' => false,
                'contenu' => '<h1>FAQ</h1>'
            ],
            [
                'titre' => 'Nous connaître',
                'slug' => 'nous-connaitre',
                'connexion' => false,
                'contenu' => '<h1>Nous connaître</h1>'
            ],
            [
                'titre' => 'Protection des données et CGU',
                'slug' => 'protection-des-donnees-et-cgu',
                'connexion' => false,
                'contenu' => '<h1>Protection des données et CGU</h1>'
            ],
            [
                'titre' => 'Développeurs',
                'slug' => 'developpeurs',
                'connexion' => false,
                'contenu' => '<h1>Développeurs</h1>'
            ],
            [
                'titre' => 'Améliorer la visibilité d\'un projet',
                'slug' => 'ameliorer-la-lisibilite-d-un-projet',
                'connexion' => false,
                'contenu' => '<h1>Améliorer la visibilité d\'un projet</h1>'
            ]
        ];
    }
}
