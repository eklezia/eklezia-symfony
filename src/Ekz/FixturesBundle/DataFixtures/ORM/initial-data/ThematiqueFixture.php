<?php

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\ProjetBundle\Entity\Thematique;

class ThematiqueFixture extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->getData() as $data) {
            $entity = new Thematique();
            
            $entity->setNom($data['nom']);
            
            $manager->persist($entity);
            $manager->flush();
        }
    }

    public function getOrder() {
        return OrderFixtures::THEMATIQUE;
    }

    private function getData() {
        return [
            ['nom' => 'Politique sociale / Santé'],
            ['nom' => 'Justice / Sécurité'],
            ['nom' => 'Économie / Finance / Consommation'],
            ['nom' => 'Entreprise / Tourisme / Emploi'],
            ['nom' => 'Éducation/ Jeunesse'],
            ['nom' => 'Culture/ Sport / Vie associative'],
            ['nom' => 'Transport / Voiries / Logement'],
            ['nom' => 'Développement durable / Énergie'],
        ];
    }

}
