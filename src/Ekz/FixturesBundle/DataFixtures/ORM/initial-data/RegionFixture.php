<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\LocalisationBundle\Entity\Region;

/**
 * Description of UtilisateurFixture
 *
 * @author gwennael
 */
class RegionFixture extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->getData() as $data) {
            $entity = new Region();
            $entity->setNom($data['nom']);
            $entity->setPays($this->getReference('pays-' . $data['pays']));

            $this->setReference('region-' . $entity->getNom(), $entity);

            $manager->persist($entity);
            $manager->flush();
        }
    }

    public function getOrder() {
        return OrderFixtures::REGION;
    }

    private function getData() {
        return [
            ['pays' => "France", 'nom' => "Alsace"],
            ['pays' => "France", 'nom' => "Aquitaine"],
            ['pays' => "France", 'nom' => "Auvergne"],
            ['pays' => "France", 'nom' => "Basse-Normandie"],
            ['pays' => "France", 'nom' => "Bourgogne"],
            ['pays' => "France", 'nom' => "Bretagne"],
            ['pays' => "France", 'nom' => "Centre"],
            ['pays' => "France", 'nom' => "Champagne-Ardenne"],
            ['pays' => "France", 'nom' => "Corse"],
            ['pays' => "France", 'nom' => "Franche-Comté"],
            ['pays' => "France", 'nom' => "Haute-Normandie"],
            ['pays' => "France", 'nom' => "Île-de-France"],
            ['pays' => "France", 'nom' => "Languedoc-Roussillon"],
            ['pays' => "France", 'nom' => "Limousin"],
            ['pays' => "France", 'nom' => "Lorraine"],
            ['pays' => "France", 'nom' => "Midi-Pyrénées"],
            ['pays' => "France", 'nom' => "Nord-Pas-de-Calais"],
            ['pays' => "France", 'nom' => "Pays de la Loire"],
            ['pays' => "France", 'nom' => "Picardie"],
            ['pays' => "France", 'nom' => "Poitou-Charentes"],
            ['pays' => "France", 'nom' => "Provence-Alpes-Côte d'Azur"],
            ['pays' => "France", 'nom' => "Rhône-Alpes"],
            ['pays' => "France", 'nom' => "Guadeloupe"],
            ['pays' => "France", 'nom' => "Guyane"],
            ['pays' => "France", 'nom' => "La Réunion"],
            ['pays' => "France", 'nom' => "Martinique"],
            ['pays' => "France", 'nom' => "Mayotte"],
        ];
    }

}
