<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\LocalisationBundle\Entity\Ville;

/**
 * Description of UtilisateurFixture
 *
 * @author gwennael
 */
class VilleFixture extends AbstractFixture implements OrderedFixtureInterface {

    const COL_NOM = 5;
    const COL_SLUG = 2;
    const COL_LATITUDE = 20;
    const COL_LONGITUDE = 19;
    const COL_DEPARTEMENT = 1;
    const COL_CODE_POSTAL = 8;
    
    public function load(ObjectManager $manager) {
        $file = dirname(dirname(dirname(dirname(dirname(dirname(__DIR__)))))) . '/app/Resources/data/villes.csv';
        $oHandleVille = fopen($file, 'r');
        
        $i = 0;
        while (($data = fgetcsv($oHandleVille)) !== false) {
            $i++;
            $entity = new Ville();
            $entity->setNom($data[self::COL_NOM]);
            $entity->setCodePostal($data[self::COL_CODE_POSTAL]);
            $entity->setSlug($data[self::COL_SLUG]);
            $entity->setLatitude($data[self::COL_LATITUDE]);
            $entity->setLongitude($data[self::COL_LONGITUDE]);
            $entity->setDepartement($this->getReference("departement-{$data[self::COL_DEPARTEMENT]}"));

            $this->setReference('ville-' . $entity->getNom(), $entity);
            
            $manager->persist($entity);
            
            if ($i % 25 == 0) {
                echo "        > Import de {$i} villes\r";
                $manager->flush();
                $manager->clear();
            }
        }
        $manager->flush();
        echo "\n";
    }

    public function getOrder() {
        return OrderFixtures::VILLE;
    }

}
