<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\LocalisationBundle\Entity\Pays;

/**
 * Description of UtilisateurFixture
 *
 * @author gwennael
 */
class PaysFixture extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->getData() as $data) {
            $entity = new Pays();
            $entity->setNom($data['nom']);

            $this->setReference('pays-' . $entity->getNom(), $entity);

            $manager->persist($entity);
            $manager->flush();
        }
    }

    public function getOrder() {
        return OrderFixtures::PAYS;
    }

    private function getData() {
        return [
            ['nom' => 'France'],
        ];
    }

}
