<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FixturesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ekz\FixturesBundle\OrderFixtures\OrderFixtures;
use Ekz\LocalisationBundle\Entity\Departement;

/**
 * Description of UtilisateurFixture
 *
 * @author gwennael
 */
class DepartementFixture extends AbstractFixture implements OrderedFixtureInterface {

    public function load(ObjectManager $manager) {
        foreach ($this->getData() as $data) {
            $entity = new Departement();
            $entity->setNom($data['nom']);
            $entity->setCode($data['code']);
            $entity->setRegion($this->getReference('region-' . $data['region']));

            $this->setReference("departement-{$data['code']}", $entity);

            $manager->persist($entity);
            $manager->flush();
        }
    }

    public function getOrder() {
        return OrderFixtures::DEPARTEMENT;
    }

    private function getData() {
        return [
            [
                'nom' => "Ain",
                'code' => 1,
                'prefecture' => "Bourg-en-Bresse",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Aisne",
                'code' => 2,
                'prefecture' => "Laon",
                'region' => "Picardie"
            ],
            [
                'nom' => "Allier",
                'code' => 3,
                'prefecture' => "Moulins",
                'region' => "Auvergne"
            ],
            [
                'nom' => "Alpes de Hautes-Provence",
                'code' => 4,
                'prefecture' => "Digne",
                'region' => "Provence-Alpes-Côte d'Azur"
            ],
            [
                'nom' => "Hautes-Alpes",
                'code' => 5,
                'prefecture' => "Gap",
                'region' => "Provence-Alpes-Côte d'Azur"
            ],
            [
                'nom' => "Alpes-Maritimes",
                'code' => 6,
                'prefecture' => "Nice",
                'region' => "Provence-Alpes-Côte d'Azur"
            ],
            [
                'nom' => "Ardèche",
                'code' => 7,
                'prefecture' => "Privas",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Ardennes",
                'code' => 8,
                'prefecture' => "Charleville-Mézières",
                'region' => "Champagne-Ardenne"
            ],
            [
                'nom' => "Ariège",
                'code' => 9,
                'prefecture' => "Foix",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Aube",
                'code' => 10,
                'prefecture' => "Troyes",
                'region' => "Champagne-Ardenne"
            ],
            [
                'nom' => "Aude",
                'code' => 11,
                'prefecture' => "Carcassonne",
                'region' => "Languedoc-Roussillon"
            ],
            [
                'nom' => "Aveyron",
                'code' => 12,
                'prefecture' => "Rodez",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Bouches-du-Rhône",
                'code' => 13,
                'prefecture' => "Marseille",
                'region' => "Provence-Alpes-Côte d'Azur"
            ],
            [
                'nom' => "Calvados",
                'code' => 14,
                'prefecture' => "Caen",
                'region' => "Basse-Normandie"
            ],
            [
                'nom' => "Cantal",
                'code' => 15,
                'prefecture' => "Aurillac",
                'region' => "Auvergne"
            ],
            [
                'nom' => "Charente",
                'code' => 16,
                'prefecture' => "Angoulême",
                'region' => "Poitou-Charentes"
            ],
            [
                'nom' => "Charente-Maritime",
                'code' => 17,
                'prefecture' => "La Rochelle",
                'region' => "Poitou-Charentes"
            ],
            [
                'nom' => "Cher",
                'code' => 18,
                'prefecture' => "Bourges",
                'region' => "Centre"
            ],
            [
                'nom' => "Corrèze",
                'code' => 19,
                'prefecture' => "Tulle",
                'region' => "Limousin"
            ],
            [
                'nom' => "Corse-du-Sud",
                'code' => '2A',
                'prefecture' => "Ajaccio",
                'region' => "Corse"
            ],
            [
                'nom' => "Haute-Corse",
                'code' => '2B',
                'prefecture' => "Bastia",
                'region' => "Corse"
            ],
            [
                'nom' => "Côte-d'Or",
                'code' => 21,
                'prefecture' => "Dijon",
                'region' => "Bourgogne"
            ],
            [
                'nom' => "Côtes d'Armor",
                'code' => 22,
                'prefecture' => "Saint-Brieuc",
                'region' => "Bretagne"
            ],
            [
                'nom' => "Creuse",
                'code' => 23,
                'prefecture' => "Guéret",
                'region' => "Limousin"
            ],
            [
                'nom' => "Dordogne",
                'code' => 24,
                'prefecture' => "Périgueux",
                'region' => "Aquitaine"
            ],
            [
                'nom' => "Doubs",
                'code' => 25,
                'prefecture' => "Besançon",
                'region' => "Franche-Comté"
            ],
            [
                'nom' => "Drôme",
                'code' => 26,
                'prefecture' => "Valence",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Eure",
                'code' => 27,
                'prefecture' => "Évreux",
                'region' => "Haute-Normandie"
            ],
            [
                'nom' => "Eure-et-Loir",
                'code' => 28,
                'prefecture' => "Chartres",
                'region' => "Centre"
            ],
            [
                'nom' => "Finistère",
                'code' => 29,
                'prefecture' => "Quimper",
                'region' => "Bretagne"
            ],
            [
                'nom' => "Gard",
                'code' => 30,
                'prefecture' => "Nîmes",
                'region' => "Languedoc-Roussillon"
            ],
            [
                'nom' => "Haute-Garonne",
                'code' => 31,
                'prefecture' => "Toulouse",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Gers",
                'code' => 32,
                'prefecture' => "Auch",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Gironde",
                'code' => 33,
                'prefecture' => "Bordeaux",
                'region' => "Aquitaine"
            ],
            [
                'nom' => "Hérault",
                'code' => 34,
                'prefecture' => "Montpellier",
                'region' => "Languedoc-Roussillon"
            ],
            [
                'nom' => "Ille-et-Vilaine",
                'code' => 35,
                'prefecture' => "Rennes",
                'region' => "Bretagne"
            ],
            [
                'nom' => "Indre",
                'code' => 36,
                'prefecture' => "Châteauroux",
                'region' => "Centre"
            ],
            [
                'nom' => "Indre-et-Loire",
                'code' => 37,
                'prefecture' => "Tours",
                'region' => "Centre"
            ],
            [
                'nom' => "Isère",
                'code' => 38,
                'prefecture' => "Grenoble",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Jura",
                'code' => 39,
                'prefecture' => "Lons-le-Saunier",
                'region' => "Franche-Comté"
            ],
            [
                'nom' => "Landes",
                'code' => 40,
                'prefecture' => "Mont-de-Marsan",
                'region' => "Aquitaine"
            ],
            [
                'nom' => "Loir-et-Cher",
                'code' => 41,
                'prefecture' => "Blois",
                'region' => "Centre"
            ],
            [
                'nom' => "Loire",
                'code' => 42,
                'prefecture' => "Saint-Étienne",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Haute-Loire",
                'code' => 43,
                'prefecture' => "Le Puy-en-Velay",
                'region' => "Auvergne"
            ],
            [
                'nom' => "Loire-Atlantique",
                'code' => 44,
                'prefecture' => "Nantes",
                'region' => "Pays de la Loire"
            ],
            [
                'nom' => "Loiret",
                'code' => 45,
                'prefecture' => "Orléans",
                'region' => "Centre"
            ],
            [
                'nom' => "Lot",
                'code' => 46,
                'prefecture' => "Cahors",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Lot-et-Garonne",
                'code' => 47,
                'prefecture' => "Agen",
                'region' => "Aquitaine"
            ],
            [
                'nom' => "Lozère",
                'code' => 48,
                'prefecture' => "Mende",
                'region' => "Languedoc-Roussillon"
            ],
            [
                'nom' => "Maine-et-Loire",
                'code' => 49,
                'prefecture' => "Angers",
                'region' => "Pays de la Loire"
            ],
            [
                'nom' => "Manche",
                'code' => 50,
                'prefecture' => "Saint-Lô",
                'region' => "Basse-Normandie"
            ],
            [
                'nom' => "Marne",
                'code' => 51,
                'prefecture' => "Châlons-en-Champagne",
                'region' => "Champagne-Ardenne"
            ],
            [
                'nom' => "Haute-Marne",
                'code' => 52,
                'prefecture' => "Chaumont",
                'region' => "Champagne-Ardenne"
            ],
            [
                'nom' => "Mayenne",
                'code' => 53,
                'prefecture' => "Laval",
                'region' => "Pays de la Loire"
            ],
            [
                'nom' => "Meurthe-et-Moselle",
                'code' => 54,
                'prefecture' => "Nancy",
                'region' => "Lorraine"
            ],
            [
                'nom' => "Meuse",
                'code' => 55,
                'prefecture' => "Bar-le-Duc",
                'region' => "Lorraine"
            ],
            [
                'nom' => "Morbihan",
                'code' => 56,
                'prefecture' => "Vannes",
                'region' => "Bretagne"
            ],
            [
                'nom' => "Moselle",
                'code' => 57,
                'prefecture' => "Metz",
                'region' => "Lorraine"
            ],
            [
                'nom' => "Nièvre",
                'code' => 58,
                'prefecture' => "Nevers",
                'region' => "Bourgogne"
            ],
            [
                'nom' => "Nord",
                'code' => 59,
                'prefecture' => "Lille",
                'region' => "Nord-Pas-de-Calais"
            ],
            [
                'nom' => "Oise",
                'code' => 60,
                'prefecture' => "Beauvais",
                'region' => "Picardie"
            ],
            [
                'nom' => "Orne",
                'code' => 61,
                'prefecture' => "Alençon",
                'region' => "Basse-Normandie"
            ],
            [
                'nom' => "Pas-de-Calais",
                'code' => 62,
                'prefecture' => "Arras",
                'region' => "Nord-Pas-de-Calais"
            ],
            [
                'nom' => "Puy-de-Dôme",
                'code' => 63,
                'prefecture' => "Clermont-Ferrand",
                'region' => "Auvergne"
            ],
            [
                'nom' => "Pyrénées-Atlantiques",
                'code' => 64,
                'prefecture' => "Pau",
                'region' => "Aquitaine"
            ],
            [
                'nom' => "Hautes-Pyrénées",
                'code' => 65,
                'prefecture' => "Tarbes",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Pyrénées-Orientales",
                'code' => 66,
                'prefecture' => "Perpignan",
                'region' => "Languedoc-Roussillon"
            ],
            [
                'nom' => "Bas-Rhin",
                'code' => 67,
                'prefecture' => "Strasbourg",
                'region' => "Alsace"
            ],
            [
                'nom' => "Haut-Rhin",
                'code' => 68,
                'prefecture' => "Colmar",
                'region' => "Alsace"
            ],
            [
                'nom' => "Rhône",
                'code' => 69,
                'prefecture' => "Lyon",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Haute-Saône",
                'code' => 70,
                'prefecture' => "Vesoul",
                'region' => "Franche-Comté"
            ],
            [
                'nom' => "Saône-et-Loire",
                'code' => 71,
                'prefecture' => "Mâcon",
                'region' => "Bourgogne"
            ],
            [
                'nom' => "Sarthe",
                'code' => 72,
                'prefecture' => "Le Mans",
                'region' => "Pays de la Loire"
            ],
            [
                'nom' => "Savoie",
                'code' => 73,
                'prefecture' => "Chambéry",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Haute-Savoie",
                'code' => 74,
                'prefecture' => "Annecy",
                'region' => "Rhône-Alpes"
            ],
            [
                'nom' => "Paris (Département)",
                'code' => 75,
                'prefecture' => "Paris",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Seine-Maritime",
                'code' => 76,
                'prefecture' => "Rouen",
                'region' => "Haute-Normandie"
            ],
            [
                'nom' => "Seine-et-Marne",
                'code' => 77,
                'prefecture' => "Melun",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Yvelines",
                'code' => 78,
                'prefecture' => "Versailles",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Deux-Sèvres",
                'code' => 79,
                'prefecture' => "Niort",
                'region' => "Poitou-Charentes"
            ],
            [
                'nom' => "Somme",
                'code' => 80,
                'prefecture' => "Amiens",
                'region' => "Picardie"
            ],
            [
                'nom' => "Tarn",
                'code' => 81,
                'prefecture' => "Albi",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Tarn-et-Garonne",
                'code' => 82,
                'prefecture' => "Montauban",
                'region' => "Midi-Pyrénées"
            ],
            [
                'nom' => "Var",
                'code' => 83,
                'prefecture' => "Toulon",
                'region' => "Provence-Alpes-Côte d'Azur"
            ],
            [
                'nom' => "Vaucluse",
                'code' => 84,
                'prefecture' => "Avignon",
                'region' => "Provence-Alpes-Côte d'Azur"
            ],
            [
                'nom' => "Vendée",
                'code' => 85,
                'prefecture' => "La Roche-sur-Yon",
                'region' => "Pays de la Loire"
            ],
            [
                'nom' => "Vienne",
                'code' => 86,
                'prefecture' => "Poitiers",
                'region' => "Poitou-Charentes"
            ],
            [
                'nom' => "Haute-Vienne",
                'code' => 87,
                'prefecture' => "Limoges",
                'region' => "Limousin"
            ],
            [
                'nom' => "Vosges",
                'code' => 88,
                'prefecture' => "Épinal",
                'region' => "Lorraine"
            ],
            [
                'nom' => "Yonne",
                'code' => 89,
                'prefecture' => "Auxerre",
                'region' => "Bourgogne"
            ],
            [
                'nom' => "Territoire-de-Belfort",
                'code' => 90,
                'prefecture' => "Belfort",
                'region' => "Franche-Comté"
            ],
            [
                'nom' => "Essonne",
                'code' => 91,
                'prefecture' => "Évry",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Hauts-de-Seine",
                'code' => 92,
                'prefecture' => "Nanterre",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Seine-Saint-Denis",
                'code' => 93,
                'prefecture' => "Bobigny",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Val-de-Marne",
                'code' => 94,
                'prefecture' => "Créteil",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Val-d'Oise",
                'code' => 95,
                'prefecture' => "Pontoise",
                'region' => "Île-de-France"
            ],
            [
                'nom' => "Guadeloupe",
                'code' => 971,
                'prefecture' => "Pointe-à-Pitre",
                'region' => "Guadeloupe"
            ],
            [
                'nom' => "Martinique",
                'code' => 972,
                'prefecture' => "Fort-de-France",
                'region' => "Martinique"
            ],
            [
                'nom' => "La Réunion",
                'code' => 973,
                'prefecture' => "Cayenne",
                'region' => "La Réunion"
            ],
            [
                'nom' => "Martinique",
                'code' => 974,
                'prefecture' => "Saint-Denis",
                'region' => "Martinique"
            ],
            [
                'nom' => "Mayotte",
                'code' => 976,
                'prefecture' => "Mamoudzou",
                'region' => "Mayotte"
            ]
        ];
    }

}
