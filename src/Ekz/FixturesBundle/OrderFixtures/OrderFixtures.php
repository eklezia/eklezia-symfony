<?php

namespace Ekz\FixturesBundle\OrderFixtures;

abstract class OrderFixtures {

    const PAYS = 1;
    const REGION = 2;
    const DEPARTEMENT = 3;
    const VILLE = 4;
    const THEMATIQUE = 5;
    const UTILISATEUR = 6;
    const PAGE = 7;
    const PARAMETRE = 8;

}
