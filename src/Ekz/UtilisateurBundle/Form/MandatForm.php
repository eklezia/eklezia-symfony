<?php

namespace Ekz\UtilisateurBundle\Form;

use AppBundle\Enum\Data\RoleEnum;
use Doctrine\ORM\EntityManager;
use Ekz\LocalisationBundle\Form\Type\ZoneGeographiqueType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MandatForm extends AbstractType {

    private $manager;

    public function __construct(EntityManager $manager) {
        $this->manager = $manager;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titres', 'choice', [
                    'expanded' => true,
                    'multiple' => true,
                    'choices' => [
                        RoleEnum::ROLE_CONSEILLER_MUNICIPAL => 'role.' . RoleEnum::ROLE_CONSEILLER_MUNICIPAL,
                        RoleEnum::ROLE_CONSEILLER_GENERAL => 'role.' . RoleEnum::ROLE_CONSEILLER_GENERAL,
                        RoleEnum::ROLE_CONSEILLER_REGIONAL => 'role.' . RoleEnum::ROLE_CONSEILLER_REGIONAL,
                        RoleEnum::ROLE_MAIRE => 'role.' . RoleEnum::ROLE_MAIRE,
                        RoleEnum::ROLE_PRESIDENT_CONSEIL_GENERAL => 'role.' . RoleEnum::ROLE_PRESIDENT_CONSEIL_GENERAL,
                        RoleEnum::ROLE_PRESIDENT_CONSEIL_REGIONAL => 'role.' . RoleEnum::ROLE_PRESIDENT_CONSEIL_REGIONAL,
                        RoleEnum::ROLE_DEPUTE => 'role.' . RoleEnum::ROLE_DEPUTE,
                        RoleEnum::ROLE_SENATEUR => 'role.' . RoleEnum::ROLE_SENATEUR
                    ]
                ])
                ->add('localisation', new ZoneGeographiqueType($this->manager))
                ->add('honneurCheck')
                ->add('honneurTexte')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\UtilisateurBundle\Entity\Mandat'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ekz_utilisateurbundle_mandat';
    }

}
