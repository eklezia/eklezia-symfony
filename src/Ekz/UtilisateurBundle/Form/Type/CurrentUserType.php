<?php

namespace Ekz\UtilisateurBundle\Form\Type;

use Ekz\UtilisateurBundle\DataTransformer\CurrentUserTransformer;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\SecurityContext;

class CurrentUserType extends AbstractType {

    /**
     *
     * @var Utilisateur
     */
    private $user;

    public function __construct(SecurityContext $securityContext) {
        $this->user = $securityContext->getToken()->getUser();
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $transformer = new CurrentUserTransformer($this->user);
        $builder->addModelTransformer($transformer);
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'required' => true
        ]);
    }
    
    public function getParent() {
        return 'hidden';
    }

    public function getName() {
        return 'current_user';
    }

}
