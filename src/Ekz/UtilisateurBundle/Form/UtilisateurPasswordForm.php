<?php

namespace Ekz\UtilisateurBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UtilisateurPasswordForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('oldPassword', 'password', [
                    'attr'=> ['placeholder' => 'Ancien mot de passe']
                ])
                ->add('newPassword', 'password', [
                    'attr'=> ['placeholder' => 'Nouveau mot de passe']
                ])
                ->add('confirmPassword', 'password', [
                    'attr'=> ['placeholder' => 'Nouveau mot de passe']
                ])
        ;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'utilisateur_password';
    }

}
