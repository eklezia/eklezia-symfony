<?php

namespace Ekz\UtilisateurBundle\Form;

use AppBundle\DataTransformer\FileTransformer;
use Doctrine\ORM\EntityManager;
use Ekz\LocalisationBundle\DataTransformer\VilleTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UtilisateurProfilForm extends AbstractType {

    private $manager;

    private $isFacebookUser;
    
    public function __construct(EntityManager $manager, $isFacebookUser) {
        $this->manager = $manager;
        $this->isFacebookUser = $isFacebookUser;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('prenom')
                ->add('pseudo')
                ->add('email')
                ->add('villePrincipal', 'text', [
                    'attr' => [
                        'data-autocomplete' => 'ville'
                    ]
                ]);
        
        if (!$this->isFacebookUser) {
            $builder->add('image', 'file', [
                    'required' => false,
                    'attr' => [
                        'accept' => 'image/*'
                    ]
                ]);
            
            $builder->get('image')->addModelTransformer(new FileTransformer());
        }

        $builder->get('villePrincipal')->addModelTransformer(new VilleTransformer($this->manager));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\UtilisateurBundle\Entity\Utilisateur'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'utilisateur_profil';
    }

}
