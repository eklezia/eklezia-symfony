<?php

namespace Ekz\UtilisateurBundle\DataTransformer;

use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class CurrentUserTransformer implements DataTransformerInterface {

    /**
     *
     * @var Utilisateur
     */
    private $user;

    public function __construct(Utilisateur $user) {
        $this->user = $user;
    }

    /**
     * A partir du nom on retourne la ville
     * @param type $value
     * @return type
     * @throws TransformationFailedException
     */
    public function reverseTransform($value) {
        return $this->user;
    }

    public function transform($value) {
        return $this->user->getUsername();
    }

}
