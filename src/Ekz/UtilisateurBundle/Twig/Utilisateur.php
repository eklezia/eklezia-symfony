<?php

namespace Ekz\UtilisateurBundle\Twig;

use Ekz\FacebookBundle\Service\FacebookService;
use Ekz\UtilisateurBundle\Entity\Utilisateur as EntityUtilisateur;
use Symfony\Component\Routing\Router;
use Twig_Extension;
use Twig_SimpleFunction;

class Utilisateur extends Twig_Extension {

    private $oFacebookService;
    private $router;

    public function __construct(FacebookService $oFacebookService, Router $router) {
        $this->oFacebookService = $oFacebookService;
        $this->router = $router;
    }

    public function getFunctions() {
        return array(
            new Twig_SimpleFunction('avatar', array($this, 'getAvatar'), array('is_safe' => array('html'))),
        );
    }

    public function getAvatar(EntityUtilisateur $user, $taille = null, $class = '') {
        $src = 'images/ek-user.jpg';

        if (null === $user->getImage()) {
            if (null !== $user->getFacebookId()) {
                $src = $this->oFacebookService->getPictureUrl($user->getFacebookId());
                if (null !== $taille) {
                    list ($w, $h) = explode('x', $taille);
                    $src .= "?width={$w}&height={$h}";
                }
            }
        } else {
            $src = $this->getUrl($user->getImage());
        }
        return '<img alt="' . $user->getPublicName() . '" src="' . $src . '" class="' . $class . '">';
    }

    public function getName() {
        return 'EkzUtilisateurService';
    }

    private function getUrl($url) {
        if (!$this->startsWith($url, 'http://') && !$this->startsWith($url, 'https://')) {
            $context = $this->router->getContext();
            $base = $context->getScheme() . '://' . $context->getHost() . '/' . $context->getBaseUrl();
            $url = rtrim($base, '/') . '/' . ltrim($url, '/');
        }
        return $url;
    }

    private function startsWith($haystack, $needle) {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

}
