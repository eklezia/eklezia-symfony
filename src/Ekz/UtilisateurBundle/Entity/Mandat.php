<?php

namespace Ekz\UtilisateurBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;

/**
 * Mandat
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Mandat
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var array
     *
     * @ORM\Column(name="titres", type="array")
     */
    private $titres;

    /**
     * @var Utilisateur
     * 
     * @ORM\OneToOne(targetEntity="Ekz\UtilisateurBundle\Entity\Utilisateur", inversedBy="mandat")
     * @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id")
     */
    private $elu;
    
    /**
     * @var string
     *
     * @ORM\Column(name="localisation", type="localisation", nullable=false)
     */
    private $localisation;

    /**
     * @var string
     *
     * @ORM\Column(name="honneur_check", type="boolean")
     * @Assert\IsTrue(message = "veuillez cocher la case d'attestation")
     */
    private $honneurCheck;
    
    /**
     * @var string
     *
     * @ORM\Column(name="honneur_texte", type="string", length=100)
     * @AppAssert\Honneur(value="j'atteste sur l'honneur")
     */
    private $honneurTexte;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=50)
     */
    private $ip;

    public function __construct() {
        $this->honneurCheck = false;
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set localisation
     *
     * @param string $localisation
     *
     * @return Mandat
     */
    public function setLocalisation($localisation)
    {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation
     *
     * @return string
     */
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * Set honneurTexte
     *
     * @param string $honneurTexte
     *
     * @return Mandat
     */
    public function setHonneurTexte($honneurTexte)
    {
        $this->honneurTexte = $honneurTexte;

        return $this;
    }

    /**
     * Get honneurTexte
     *
     * @return string
     */
    public function getHonneurTexte()
    {
        return $this->honneurTexte;
    }

    /**
     * Set elu
     *
     * @param \Ekz\UtilisateurBundle\Entity\Utilisateur $elu
     *
     * @return Mandat
     */
    public function setElu(\Ekz\UtilisateurBundle\Entity\Utilisateur $elu)
    {
        $this->elu = $elu;

        return $this;
    }

    /**
     * Get elu
     *
     * @return \Ekz\UtilisateurBundle\Entity\Utilisateur
     */
    public function getElu()
    {
        return $this->elu;
    }

    /**
     * Set honneurCheck
     *
     * @param boolean $honneurCheck
     *
     * @return Mandat
     */
    public function setHonneurCheck($honneurCheck)
    {
        $this->honneurCheck = $honneurCheck;

        return $this;
    }

    /**
     * Get honneurCheck
     *
     * @return boolean
     */
    public function getHonneurCheck()
    {
        return $this->honneurCheck;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return Mandat
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }


    /**
     * Set titres
     *
     * @param array $titres
     *
     * @return Mandat
     */
    public function setTitres($titres)
    {
        $this->titres = $titres;

        return $this;
    }

    /**
     * Get titres
     *
     * @return array
     */
    public function getTitres()
    {
        return $this->titres;
    }
}
