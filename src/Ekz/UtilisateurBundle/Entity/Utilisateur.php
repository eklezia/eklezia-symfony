<?php

namespace Ekz\UtilisateurBundle\Entity;

use AppBundle\Enum\Data\RoleEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Ekz\LocalisationBundle\Entity\Ville;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Utilisateur
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="UtilisateurRepository")
 * @UniqueEntity(fields="email", message="Cette adresse mail est déjà utilisée")
 * @ORM\HasLifecycleCallbacks()
 */
class Utilisateur implements UserInterface {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="pseudo", type="string", length=50, unique=true, nullable=true)
     */
    private $pseudo;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=150, unique=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var array
     * 
     * @ORM\Column(name="roles", type="array") 
     */
    private $roles;

    /**
     * @var array
     *
     * @ORM\Column(name="facebook_id", type="bigint", nullable=true, unique=true)
     */
    private $facebookId;

    /**
     * @var array
     *
     * @ORM\Column(name="is_actif", type="boolean")
     */
    private $isActif;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Ekz\LocalisationBundle\Entity\Ville", cascade={"persist"})
     * @ORM\JoinColumn(name="ville_principal_id")
     * @AppBundle\Validator\Constraints\LimitedChangement(type="ville", logEntity="AppBundle\Entity\Log\LogChangementVille")
     */
    private $villePrincipal;

    /**
     * @var string
     *
     * @ORM\Column(name="token_inscription", type="string", length=255, nullable=true)
     */
    private $tokenInscription;

    /**
     * @var ArrayCollection Projet $produits
     * Owning Side
     *
     * @ORM\ManyToMany(targetEntity="Ekz\ProjetBundle\Entity\Projet", inversedBy="usersFavoriteIt", cascade={"persist", "merge"})
     * @ORM\JoinTable(name="projet_en_favoris",
     *   joinColumns={@ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="produit_id", referencedColumnName="id")}
     * )
     */
    private $projetsEnFavoris;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     * @Assert\Image(
     *      maxSize = 1000000,
     *      minWidth = 150,
     *      maxWidth = 250,
     *      minHeight = 150,
     *      maxHeight = 250,
     *      groups={"preview"}
     * )
     */
    private $image;

    /**
     * @var type 
     * 
     * @ORM\OneToOne(targetEntity="Ekz\UtilisateurBundle\Entity\Mandat", mappedBy="elu", cascade={"persist", "remove"})
     */
    private $mandat;
    
    /**
     * @var \DateTime 
     * 
     * @ORM\Column(name="last_activity", type="datetime")
     * @Assert\Date()
     */
    private $lastActivity;

    /**
     * @var \DateTime
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Log\LogChangementVille", mappedBy="user", cascade={"persist","remove"})
     */
    private $logsChangementVille;

    public function __construct() {
        $this->setSalt('');
        $this->setIsActif(false);
        $this->setTokenInscription(md5($this->getEmail() . date('YmdHis')));
        $this->setLastActivity(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Utilisateur
     */
    public function setNom($nom) {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom() {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return Utilisateur
     */
    public function setPrenom($prenom) {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom() {
        return $this->prenom;
    }

    /**
     * Set pseudo
     *
     * @param string $pseudo
     *
     * @return Utilisateur
     */
    public function setPseudo($pseudo) {
        $this->pseudo = $pseudo;

        return $this;
    }

    /**
     * Get pseudo
     *
     * @return string
     */
    public function getPseudo() {
        return $this->pseudo;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Utilisateur
     */
    public function setEmail($email) {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Utilisateur
     */
    public function setPassword($password) {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return Utilisateur
     */
    public function setSalt($salt) {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt() {
        return $this->salt;
    }

    /**
     * Set facebookId
     *
     * @param integer $facebookId
     *
     * @return Utilisateur
     */
    public function setFacebookId($facebookId) {
        $this->facebookId = $facebookId;

        return $this;
    }

    /**
     * Get facebookId
     *
     * @return integer
     */
    public function getFacebookId() {
        return $this->facebookId;
    }

    /**
     * Set isActif
     *
     * @param boolean $isActif
     *
     * @return Utilisateur
     */
    public function setIsActif($isActif) {
        $this->isActif = $isActif;

        return $this;
    }

    /**
     * Get isActif
     *
     * @return boolean
     */
    public function getIsActif() {
        return $this->isActif;
    }

    /**
     * Set tokenInscription
     *
     * @param string $tokenInscription
     *
     * @return Utilisateur
     */
    public function setTokenInscription($tokenInscription) {
        $this->tokenInscription = $tokenInscription;

        return $this;
    }

    /**
     * Get tokenInscription
     *
     * @return string
     */
    public function getTokenInscription() {
        return $this->tokenInscription;
    }

    /**
     * Set villePrincipal
     *
     * @param \Ekz\LocalisationBundle\Entity\Ville $villePrincipal
     *
     * @return Utilisateur
     */
    public function setVillePrincipal(Ville $villePrincipal = null) {
        $this->villePrincipal = $villePrincipal;

        return $this;
    }

    /**
     * Get villePrincipal
     *
     * @return \Ekz\LocalisationBundle\Entity\Ville
     */
    public function getVillePrincipal() {
        return $this->villePrincipal;
    }

    /**
     * 
     * @return \Ekz\LocalisationBundle\Entity\Departement
     */
    public function getDepartement() {
        return $this->villePrincipal->getDepartement();
    }

    /**
     * 
     * @return \Ekz\LocalisationBundle\Entity\Region
     */
    public function getRegion() {
        return $this->villePrincipal->getDepartement()->getRegion();
    }

    /**
     * 
     * @return \Ekz\LocalisationBundle\Entity\Pays
     */
    public function getPays() {
        return $this->villePrincipal->getDepartement()->getRegion()->getPays();
    }

    /**
     * Retourne le nom public de l'utilisateur
     * @return type
     */
    public function getPublicName() {
        return $this->getPseudo() ? : $this->getPrenom() . ' ' . $this->getNom();
    }

    public function eraseCredentials() {
        
    }

    /**
     * Champ utiliser pour la connexion
     * @return type
     */
    public function getUsername() {
        return $this->email;
    }

    /**
     * Add projetsEnFavori
     *
     * @param \Ekz\ProjetBundle\Entity\Projet $projetsEnFavori
     *
     * @return Utilisateur
     */
    public function addProjetEnFavoris(\Ekz\ProjetBundle\Entity\Projet $projetsEnFavori) {
        $this->projetsEnFavoris[] = $projetsEnFavori;

        return $this;
    }

    /**
     * Remove projetsEnFavori
     *
     * @param \Ekz\ProjetBundle\Entity\Projet $projetsEnFavori
     */
    public function removeProjetEnFavoris(\Ekz\ProjetBundle\Entity\Projet $projetsEnFavori) {
        $this->projetsEnFavoris->removeElement($projetsEnFavori);
    }

    /**
     * Get projetsEnFavoris
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjetsEnFavoris() {
        return $this->projetsEnFavoris;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Utilisateur
     */
    public function setImage($image) {
        if (null !== $image) {
            $this->image = $image;
        }

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Retourne TRUE si la personne est associe a une ville
     * @return type
     */
    public function hasLocalisation() {
        return null !== $this->villePrincipal;
    }
    
    /**
     * Retourne TRUE si la personne se situe dans cette localisation
     * @param type $localisation
     */
    public function isInLocalisation($localisation) {
        $ville = $this->getVillePrincipal();
        $departement = $this->getDepartement();
        $region = $this->getRegion();
        $pays = $this->getPays();
        
        switch (true) {
            case $localisation instanceof $ville: return true; break;
            case $localisation instanceof $departement: return true; break;
            case $localisation instanceof $region: return true; break;
            case $localisation instanceof $pays: return true; break;
        }
        
        return false;
    }

    /**
     * Set mandat
     *
     * @param \Ekz\UtilisateurBundle\Entity\Mandat $mandat
     *
     * @return Utilisateur
     */
    public function setMandat(\Ekz\UtilisateurBundle\Entity\Mandat $mandat = null) {
        $this->mandat = $mandat;

        return $this;
    }

    /**
     * Get mandat
     *
     * @return \Ekz\UtilisateurBundle\Entity\Mandat
     */
    public function getMandat() {
        return $this->mandat;
    }

    /**
     * Add projetsEnFavori
     *
     * @param \Ekz\ProjetBundle\Entity\Projet $projetsEnFavori
     *
     * @return Utilisateur
     */
    public function addLogsChangementVille(\AppBundle\Entity\Log\LogChangementVille $logsChangementVille) {
        $logsChangementVille->setUser($this);
        $this->logsChangementVille[] = $logsChangementVille;

        return $this;
    }

    /**
     * Remove projetsEnFavori
     *
     * @param \Ekz\ProjetBundle\Entity\Projet $projetsEnFavori
     */
    public function removeLogsChangementVille(\AppBundle\Entity\Log\LogChangementVille $logsChangementVille) {
        $this->logsChangementVille->removeElement($logsChangementVille);
    }

    /**
     * Get projetsEnFavoris
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLogsChangementVille() {
        return $this->logsChangementVille;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFile() {
        if ($this->image instanceof UploadedFile) {
            $fileName = 'utilisateur-' . md5(uniqid()) . '.' . $this->image->guessExtension();
            $dir = 'media/utilisateurs/';
            $this->image->move($dir, $fileName);
            $this->setImage('/' . $dir . $fileName);
        }
    }

    public function getRoles() {
        return $this->roles;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return Utilisateur
     */
    public function setRoles($roles) {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Set lastActivity
     *
     * @param \DateTime $lastActivity
     *
     * @return Utilisateur
     */
    public function setLastActivity(\DateTime $lastActivity)
    {
        $this->lastActivity = $lastActivity;

        return $this;
    }

    /**
     * Get lastActivity
     *
     * @return \DateTime
     */
    public function getLastActivity()
    {
        return $this->lastActivity;
    }
    
    public function isFacebookUser() {
        return null !== $this->facebookId;
    }
}
