<?php

namespace Ekz\UtilisateurBundle\Listener;

use Doctrine\ORM\EntityManager;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\SecurityContext;
use DateTime;

/**
 * Description of Activity
 *
 * @author gwennael
 */
class Activity {

    protected $context;
    protected $em;

    public function __construct(SecurityContext $context, EntityManager $em) {
        $this->context = $context;
        $this->em = $em;
    }

    public function onCoreController(FilterControllerEvent $event) {
        if (null !== $this->context->getToken()) {
            $user = $this->context->getToken()->getUser();
            if ($user instanceof Utilisateur) {
                $user->setLastActivity(new DateTime());
                $this->em->persist($user);
                $this->em->flush($user);
            }
        }
    }

}
