<?php

namespace Ekz\UtilisateurBundle\Controller;

use AppBundle\Enum\Data\RoleEnum;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Ekz\UtilisateurBundle\Form\UtilisateurInscriptionForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FormController extends Controller {

    /**
     * Affiche le template du formulaire de connexion
     * @return type
     */
    public function connexionAction() {
        $authenticationUtils = $this->get('security.authentication_utils');

        return $this->render('EkzUtilisateurBundle:Form:connexion.html.twig', array(
                    'last_username' => $authenticationUtils->getLastUsername(),
                    'error' => $authenticationUtils->getLastAuthenticationError(),
        ));
    }

    /**
     * Affiche le template du formulaire d'inscription
     * @return type
     */
    public function inscriptionAction() {
        $oInscriptionService = $this->get('utilisateur.inscription');
        $oUtilisateur = $oInscriptionService->getEntity() ? : new Utilisateur();
        $oForm = $this->createForm(new UtilisateurInscriptionForm(), $oUtilisateur);

        return $this->render('EkzUtilisateurBundle:Form:inscription.html.twig', [
                    'form' => $oForm->createView(),
                    'errors' => $oInscriptionService->getErrors()
        ]);
    }

    /**
     * Check le formulaire d'inscription standard
     * @param Request $oRequest
     * @return type
     */
    public function inscriptionCheckAction(Request $oRequest) {
        $oUtilisateur = new Utilisateur();
        $oForm = $this->createForm(new UtilisateurInscriptionForm(), $oUtilisateur);

        if ($oForm->handleRequest($oRequest)->isValid()) {
            
            $oUtilisateur->setSalt(uniqid(mt_rand()));
            
            // On encode le mot de passe
            $this->get('utilisateur.encoder')->setUserPassword($oUtilisateur, $oUtilisateur->getPassword());
            
            // On leur donne un avatar par defaut
            $oUtilisateur->setImage("/img/ek-user.jpg");
            $oUtilisateur->setRoles([RoleEnum::ROLE_USER]);
            
            // On sauvegarde notre entite
            $this->getDoctrine()->getManager()->persist($oUtilisateur);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Vous avez bien été enregistrée.');
            $this->addFlash('success', 'Vous allez recevoir un mail afin de confirmer votre inscription.');

            $this->get('ekz_email.sender')->sendMailConfirmationInscription($oUtilisateur);

            return $this->redirectToRoute('app_layout_home');
        }

        $oInscriptionService = $this->get('utilisateur.inscription');
        $oInscriptionService->setEntity($oUtilisateur);
        $oInscriptionService->setErrors($this->get('validator')->validate($oUtilisateur));

        return $this->redirectToRoute('app_layout_inscription');
    }

}
