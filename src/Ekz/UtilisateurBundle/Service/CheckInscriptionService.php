<?php

namespace Ekz\UtilisateurBundle\Service;

use Symfony\Component\HttpFoundation\Session\Session;

class CheckInscriptionService {

    private $oSession;

    public function __construct(Session $oSession) {
        $this->oSession = $oSession;
    }

    /**
     * Retourne l'entite lie au formulaire
     * @return type
     */
    public function getEntity() {
        $oEntity = unserialize($this->oSession->get('inscription_service_entity'));
        $this->oSession->remove('inscription_service_entity');
        return $oEntity;
    }

    /**
     * Ajoute les information lie au formulaire
     * @param type $oEntity
     */
    public function setEntity($oEntity) {
        $this->oSession->set('inscription_service_entity', serialize($oEntity));
    }

    /**
     * Retourne les messages d'erreus
     * @return type
     */
    public function getErrors() {
        $oEntity = unserialize($this->oSession->get('inscription_service_errors'));
        $this->oSession->remove('inscription_service_errors');
        return $oEntity;
    }

    /**
     * Inserer les messages d'erreurs
     * @param \Symfony\Component\Form\FormErrorIterator $oError
     */
    public function setErrors($oError) {
        $this->oSession->set('inscription_service_errors', serialize($oError));
    }

}
