<?php

namespace Ekz\FacebookBundle\Controller;

use AppBundle\Enum\Data\RoleEnum;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Exception;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\GraphNodes\GraphUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ConnexionController extends Controller {

    public function indexAction() {
        $oFacebookConnect = $this->container->get('facebook.connect');
        $oHelper = $oFacebookConnect->getFacebook()->getRedirectLoginHelper();

        try {
            $accessToken = $oHelper->getAccessToken();
        } catch (FacebookResponseException $e) {
            throw new Exception('Graph returned an error: ' . $e->getMessage(), $e->getCode());
        } catch (FacebookSDKException $e) {
            throw new Exception('Facebook SDK returned an error: ' . $e->getMessage(), $e->getCode());
        }

        if (isset($accessToken)) {
            $oFacebookConnect->getFacebook()->setDefaultAccessToken($accessToken);
            $oResponse = $oFacebookConnect->getFacebook()->get('/me');
            $userNode = $oResponse->getGraphUser();

            $oEntityManager = $this->getDoctrine()->getManager();
            $oUtilisateurRepository = $oEntityManager->getRepository('EkzUtilisateurBundle:Utilisateur');
            $oVilleRepository = $oEntityManager->getRepository('EkzLocalisationBundle:Ville');

            /* @var $oUser GraphUser */
            $oUser = $oUtilisateurRepository->getByFacebookUserNode($userNode);

            if (null === $oUser) {

                $oVille = $oVilleRepository->getByFacebookLocalisation($userNode->getLocation()->getName());

                $oUser = new Utilisateur();
                $oUser->setNom($userNode->getLastName());
                $oUser->setPrenom($userNode->getFirstName());
                $oUser->setEmail($userNode->getEmail());
                $oUser->setFacebookId($userNode->getId());
                $oUser->setVillePrincipal($oVille);
                $oUser->setIsActif(true);
                $oUser->setPassword('');
                $oUser->setSalt('');
                $oUser->setImage($this->get('facebook.connect')->getPictureUrl($userNode->getId()));
                $oUser->setRoles([RoleEnum::ROLE_USER]);

                $oEntityManager->persist($oUser);
                $oEntityManager->flush();
            }

            // Authentification
            $token = new UsernamePasswordToken($oUser, $oUser->getPassword(), "main", $oUser->getRoles());
            $this->get("security.context")->setToken($token);

            return $this->redirectToRoute('app_frontend_home');
        }

        return $this->redirectToRoute('app_layout_home');
    }

}
