<?php

namespace Ekz\FacebookBundle\Twig;

use Ekz\FacebookBundle\Service\FacebookService;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig_Environment;
use Twig_Extension;
use Twig_Loader_Array;
use Twig_SimpleFunction;

class FacebookServiceTwigExtension extends Twig_Extension {

    private $router;
    private $oFacebookService;

    public function __construct(Router $router, FacebookService $oFacebookService) {
        $this->router = $router;
        $this->oFacebookService = $oFacebookService;
    }

    public function getFunctions() {
        return array(
            new Twig_SimpleFunction('facebook_get_picture_url', array($this, 'facebookGetPicture')),
            new Twig_SimpleFunction('facebook_url', array($this, 'getFacebookUrl'))
        );
    }
    
    public function getFacebookUrl() {
        $oHelper = $this->oFacebookService->getFacebook()->getRedirectLoginHelper();
        $sUrl = $this->router->generate($this->oFacebookService->getRoute(), array(), UrlGeneratorInterface::ABSOLUTE_URL);
        return $oHelper->getLoginUrl($sUrl, $this->oFacebookService->getPermissions());
    }
    
    public function facebookGetPictureUrl($id) {
        return $this->oFacebookService->getPictureUrl($id);
    }

    public function getName() {
        return 'EkzFacebookService';
    }
}
