<?php

namespace Ekz\FacebookBundle\Service;

use Facebook\Facebook;

class FacebookService {

    private $sAppId;
    private $sAppSecret;
    private $sAppVersion;
    private $aPermission;
    private $sRouteRedirect;
    private static $oFacebook;

    public function __construct($parameters) {
        $this->sAppId = $parameters['app_id'];
        $this->sAppSecret = $parameters['app_secret'];
        $this->sAppVersion = $parameters['app_version'];
        $this->aPermission = $parameters['permissions'];
        $this->sRouteRedirect = $parameters['route_redirect'];
    }

    /**
     * 
     * @return \Facebook\Facebook
     */
    public function getFacebook() {
        if (null === self::$oFacebook) {
            self::$oFacebook = new Facebook([
                'app_id' => $this->sAppId,
                'app_secret' => $this->sAppSecret,
                'default_graph_version' => $this->sAppVersion,
            ]);
        }
        return self::$oFacebook;
    }
    
    public function getPermissions() {
        return $this->aPermission;
    }
    
    public function getRoute() {
        return $this->sRouteRedirect;
    }

    /**
     * Retourne l'image actuelle
     * @param type $id
     * @return type
     */
    public static function getPictureUrl($id) {
        return "http://graph.facebook.com/{$id}/picture";
    }
}
