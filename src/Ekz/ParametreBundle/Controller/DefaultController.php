<?php

namespace Ekz\ParametreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('EkzParametreBundle:Default:index.html.twig', array('name' => $name));
    }
}
