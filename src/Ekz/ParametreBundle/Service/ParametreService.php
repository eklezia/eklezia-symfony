<?php

namespace Ekz\ParametreBundle\Service;

use DateTime;
use Doctrine\ORM\EntityManager;
use Ekz\ParametreBundle\Entity\Parametre;
use Twig_Extension;
use Twig_SimpleFunction;

class ParametreService extends Twig_Extension {

    private $em;
    private $parametres = [];

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function get($name) {
        if (!isset($this->parametres[$name])) {
            $parametre = $this->em->getRepository('EkzParametreBundle:Parametre')->findOneByNom($name);
            $this->parametres[$name] = $this->getValue($parametre);
        }
        return $this->parametres[$name];
    }
    
    public function set($name, $value) {
        $parametre = $this->em->getRepository('EkzParametreBundle:Parametre')->findOneByNom($name);
        $parametre->setValeur($value);
        $this->em->persist($parametre);
        $this->em->flush();
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('parametre', array($this, 'get'))
        ];
    }

    public function getName() {
        return 'ekz_parametre';
    }

    private function getValue(Parametre $parametre) {
        switch ($parametre->getType()) {
            case 'boolean': return (bool) $parametre->getValeur(); break;
            case 'string': return (string) $parametre->getValeur(); break;
        }
    }

}
