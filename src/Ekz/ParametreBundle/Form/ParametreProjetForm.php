<?php

namespace Ekz\ParametreBundle\Form;

use Ekz\ParametreBundle\Service\ParametreService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParametreProjetForm extends AbstractType
{
    private $service;

    public function __construct(ParametreService $service) {
        $this->service = $service;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('projet_moderation', 'checkbox', [
                'data' => $this->service->get('projet_moderation')
            ])
            ->add('projet_date_butoire_min', 'text', [
                'data' => $this->service->get('projet_date_butoire_min', true)
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ekz_parametrebundle_projet';
    }
}
