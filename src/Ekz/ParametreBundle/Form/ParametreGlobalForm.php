<?php

namespace Ekz\ParametreBundle\Form;

use Ekz\ParametreBundle\Service\ParametreService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ParametreGlobalForm extends AbstractType
{
    private $service;

    public function __construct(ParametreService $service) {
        $this->service = $service;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('blacklist_mot_regex', 'textarea', [
                'data' => $this->service->get('blacklist_mot_regex', true)
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ekz_parametrebundle_projet';
    }
}
