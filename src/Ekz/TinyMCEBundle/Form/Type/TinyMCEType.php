<?php

namespace Ekz\TinyMCEBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TinyMCEType extends AbstractType {

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'required' => false,
            'attr' => [
                'data-editor' => 'tinymce'
            ]
        ]);
    }

    public function getParent() {
        return 'textarea';
    }

    public function getName() {
        return 'tinymce';
    }

}
