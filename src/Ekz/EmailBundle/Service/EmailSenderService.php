<?php

namespace Ekz\EmailBundle\Service;

use Ekz\ProjetBundle\Entity\Projet;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Templating\EngineInterface;

class EmailSenderService {

    private $mailer;
    private $templating;
    private $sender;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating) {
        $this->sender = 'webmaster@eklezia.org';
        $this->mailer = $mailer;
        $this->templating = $templating;
    }

    /**
     * permet l'envoi d'un email. Cette fonction est utiliser pour faciliter le developpement des
     * autres fonction d'envoi d'email
     * @param type $to
     * @param type $subject
     * @param type $body
     * @param type $from
     */
    protected function sendMessage($to, $subject, $body, $from = null) {
        $mail = Swift_Message::newInstance()
                ->setFrom(null === $from ? $this->sender : $from)
                ->setTo($to)
                ->setSubject($subject)
                ->setBody($body)
                ->setContentType('text/html');
        $this->mailer->send($mail);
    }
    
    // ---
    // LISTE DES FONCTION D'ENVOI D'EMAIL
    // ---
    
    /**
     * Envoi de l'email de confirmation d'inscription
     * @param Utilisateur $utilisateur
     */
    public function sendMailConfirmationInscription(Utilisateur $utilisateur) {
        $template = 'EkzEmailBundle:Utilisateur:confirmation-inscription.html.twig';
        $receiver = $utilisateur->getEmail();
        $subject = "Validation d'inscription";
        $body = $this->templating->render($template, [
            'user' => $utilisateur
        ]);
        $this->sendMessage($receiver, $subject, $body);
    }
    
    /**
     * Envoi de l'email contenant le nouveau mot de passe
     * @param Utilisateur $utilisateur
     * @param type $password
     */
    public function sendMailNouveauMotDePasse(Utilisateur $utilisateur, $password) {
        $template = 'EkzEmailBundle:Utilisateur:new-password.html.twig';
        $receiver = $utilisateur->getEmail();
        $subject = "Nouveau mot de passe";
        $body = $this->templating->render($template, [
            'password' => $password,
            'user' => $utilisateur,            
        ]);
        $this->sendMessage($receiver, $subject, $body);
    }
    
    /**
     * Envoi un email indiquant a l'utilisateur que son projet est en attente de moderation
     * @param Projet $projet
     */
    public function sendMailProjetAttenteDeModeration(Projet $projet) {
        $template = 'EkzEmailBundle:Projet:attente-de-moderation.html.twig';
        $receiver = $projet->getCreateur()->getEmail();
        $subject = "Projet en attente de modération";
        $body = $this->templating->render($template, [
            'projet' => $projet
        ]);
        $this->sendMessage($receiver, $subject, $body);
    }
    
    /**
     * Envoi un email indiquant a l'utilisateur que son projet a ete valide
     * @param Projet $projet
     */
    public function sendMailProjetModerationValider(Projet $projet) {
        $template = 'EkzEmailBundle:Projet:moderation-valide.html.twig';
        $receiver = $projet->getCreateur()->getEmail();
        $subject = "Votre projet a été validé";
        $body = $this->templating->render($template, [
            'projet' => $projet
       ]);
        $this->sendMessage($receiver, $subject, $body);
    }
    
    /**
     * Envoi un email indiquant a l'utilisateur que son projet a ete refuse
     * @param Projet $projet
     */
    public function sendMailProjetModerationRefuser(Projet $projet) {
        $template = 'EkzEmailBundle:Projet:moderation-refuse.html.twig';
        $receiver = $projet->getCreateur()->getEmail();
        $subject = "Votre projet a été refusé";
        $body = $this->templating->render($template, [
            'projet' => $projet
        ]);
        $this->sendMessage($receiver, $subject, $body);
    }

    /**
     * Envoi un email a l'elu pour lui annoncer qu'un projet a ete valide par
     * les citoyens
     * @param Projet $projet
     */
    public function sendMailProjetElu(Projet $projet, $receiver) {
        $template = 'EkzEmailBundle:Projet:envoyer-a-elu.html.twig';
        $subject = "Votre projet a été validé";
        $body = $this->templating->render($template, [
            'projet' => $projet
       ]);
        $this->sendMessage($receiver, $subject, $body);
    }
}
