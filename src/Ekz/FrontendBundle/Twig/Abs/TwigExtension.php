<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\FrontendBundle\Twig\Abs;

use Twig_Extension;

abstract class TwigExtension extends Twig_Extension {
    
    protected function needEnvironment() {
        return [
            'is_safe' => array('html'),
            'needs_environment' => true
        ];
    }
    
}
