<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FacebookBundle\Service\FacebookService;
use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Twig_Environment;
use Twig_SimpleFunction;

class Topbar extends TwigExtension {

    private $router;
    private $facebookService;
    private $context;

    public function __construct(Router $oRouter, FacebookService $oFacebookService, SecurityContext $context) {
        $this->router = $oRouter;
        $this->facebookService = $oFacebookService;
        $this->context = $context;
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('topbar', array($this, 'topbar'), $this->needEnvironment()),
        ];
    }

    public function topbar(Twig_Environment $twig) {
        return $this->context->isGranted('IS_AUTHENTICATED_REMEMBERED')
                ? $this->loggedTopbar($twig)
                : $this->unloggedTopbar($twig);
        
    }
    
    private function unloggedTopbar(Twig_Environment $twig) {
        return $twig->render('EkzFrontendBundle:Topbar:unlogged-topbar.html.twig');
    }
    
    private function loggedTopbar(Twig_Environment $twig) {
        return $twig->render('EkzFrontendBundle:Topbar:logged-topbar.html.twig', [
            'user' => $this->context->getToken()->getUser()
        ]);
    }

    public function getName() {
        return 'ekz.frontend.topbar';
    }

}
