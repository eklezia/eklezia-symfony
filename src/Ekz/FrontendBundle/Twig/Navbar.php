<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Twig_Environment;
use Twig_SimpleFunction;

class Navbar extends TwigExtension {

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('navbar_elu', array($this, 'elu'), $this->needEnvironment())
        ];
    }

    public function elu(Twig_Environment $twig, $etat = null) {
        return $twig->render('EkzFrontendBundle:Navbar:elu.html.twig', [
            'etat' => $etat
        ]);
    }

    public function getName() {
        return 'ekz.frontend.navbar';
    }

}
