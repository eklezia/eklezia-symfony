<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Component\Routing\Router;
use Twig_Environment;
use Twig_SimpleFunction;

class Projet extends TwigExtension {

    private $router;
    
    public function __construct(Router $oRouter) {
        $this->router = $oRouter;
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('projet_block', array($this, 'block'), $this->needEnvironment()),
            new Twig_SimpleFunction('projet_social_block', array($this, 'socialBlock'), $this->needEnvironment()),
            new Twig_SimpleFunction('projet_cover', array($this, 'cover'), $this->needEnvironment())
        ];
    }
    
    public function block(Twig_Environment $twig, $projet) {
        return $twig->render('EkzFrontendBundle:Projet:block.html.twig', [
            'projet' => $projet
        ]);
    }
    
    public function socialBlock(Twig_Environment $twig, $projet) {
        return $twig->render('EkzFrontendBundle:Projet:social-block.html.twig', [
            'projet' => $projet
        ]);
    }
    
    public function cover(Twig_Environment $twig, $projet) {
        return $twig->render('EkzFrontendBundle:Projet:cover.html.twig', [
            'projet' => $projet
        ]);
    }
    
    public function getName() {
        return 'ekz.frontend.projet';
    }

}
