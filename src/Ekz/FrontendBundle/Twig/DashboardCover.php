<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Twig_Environment;
use Twig_SimpleFunction;

class DashboardCover extends TwigExtension {

    private $router;
    private $context;

    public function __construct(Router $oRouter, SecurityContext $context) {
        $this->router = $oRouter;
        $this->context = $context;
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('dashboard_cover', array($this, 'dashboardCover'), $this->needEnvironment())
        ];
    }

    public function dashboardCover(Twig_Environment $twig, $tab) {
        return $twig->render('EkzFrontendBundle:DashboardCover:index.html.twig', [
            'user' => $this->context->getToken()->getUser(),
            'tab' => $tab
        ]);
    }

    public function getName() {
        return 'ekz.frontend.dashboard_cover';
    }

}
