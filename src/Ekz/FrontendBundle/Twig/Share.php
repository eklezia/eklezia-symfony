<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Component\Routing\Router;
use Twig_Environment;
use Twig_SimpleFunction;

class Share extends TwigExtension {

    private $router;

    public function __construct(Router $oRouter) {
        $this->router = $oRouter;
    }
    
    public function getFunctions() {
        return [
            new Twig_SimpleFunction('share_facebook', array($this, 'shareFacebook'), $this->needEnvironment()),
            new Twig_SimpleFunction('share_twitter', array($this, 'shareTwitter'), $this->needEnvironment()),
        ];
    }

    public function shareFacebook(Twig_Environment $twig, $url, $text) {
        return $twig->render('EkzFrontendBundle:Share:facebook.html.twig', [
            'url' => $url,
            'text' => $text,
        ]);
    }
    
    public function shareTwitter(Twig_Environment $twig, $url, $text) {
        return $twig->render('EkzFrontendBundle:Share:twitter.html.twig', [
            'url' => $url,
            'text' => $text,
        ]);
    }

    public function getName() {
        return 'ekz.frontend.share';
    }

}
