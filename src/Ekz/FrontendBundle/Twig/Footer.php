<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Twig_Environment;
use Twig_SimpleFunction;

class Footer extends TwigExtension {

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('footer', array($this, 'footer'), $this->needEnvironment())
        ];
    }

    public function footer(Twig_Environment $twig) {
        return $twig->render('EkzFrontendBundle:Footer:index.html.twig');
    }

    public function getName() {
        return 'ekz.frontend.footer';
    }

}
