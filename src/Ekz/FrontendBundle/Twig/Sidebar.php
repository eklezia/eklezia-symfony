<?php

namespace Ekz\FrontendBundle\Twig;

use Doctrine\ORM\EntityManager;
use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Twig_Environment;
use Twig_SimpleFunction;

class Sidebar extends TwigExtension {

    private $router;
    private $context;
    private $em;

    public function __construct(Router $oRouter, SecurityContext $context, EntityManager $em) {
        $this->router = $oRouter;
        $this->context = $context;
        $this->em = $em;
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('sidebar', array($this, 'sidebar'), $this->needEnvironment())
        ];
    }

    public function sidebar(Twig_Environment $twig, $tag = null) {
        return $twig->render('EkzFrontendBundle:Sidebar:index.html.twig', [
            'tag' => $tag
        ]);
    }

    public function getName() {
        return 'ekz.frontend.sidebar';
    }

}
