<?php

namespace Ekz\FrontendBundle\Twig;

use Doctrine\ORM\EntityManager;
use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Twig_Environment;
use Twig_SimpleFunction;

class SidebarInfo extends TwigExtension {

    private $router;
    private $context;
    private $em;

    public function __construct(Router $oRouter, SecurityContext $context, EntityManager $em) {
        $this->router = $oRouter;
        $this->context = $context;
        $this->em = $em;
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('sidebar_info', array($this, 'sidebar'), $this->needEnvironment())
        ];
    }

    public function sidebar(Twig_Environment $twig) {
        $oUser = $this->context->getToken()->getUser();

        $iNombreProjet = $this->em->getRepository('EkzProjetBundle:Projet')->countByUser($oUser);
        $iNombreCommentaire = $this->em->getRepository('EkzProjetBundle:Commentaire')->countByUser($oUser);
        $iNombreVote = $this->em->getRepository('EkzProjetBundle:Vote')->countByUser($oUser);

        return $twig->render('EkzFrontendBundle:SidebarInfo:index.html.twig', [
            'nombre_projet' => $iNombreProjet,
            'nombre_commentaire' => $iNombreCommentaire,
            'nombre_vote' => $iNombreVote
        ]);
    }

    public function getName() {
        return 'ekz.frontend.sidebar_info';
    }

}
