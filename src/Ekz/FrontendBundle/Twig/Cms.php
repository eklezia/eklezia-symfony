<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Component\Routing\Router;
use Twig_Environment;
use Twig_SimpleFunction;

class Cms extends TwigExtension {

    private $router;
    
    public function __construct(Router $oRouter) {
        $this->router = $oRouter;
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('cms_page_cover', array($this, 'cover'), $this->needEnvironment())
        ];
    }

    public function cover(Twig_Environment $twig, $page) {
        return $twig->render('EkzFrontendBundle:CMS:page/cover.html.twig', [
            'page' => $page
        ]);
    }
    
    public function getName() {
        return 'ekz.frontend.cms';
    }

}
