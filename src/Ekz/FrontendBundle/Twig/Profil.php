<?php

namespace Ekz\FrontendBundle\Twig;

use Ekz\FrontendBundle\Twig\Abs\TwigExtension;
use Symfony\Component\Routing\Router;
use Twig_Environment;
use Twig_SimpleFunction;

class Profil extends TwigExtension {

    private $router;
    
    public function __construct(Router $oRouter) {
        $this->router = $oRouter;
    }

    public function getFunctions() {
        return [
            new Twig_SimpleFunction('profil_sidebar', array($this, 'sidebar'), $this->needEnvironment())
        ];
    }
    
    public function sidebar(Twig_Environment $twig, $tag) {
        return $twig->render('EkzFrontendBundle:Profil:sidebar.html.twig', [
            'tag' => $tag
        ]);
    }
    
    public function getName() {
        return 'ekz.frontend.profil';
    }

}
