<?php

namespace Ekz\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function renderAction() {
        return $this->render('EkzTopbarBundle:Topbar:unlogged-topbar.html.twig', [
            'facebook_url' => $this->getFacebookUrl(),
        ]);
    }

    private function getFacebookUrl() {
        $oFacebookConnect = $this->container->get('facebook.connect');
        $oHelper = $oFacebookConnect->getFacebook()->getRedirectLoginHelper();
        $sUrl = $this->generateUrl($oFacebookConnect->getRoute(), array(), UrlGeneratorInterface::ABSOLUTE_URL);
        return $oHelper->getLoginUrl($sUrl, $oFacebookConnect->getPermissions());
    }
}
