<?php

namespace Ekz\LocalisationBundle\EventListener\Doctrine;

use Symfony\Component\DependencyInjection\ContainerInterface;

class ContainerListener {

    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
    }

    public function getContainer() {
        return $this->container;
    }

}
