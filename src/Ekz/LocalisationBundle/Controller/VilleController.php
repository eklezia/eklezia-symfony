<?php

namespace Ekz\LocalisationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class VilleController extends Controller
{
    public function indexAction(Request $oRequest)
    {
        if ($oRequest->isXmlHttpRequest()) {
            $oRepository = $this->getDoctrine()->getRepository('EkzLocalisationBundle:Ville');
            $aVille = $oRepository->findByTerm($oRequest->get('term'));
            return new JsonResponse($aVille);
        }
        throw $this->createNotFoundException();
    }
}
