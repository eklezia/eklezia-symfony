<?php

namespace Ekz\LocalisationBundle\EntityPropertyType;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\StringType;
use Ekz\LocalisationBundle\Entity\Departement;
use Ekz\LocalisationBundle\Entity\Pays;
use Ekz\LocalisationBundle\Entity\Region;
use Ekz\LocalisationBundle\Entity\Ville;

class LocalisationType extends StringType {

    const LOCALISATION = 'localisation';
    const VILLE = 'ville';
    const DEPARTEMENT = 'departement';
    const REGION = 'region';
    const PAYS = 'pays';

    public function getName() {
        return self::LOCALISATION;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform) {
        switch (true) {
            case $value instanceof Ville:
                return self::VILLE . "-" . $value->getId();
            case $value instanceof Departement:
                return self::DEPARTEMENT . "-" . $value->getId();
            case $value instanceof Region:
                return self::REGION . "-" . $value->getId();
            case $value instanceof Pays:
                return self::PAYS . "-" . $value->getId();
        }
        return null;
    }

    public function convertToPHPValue($value, AbstractPlatform $platform) {
        if (null !== $value) {
            list ($type, $id) = explode('-', $value);
            $oDoctrine = $this->getDoctrine($platform);
            switch ($type) {
                case self::VILLE:
                    return $oDoctrine->getRepository('EkzLocalisationBundle:Ville')->find($id);
                case self::DEPARTEMENT:
                    return $oDoctrine->getRepository('EkzLocalisationBundle:Departement')->find($id);
                case self::REGION:
                    return $oDoctrine->getRepository('EkzLocalisationBundle:Region')->find($id);
                case self::PAYS:
                    return $oDoctrine->getRepository('EkzLocalisationBundle:Pays')->find($id);
            }
        }
        return null;
    }

    private function getDoctrine($platform) {
        $listeners = $platform->getEventManager()->getListeners('getContainer');
        $listener = array_shift($listeners);
        return $listener->getContainer()->get('doctrine');
    }

}
