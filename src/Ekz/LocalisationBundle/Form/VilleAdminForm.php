<?php

namespace Ekz\LocalisationBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class VilleAdminForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom', 'text')
                ->add('slug')
                ->add('codePostal')
                ->add('departement', 'entity', [
                    'class' => 'EkzLocalisationBundle:Departement',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('d')
                                ->orderBy('d.nom', 'ASC');
                    },
                ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\LocalisationBundle\Entity\Ville'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ekz_localisationbundle_ville';
    }

}
