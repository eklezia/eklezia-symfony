<?php

namespace Ekz\LocalisationBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class DepartementAdminForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('slug')
                ->add('code')
                ->add('region', 'entity', [
                    'class' => 'EkzLocalisationBundle:Region',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('r')
                                ->orderBy('r.nom', 'ASC');
                    },
                ])
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\LocalisationBundle\Entity\Departement'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ekz_localisationbundle_departement';
    }

}
