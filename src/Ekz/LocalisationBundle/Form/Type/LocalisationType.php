<?php

namespace Ekz\LocalisationBundle\Form\Type;

use Ekz\LocalisationBundle\DataTransformer\LocalisationTransformer;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\SecurityContext;

class LocalisationType extends AbstractType {

    /**
     *
     * @var Utilisateur
     */
    private $user;

    public function __construct(SecurityContext $securityContext) {
        $this->user = $securityContext->getToken()->getUser();
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->addModelTransformer(new LocalisationTransformer($this->user));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'choices' => [
                'ville' => $this->user->getVillePrincipal(),
                'departement' => $this->user->getDepartement(),
                'region' => $this->user->getRegion(),
            ]
        ]);
    }

    public function getParent() {
        return 'choice';
    }

    public function getName() {
        return 'localisation';
    }

}
