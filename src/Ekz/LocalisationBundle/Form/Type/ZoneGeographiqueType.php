<?php

namespace Ekz\LocalisationBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Ekz\LocalisationBundle\DataTransformer\ZoneGeographiqueTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ZoneGeographiqueType extends AbstractType {
    
    private $manager;

    const ELEMENT_ID = 'zone_geographique_id';
    const ELEMENT_TEXT = 'zone_geographique_text';
    
    public function __construct(EntityManager $manager) {
        $this->manager = $manager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add(self::ELEMENT_ID, 'hidden', [
                    'attr' => [
                        'data-autocomplete-hidden' => true
                    ]
                ])
                ->add(self::ELEMENT_TEXT, 'text', [
                    'attr' => [
                        'placeholder' => 'Localisation ...',
                        'data-autocomplete' => 'localisation'
                    ]
                ])
        ;
        
        $builder->addModelTransformer(new ZoneGeographiqueTransformer($this->manager));
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'compound' => true
        ));
    }

    public function getParent() {
        return 'text';
    }

    public function getName() {
        return 'zone_geographique';
    }

}
