<?php

namespace Ekz\LocalisationBundle\DataTransformer;

use Doctrine\ORM\EntityManager;
use Ekz\LocalisationBundle\Entity\Departement;
use Ekz\LocalisationBundle\Entity\Region;
use Ekz\LocalisationBundle\Entity\Ville;
use Ekz\LocalisationBundle\Form\Type\ZoneGeographiqueType;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ZoneGeographiqueTransformer implements DataTransformerInterface {

    private $manager;
    
    const VILLE = 'ville';
    const DEPARTEMENT = 'departement';
    const REGION = 'region';
    
    public function __construct(EntityManager $manager) {
        $this->manager = $manager;
    }
    
    /**
     * A partir du nom on retourne l'entite
     * @param type $value
     * @return type
     * @throws TransformationFailedException
     */
    public function reverseTransform($value) {
        if (isset($value[ZoneGeographiqueType::ELEMENT_ID]) && null !== $value[ZoneGeographiqueType::ELEMENT_ID]) {
            list ($type, $id) = explode('-', $value[ZoneGeographiqueType::ELEMENT_ID]);
            switch($type) {
                case self::VILLE:
                    return $this->manager->getRepository('EkzLocalisationBundle:Ville')->find($id);
                    break;
                case self::DEPARTEMENT:
                    return $this->manager->getRepository('EkzLocalisationBundle:Departement')->find($id);
                    break;
                case self::REGION:
                    return $this->manager->getRepository('EkzLocalisationBundle:Region')->find($id);
                    break;
            }
        }
        return null;
    }

    /**
     * A partir de l'entite on retourne le nom
     * @param type $value
     * @return type
     */
    public function transform($value) {
        $data = [
            ZoneGeographiqueType::ELEMENT_ID => null,
            ZoneGeographiqueType::ELEMENT_TEXT => null,
        ];
        if (null !== $value) {
            switch(true) {
                case $value instanceof Ville:
                    $data[ZoneGeographiqueType::ELEMENT_ID] = self::VILLE . '-' . $value->getId();
                    $data[ZoneGeographiqueType::ELEMENT_TEXT] = $value->getNom();
                    break;
                case $value instanceof Departement:
                    $data[ZoneGeographiqueType::ELEMENT_ID] = self::DEPARTEMENT . '-' . $value->getId();
                    $data[ZoneGeographiqueType::ELEMENT_TEXT] = $value->getNom();
                    break;
                case $value instanceof Region:
                    $data[ZoneGeographiqueType::ELEMENT_ID] = self::REGION . '-' . $value->getId();
                    $data[ZoneGeographiqueType::ELEMENT_TEXT] = $value->getNom();
                    break;
            }
        }
        return $data;
    }

}
