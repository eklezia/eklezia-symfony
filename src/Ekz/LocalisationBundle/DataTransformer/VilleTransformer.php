<?php

namespace Ekz\LocalisationBundle\DataTransformer;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class VilleTransformer implements DataTransformerInterface {

    private $manager;

    public function __construct(EntityManager $manager) {
        $this->manager = $manager;
    }

    /**
     * A partir du nom on retourne la ville
     * @param type $value
     * @return type
     * @throws TransformationFailedException
     */
    public function reverseTransform($value) {
        if (!empty($value)) {
            $oVille = $this->manager->getRepository('EkzLocalisationBundle:Ville')->findOneByNom($value);

            if (null === $oVille) {
                throw new TransformationFailedException(sprintf("La ville {$value} n'existe pas"));
            }

            return $oVille;
        }

        return null;
    }

    public function transform($value) {
        if (null === $value) {
            return '';
        }
        
        return $value->getNom();
    }

}
