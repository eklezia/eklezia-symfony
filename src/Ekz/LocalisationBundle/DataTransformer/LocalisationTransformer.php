<?php

namespace Ekz\LocalisationBundle\DataTransformer;

use Ekz\LocalisationBundle\Entity\Departement;
use Ekz\LocalisationBundle\Entity\Region;
use Ekz\LocalisationBundle\Entity\Ville;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class LocalisationTransformer implements DataTransformerInterface {

    const VILLE = 'ville';
    const DEPARTEMENT = 'departement';
    const REGION = 'region';
    
    /**
     *
     * @var Utilisateur
     */
    private $user;

    public function __construct(Utilisateur $user) {
        $this->user = $user;
    }

    /**
     * A partir du nom on retourne la ville
     * @param type $value
     * @return type
     * @throws TransformationFailedException
     */
    public function reverseTransform($value) {
        $return = $value;
        switch ($value) {
            case self::VILLE: $return = $this->user->getVillePrincipal(); break;
            case self::DEPARTEMENT: $return = $this->user->getDepartement(); break;
            case self::REGION: $return = $this->user->getRegion(); break;
        }
        return $return;
    }

    public function transform($value) {
        if (null !== $value) {
            switch (true) {
                case $value instanceof Ville: return self::VILLE; break;
                case $value instanceof Departement: return self::DEPARTEMENT; break;
                case $value instanceof Region: return self::REGION; break;
            }
        }
        return null;
    }

}
