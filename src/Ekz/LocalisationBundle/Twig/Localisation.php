<?php

namespace Ekz\LocalisationBundle\Twig;

use Ekz\LocalisationBundle\Entity\Departement;
use Ekz\LocalisationBundle\Entity\Pays;
use Ekz\LocalisationBundle\Entity\Region;
use Ekz\LocalisationBundle\Entity\Ville;
use Twig_Extension;
use Twig_SimpleFunction;

class Localisation extends Twig_Extension {

    public function getFunctions() {
        return array(
            new Twig_SimpleFunction('localisation_name', array($this, 'getLocalisationName')),
        );
    }
    
    public function getLocalisationName($oLocalisation) {
        switch (true) {
            case $oLocalisation instanceof Ville:
                return $oLocalisation->getNom() . ', ' . $oLocalisation->getPays()->getNom();
                break;
            case $oLocalisation instanceof Departement:
                return $oLocalisation->getNom() . ', ' . $oLocalisation->getPays()->getNom();
                break;
            case $oLocalisation instanceof Region:
                return $oLocalisation->getNom() . ', ' . $oLocalisation->getPays()->getNom();
                break;
            case $oLocalisation instanceof Pays:
                return $oLocalisation->getNom();
                break;
        }
    }


    public function getName() {
        return 'EkzLocalisationService';
    }

}
