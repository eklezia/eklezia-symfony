<?php

namespace Ekz\LocalisationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Ekz\LocalisationBundle\SQL\VilleSQL;
use Symfony\Component\HttpFoundation\Request;


class VilleRepository extends EntityRepository {
    
    public function getByFacebookLocalisation($localisation) {
        return $this->createQueryBuilder('v')
                ->where('v.nom LIKE :nom')
                ->setParameter('nom', explode(', ', $localisation)[0])
                ->getQuery()
                ->getOneOrNullResult();
    }
    
    public function findByTerm($term) {
        $oConnection = $this->getEntityManager()->getConnection();
        $oStatement = $oConnection->prepare(VilleSQL::getByTerm($term));
        $oStatement->bindValue('nom', '%' . $term . '%');
        $oStatement->execute();
        return $oStatement->fetchAll();
    }
    
    public function count() {
        return $this->createQueryBuilder('u')
                        ->select('COUNT(u)')
                        ->getQuery()
                        ->getSingleScalarResult();
    }
    
    // -----
    // ZONE CONCERNANT DataTable
    // -----
    
    public function countByDatatable(Request $request) {
        $oQuery = $this->createQueryBuilder('v')
                ->select('COUNT(v)')
                ->where('v.nom LIKE :ville')
                ->setParameter('ville', '%' . $request->get('ville') . '%');
        
        return $oQuery->getQuery()->getSingleScalarResult();
    }
    
    public function findByDatatable(Request $request) {
        $oQuery = $this->createQueryBuilder('v');
        
        if (!empty($request->get('ville'))) {
            $oQuery->addSelect("(
                CASE
                    WHEN v.nom LIKE :ville_0 THEN 0
                    WHEN v.nom LIKE :ville_1 THEN 1
                    WHEN v.nom LIKE :ville_2 THEN 2
                    WHEN v.nom LIKE :ville_3 THEN 3
                    WHEN v.nom LIKE :ville_4 THEN 4
                    ELSE 5 END
                ) AS HIDDEN ville_order");
            $oQuery->setParameter('ville_0', $request->get('ville'));
            $oQuery->setParameter('ville_1', $request->get('ville') . ' %');
            $oQuery->setParameter('ville_2', '% ' . $request->get('ville') . ' %');
            $oQuery->setParameter('ville_3', $request->get('ville') . '%');
            $oQuery->setParameter('ville_4', '%' . $request->get('ville') . '%');
            $oQuery->orderBy('ville_order', 'ASC');
            
            $oQuery->where('v.nom LIKE :ville');
            $oQuery->setParameter('ville', '%' . $request->get('ville') . '%');
        }
        
        $oQuery->setFirstResult($request->get('start'));
        $oQuery->setMaxResults($request->get('length'));

        return $oQuery->getQuery()->getResult();
    }
}
