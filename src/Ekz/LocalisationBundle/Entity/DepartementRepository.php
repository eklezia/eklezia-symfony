<?php

namespace Ekz\LocalisationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Ekz\LocalisationBundle\SQL\DepartementSQL;
use Symfony\Component\HttpFoundation\Request;

class DepartementRepository extends EntityRepository {
    
    public function findByTerm($term) {
        $oConnection = $this->getEntityManager()->getConnection();
        $oStatement = $oConnection->prepare(DepartementSQL::getByTerm($term));
        $oStatement->bindValue('nom', '%' . $term . '%');
        $oStatement->execute();
        return $oStatement->fetchAll();
    }
    
    public function count() {
        return $this->createQueryBuilder('d')
                        ->select('COUNT(d)')
                        ->getQuery()
                        ->getSingleScalarResult();
    }
    
    // -----
    // ZONE CONCERNANT DataTable
    // -----
    
    public function countByDatatable(Request $request) {
        $oQuery = $this->createQueryBuilder('d')
                ->select('COUNT(d)')
                ->where('d.nom LIKE :departement')
                ->setParameter('departement', '%' . $request->get('departement') . '%');
        
        return $oQuery->getQuery()->getSingleScalarResult();
    }
    
    public function findByDatatable(Request $request) {
        $oQuery = $this->createQueryBuilder('d');
        
        if (!empty($request->get('departement'))) {
            $oQuery->addSelect("(
                CASE
                    WHEN d.nom LIKE :departement_0 THEN 0
                    WHEN d.nom LIKE :departement_1 THEN 1
                    WHEN d.nom LIKE :departement_2 THEN 2
                    WHEN d.nom LIKE :departement_3 THEN 3
                    WHEN d.nom LIKE :departement_4 THEN 4
                    ELSE 5 END
                ) AS HIDDEN departement_order");
            $oQuery->setParameter('departement_0', $request->get('departement'));
            $oQuery->setParameter('departement_1', $request->get('departement') . ' %');
            $oQuery->setParameter('departement_2', '% ' . $request->get('departement') . ' %');
            $oQuery->setParameter('departement_3', $request->get('departement') . '%');
            $oQuery->setParameter('departement_4', '%' . $request->get('departement') . '%');
            $oQuery->orderBy('departement_order', 'ASC');
            
            $oQuery->where('d.nom LIKE :departement');
            $oQuery->setParameter('departement', '%' . $request->get('departement') . '%');
        }
        
        $oQuery->setFirstResult($request->get('start'));
        $oQuery->setMaxResults($request->get('length'));

        return $oQuery->getQuery()->getResult();
    }
}
