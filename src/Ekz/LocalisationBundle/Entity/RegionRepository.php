<?php

namespace Ekz\LocalisationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Ekz\LocalisationBundle\SQL\RegionSQL;
use Symfony\Component\HttpFoundation\Request;

class RegionRepository extends EntityRepository {
    
    public function findByTerm($term) {
        $oConnection = $this->getEntityManager()->getConnection();
        $oStatement = $oConnection->prepare(RegionSQL::getByTerm($term));
        $oStatement->bindValue('nom', '%' . $term . '%');
        $oStatement->execute();
        return $oStatement->fetchAll();
    }
    
    public function count() {
        return $this->createQueryBuilder('r')
                        ->select('COUNT(r)')
                        ->getQuery()
                        ->getSingleScalarResult();
    }
    
    // -----
    // ZONE CONCERNANT DataTable
    // -----
    
    public function countByDatatable(Request $request) {
        $oQuery = $this->createQueryBuilder('r')
                ->select('COUNT(r)')
                ->where('r.nom LIKE :region')
                ->setParameter('region', '%' . $request->get('region') . '%');
        
        return $oQuery->getQuery()->getSingleScalarResult();
    }
    
    public function findByDatatable(Request $request) {
        $oQuery = $this->createQueryBuilder('r');
        
        if (!empty($request->get('region'))) {
            $oQuery->addSelect("(
                CASE
                    WHEN r.nom LIKE :region_0 THEN 0
                    WHEN r.nom LIKE :region_1 THEN 1
                    WHEN r.nom LIKE :region_2 THEN 2
                    WHEN r.nom LIKE :region_3 THEN 3
                    WHEN r.nom LIKE :region_4 THEN 4
                    ELSE 5 END
                ) AS HIDDEN region_order");
            $oQuery->setParameter('region_0', $request->get('region'));
            $oQuery->setParameter('region_1', $request->get('region') . ' %');
            $oQuery->setParameter('region_2', '% ' . $request->get('region') . ' %');
            $oQuery->setParameter('region_3', $request->get('region') . '%');
            $oQuery->setParameter('region_4', '%' . $request->get('region') . '%');
            $oQuery->orderBy('region_order', 'ASC');
            
            $oQuery->where('r.nom LIKE :region');
            $oQuery->setParameter('region', '%' . $request->get('region') . '%');
        }
        
        $oQuery->setFirstResult($request->get('start'));
        $oQuery->setMaxResults($request->get('length'));

        return $oQuery->getQuery()->getResult();
    }
}
