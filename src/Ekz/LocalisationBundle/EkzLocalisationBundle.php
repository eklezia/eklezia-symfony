<?php

namespace Ekz\LocalisationBundle;

use Doctrine\DBAL\Types\Type;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class EkzLocalisationBundle extends Bundle {

    public function boot() {
        $this->addEntityPropertyType();
    }

    private function addEntityPropertyType() {
        Type::addType('localisation', 'Ekz\LocalisationBundle\EntityPropertyType\LocalisationType');
        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->getConnection()->getDatabasePlatform()->registerDoctrineTypeMapping('localisation', 'localisation');
    }

}
