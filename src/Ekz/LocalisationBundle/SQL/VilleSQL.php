<?php

namespace Ekz\LocalisationBundle\SQL;

use Ekz\LocalisationBundle\SQL\SQL;

class VilleSQL extends SQL {

    public static function getByTerm($term) {
        $query = [];
        $query[] = "SELECT";
        $query[] = "'ville' as type,";
        $query[] = "v.id as id,";
        $query[] = "v.nom as ville,";
        $query[] = "d.nom as departement,";
        $query[] = "r.nom as region,";
        $query[] = "p.nom as pays,";
        $query[] = self::relevance("v.nom", $term) . " AS relevance_nom";
        $query[] = "FROM ville v";
        $query[] = "JOIN departement d ON v.departement_id = d.id";
        $query[] = "JOIN region r ON d.region_id = r.id";
        $query[] = "JOIN pays p ON r.pays_id = p.id";
        $query[] = "WHERE";
        $query[] = "v.nom LIKE :nom";
        $query[] = "ORDER BY";
        $query[] = "relevance_nom ASC";
        $query[] = "LIMIT 0, 15";

        return implode(' ', $query);
    }

}
