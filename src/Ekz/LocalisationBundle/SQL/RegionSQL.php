<?php

namespace Ekz\LocalisationBundle\SQL;

use Ekz\LocalisationBundle\SQL\SQL;

class RegionSQL extends SQL {

    public static function getByTerm($term) {
        $query = [];
        $query[] = "SELECT";
        $query[] = "'region' as type,";
        $query[] = "r.id as id,";
        $query[] = "r.nom as region,";
        $query[] = "p.nom as pays,";
        $query[] = self::relevance("r.nom", $term) . " AS relevance_nom";
        $query[] = "FROM region r ";
        $query[] = "JOIN pays p ON r.pays_id = p.id";
        $query[] = "WHERE";
        $query[] = "r.nom LIKE :nom";
        $query[] = "ORDER BY";
        $query[] = "relevance_nom ASC";
        $query[] = "LIMIT 0, 15";

        return implode(' ', $query);
    }

}
