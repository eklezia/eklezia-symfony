<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Ekz\LocalisationBundle\SQL;

/**
 * Description of SQL
 *
 * @author gwennael
 */
abstract class SQL {

    private static $aEscapeFrom = array('\\', "\0", "\n", "\r", "'", '"', "\x1a");
    private static $aEscapeTo = array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z');
    
    public static function relevance($property, $value) {
        $sValue = self::escape($value);
        $query = [];
        $query[] = "CASE";
        $query[] = "WHEN {$property} LIKE '{$sValue}' THEN 1";
        $query[] = "WHEN {$property} LIKE '{$sValue}%' THEN 2";
        $query[] = "WHEN {$property} LIKE '%{$sValue}%' THEN 3";
        $query[] = "END";

        return implode(' ', $query);
    }

    private static function escape($value) {
        if (is_array($value))
            return array_map(__METHOD__, $value);

        if (!empty($value) && is_string($value)) {
            return str_replace(self::$aEscapeFrom, self::$aEscapeTo, $value);
        }

        return $value;
    }

}
