<?php

namespace Ekz\LocalisationBundle\SQL;

use Ekz\LocalisationBundle\SQL\SQL;

class DepartementSQL extends SQL {

    public static function getByTerm($term) {
        $query = [];
        $query[] = "SELECT";
        $query[] = "'departement' as type,";
        $query[] = "d.id as id,";
        $query[] = "d.nom as departement,";
        $query[] = "r.nom as region,";
        $query[] = "p.nom as pays,";
        $query[] = self::relevance("d.nom", $term) . " AS relevance_nom";
        $query[] = "FROM departement d";
        $query[] = "JOIN region r ON d.region_id = r.id";
        $query[] = "JOIN pays p ON r.pays_id = p.id";
        $query[] = "WHERE";
        $query[] = "d.nom LIKE :nom";
        $query[] = "ORDER BY";
        $query[] = "relevance_nom ASC";
        $query[] = "LIMIT 0, 15";

        return implode(' ', $query);
    }

}
