<?php

namespace Ekz\InformationBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PageForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre', 'text', [
                'attr' => [
                    'class' => 'form_control',
                    'placeholder' => 'Titre de la page ...'
                ]
            ])
            ->add('slug', 'text', [
                'attr' => [
                    'class' => 'form_control',
                    'placeholder' => 'Titre de la page ...'
                ]
            ])
            ->add('contenu', 'textarea', [
                'attr' => [
                    'data-editor' => true,
                    'class' => 'form_control',
                    'placeholder' => 'Contenu de la page ...'
                ]
            ])
            ->add('connexion', 'checkbox', [
                'label' => 'Connexion obligatoire',
                'attr' => [
                    'class' => 'form_control',
                    'placeholder' => 'Titre de la page ...'
                ]
            ])
            ->add('imageCouverture', 'mediatheque')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\InformationBundle\Entity\Page',
            'attr' => [
                'novalidate' => true
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'ekz_informationbundle_page';
    }
}
