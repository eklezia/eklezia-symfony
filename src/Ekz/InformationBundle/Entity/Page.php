<?php

namespace Ekz\InformationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Page
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="PageRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Page
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255)
     */
    private $slug;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="boolean", length=100)
     */
    private $connexion;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     */
    private $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="image_couverture", type="string", length=255, nullable=true)
     * @Assert\NotBlank(message = "Veuillez sélectionner une image de couverture")
     * @Assert\Image(
     *      allowSquare = false,
     *      allowLandscape = true,
     *      allowPortrait = false,
     *      minWidth = 1024, minHeight = 768
     * )
     */
    private $imageCouverture;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Page
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Page
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Page
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }
    
    /**
     * Set connexion
     *
     * @param string $connexion
     *
     * @return Page
     */
    public function setConnexion($connexion)
    {
        $this->connexion = $connexion;

        return $this;
    }

    /**
     * Get connexion
     *
     * @return string
     */
    public function getConnexion()
    {
        return $this->connexion;
    }
    
    /**
     * Set imageCouverture
     *
     * @param string $imageCouverture
     *
     * @return Page
     */
    public function setImageCouverture($imageCouverture)
    {
        if (null !== $imageCouverture) {
            $this->imageCouverture = $imageCouverture;
        }
        
        return $this;
    }

    /**
     * Get imageCouverture
     *
     * @return string
     */
    public function getImageCouverture()
    {
        return $this->imageCouverture;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFile() {
        if ($this->imageCouverture instanceof UploadedFile) {
            $fileName = 'page-' . md5(uniqid()) . '.' . $this->imageCouverture->guessExtension();
            $dir = 'media/cms/page/';
            $this->imageCouverture->move($dir, $fileName);
            $this->setImageCouverture('/' . $dir . $fileName);
        }
    }
}
