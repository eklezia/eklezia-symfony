<?php

namespace Ekz\ProjetBundle\Search;

use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Session\Session;

class ProjetSearchService {

    private $session;
    private $titre;
    private $thematiques;
    private $localisation;

    public function __construct(Session $session, EntityManager $manager) {
        $this->session = $session;
        $this->titre = $this->session->get('titre');
        $this->thematiques = $this->session->get('thematiques');
        $this->localisation = $this->session->get('localisation');

        if (null === $this->thematiques || empty($this->thematiques)) {
            foreach ($manager->getRepository('EkzProjetBundle:Thematique')->findAll() as $thematique) {
                $this->thematiques[] = $thematique->getId();
            }
        }
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getThematiques() {
        return $this->thematiques;
    }

    public function getLocalisation() {
        return $this->localisation;
    }

    public function setTitre($titre) {
        $this->session->set('titre', $titre);
    }

    public function setThematiques($thematiques) {
        $this->session->set('thematiques', $thematiques);
    }

    public function setLocalisation($localisation) {
        $this->session->set('localisation', $localisation);
    }

}
