<?php

namespace Ekz\ProjetBundle\Service;

use Doctrine\ORM\EntityManager;
use Ekz\ProjetBundle\Entity\VoteContre;
use Ekz\ProjetBundle\Entity\VoteContreInactif;
use Ekz\ProjetBundle\Entity\VotePour;
use Ekz\ProjetBundle\Entity\VotePourInactif;
use Ekz\UtilisateurBundle\Entity\Utilisateur;

class VoteRedefinition {

    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function execute(Utilisateur $user) {
        $this->executeActif2Inactif($user);
        $this->executeInactif2Actif($user);
        $this->em->flush();
    }

    private function executeActif2Inactif($user) {
        $aVote = $this->em->getRepository('EkzProjetBundle:Vote')->findNotInProjetLocalise($user);

        foreach ($aVote as $vote) {
            switch (true) {
                case $vote instanceof VotePour:
                    $oNewVote = new VotePourInactif();
                    $oNewVote->setUtilisateur($user);
                    $oNewVote->setProjet($vote->getProjet());
                    $this->em->persist($oNewVote);
                    break;
                case $vote instanceof VoteContre:
                    $oNewVote = new VoteContreInactif();
                    $oNewVote->setUtilisateur($user);
                    $oNewVote->setProjet($vote->getProjet());
                    $this->em->persist($oNewVote);
                    break;
            }
            $this->em->remove($vote);
        }
    }

    private function executeInactif2Actif($user) {
        $aVote = $this->em->getRepository('EkzProjetBundle:Vote')->findInProjetLocalise($user);
        
        foreach ($aVote as $vote) {
            switch (true) {
                case $vote instanceof VotePourInactif:
                    $oNewVote = new VotePour();
                    $oNewVote->setUtilisateur($user);
                    $oNewVote->setProjet($vote->getProjet());
                    $this->em->persist($oNewVote);
                    break;
                case $vote instanceof VoteContreInactif:
                    $oNewVote = new VoteContre();
                    $oNewVote->setUtilisateur($user);
                    $oNewVote->setProjet($vote->getProjet());
                    $this->em->persist($oNewVote);
                    break;
            }
            $this->em->remove($vote);
        }
    }

}
