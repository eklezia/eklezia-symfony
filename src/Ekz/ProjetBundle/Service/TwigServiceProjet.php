<?php

namespace Ekz\ProjetBundle\Service;

use Doctrine\ORM\EntityManager;
use Ekz\ProjetBundle\Entity\Projet;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Twig_Environment;
use Twig_Extension;
use Twig_SimpleFunction;

/**
 * Description of EluForProjet
 *
 * @author gwennael
 */
class TwigServiceProjet extends Twig_Extension {

    private $em;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }
    
    public function getFunctions() {
        return [
            new Twig_SimpleFunction('is_elu_for_projet', array($this, 'isEluForProjet')),
            new Twig_SimpleFunction('projet_etat_progress', array($this, 'projetEtatProgress'), [
                'is_safe' => array('html'),
                'needs_environment' => true
            ])
        ];
    }

    public function isEluForProjet(Utilisateur $user, Projet $projet) {
        return null !== $this->em->getRepository('EkzProjetBundle:Projet')->findOneForEluById($user, $projet->getId());
    }
    
    public function projetEtatProgress(Twig_Environment $twig, Projet $projet) {
        return $twig->render('EkzProjetBundle:Projet:etat-progression.html.twig', [
            'projet' => $projet
        ]);
    }
    
    public function getName() {
        return 'projet.twig';
    }

}
