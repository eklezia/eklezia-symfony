<?php

namespace Ekz\ProjetBundle\Upload;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ImageService {

    private $tokenStorage;
    private $fileSystem;

    const SIZE = 'size';
    const EXTENSIONS = 'extensions';
    const MIN = 'min';
    const MAX = 'max';
    const WIDTH = 'width';
    const HEIGHT = 'height';

    public function __construct(TokenStorage $tokenStorage) {
        $this->tokenStorage = $tokenStorage;
        $this->fileSystem = new Filesystem();
    }

    public function upload(File $file) {
        $this->clear();
        $this->fileSystem->mkdir('temp' . DIRECTORY_SEPARATOR . $this->getUserId());
        $file->move('temp' . DIRECTORY_SEPARATOR . $this->getUserId(), $file->getClientOriginalName());
    }

    public function clear() {
        $this->fileSystem->remove('temp' . DIRECTORY_SEPARATOR . $this->getUserId());
    }

    private function getUserId() {
        return $this->tokenStorage->getToken()->getUser()->getId();
    }

}
