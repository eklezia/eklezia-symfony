<?php

namespace Ekz\ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use DateTime;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Projet
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="ProjetRepository")
 * @UniqueEntity(fields="slug", message="Un projet existe déjà avec ce slug")
 * @ORM\HasLifecycleCallbacks()
 */
class Projet {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ekz\UtilisateurBundle\Entity\Utilisateur")
     */
    private $createur;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     * @Assert\NotBlank(message = "Veuillez saisir un titre")
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"titre"}, updatable=false, separator="-")
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text")
     * @Assert\NotBlank(message = "Veuillez saisir le contenu de votre projet")
     */
    private $contenu;

    /**
     * @var string
     *
     * @ORM\Column(name="localisation", type="localisation")
     * @Assert\NotBlank(message = "Veuillez indiquer la localisation")
     */
    private $localisation;

    /**
     * @ORM\ManyToOne(targetEntity="Thematique")
     * @Assert\NotBlank(message = "Veuillez sélectionner une thématique")
     */
    private $thematique;

    /**
     * @ORM\Column(name="etat", type="string", length=50)
     */
    private $etat;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_butoire", type="datetime")
     * @Assert\Date()
     * @Assert\NotBlank(message = "Veuillez saisir une date butoir")
     */
    private $dateButoire;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_brouillon", type="boolean")
     */
    private $isBrouillon;

    /**
     * @ORM\OneToMany(targetEntity="VotePour", mappedBy="projet", cascade={"remove", "persist"})
     */
    private $votesPour;

    /**
     * @ORM\OneToMany(targetEntity="VoteContre", mappedBy="projet", cascade={"remove", "persist"})
     */
    private $votesContre;

    /**
     * @ORM\OneToMany(targetEntity="Commentaire", mappedBy="projet", cascade={"remove", "persist"})
     */
    private $commentaires;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_valide", type="boolean")
     */
    private $isValide;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="is_moderated", type="boolean")
     */
    private $isModerated;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_publication", type="datetimetz", nullable=true)
     * @Assert\DateTime()
     */
    private $datePublication;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetimetz")
     * @Assert\DateTime()
     */
    private $dateCreation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_modification", type="datetimetz", nullable=true)
     * @Assert\DateTime()
     */
    private $dateModification;

    /**
     * @ORM\ManyToMany(targetEntity="Ekz\UtilisateurBundle\Entity\Utilisateur", mappedBy="projetsEnFavoris", cascade={"remove", "persist"})
     */
    private $usersFavoriteIt;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     * @Assert\NotBlank(message = "Veuillez sélectionner une image")
     * @Assert\Image(
     *      allowSquare = false,
     *      allowLandscape = true,
     *      allowPortrait = false,
     *      maxSize = 6000000,
     *      minWidth = 1024, minHeight = 768,
     *      maxWidth = 2272, maxHeight = 1704,
     *      groups={"preview"}
     * )
     */
    private $image;

    /**
     * 
     */
    public function __construct() {
        $this->isValide = true;
        $this->isModerated = true;
        $this->isBrouillon = false;
        $this->etat = \Ekz\ProjetBundle\Enum\ProjetEtatEnum::EN_COURS;
        $this->dateCreation = new DateTime();
        $this->datePublication = new DateTime();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return Projet
     */
    public function setTitre($titre) {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre() {
        return $this->titre;
    }

    /**
     * Set slug
     *
     * @param string $slug
     *
     * @return Projet
     */
    public function setSlug($slug) {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Projet
     */
    public function setContenu($contenu) {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu() {
        return $this->contenu;
    }

    /**
     * Set dateButoire
     *
     * @param DateTime $dateButoire
     *
     * @return Projet
     */
    public function setDateButoire($dateButoire) {
        $this->dateButoire = $dateButoire;

        return $this;
    }

    /**
     * Get dateButoire
     *
     * @return DateTime
     */
    public function getDateButoire() {
        return $this->dateButoire;
    }

    /**
     * Set isBrouillon
     *
     * @param boolean $isBrouillon
     *
     * @return Projet
     */
    public function setIsBrouillon($isBrouillon) {
        $this->isBrouillon = $isBrouillon;

        return $this;
    }

    /**
     * Get isBrouillon
     *
     * @return boolean
     */
    public function getIsBrouillon() {
        return $this->isBrouillon;
    }

    /**
     * Set isValide
     *
     * @param boolean $isValide
     *
     * @return Projet
     */
    public function setIsValide($isValide) {
        $this->isValide = $isValide;

        return $this;
    }

    /**
     * Get isValide
     *
     * @return boolean
     */
    public function getIsValide() {
        return $this->isValide;
    }
    
    /**
     * Set isModerated
     *
     * @param boolean $isModerated
     *
     * @return Projet
     */
    public function setIsModerated($isModerated) {
        $this->isModerated = $isModerated;

        return $this;
    }
    
    /**
     * Get isModerated
     *
     * @return boolean
     */
    public function getIsModerated() {
        return $this->isModerated;
    }

    /**
     * Set datePublication
     *
     * @param DateTime $datePublication
     *
     * @return Projet
     */
    public function setDatePublication($datePublication) {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * Get datePublication
     *
     * @return DateTime
     */
    public function getDatePublication() {
        return $this->datePublication;
    }

    /**
     * Set dateCreation
     *
     * @param DateTime $dateCreation
     *
     * @return Projet
     */
    public function setDateCreation($dateCreation) {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return DateTime
     */
    public function getDateCreation() {
        return $this->dateCreation;
    }

    /**
     * Set dateModification
     *
     * @param DateTime $dateModification
     *
     * @return Projet
     */
    public function setDateModification($dateModification) {
        $this->dateModification = $dateModification;

        return $this;
    }

    /**
     * Get dateModification
     *
     * @return DateTime
     */
    public function getDateModification() {
        return $this->dateModification;
    }

    /**
     * Set localisation
     *
     * @param string $localisation
     *
     * @return Projet
     */
    public function setLocalisation($localisation) {
        $this->localisation = $localisation;

        return $this;
    }

    /**
     * Get localisation
     *
     * @return string
     */
    public function getLocalisation() {
        return $this->localisation;
    }

    /**
     * Set createur
     *
     * @param Utilisateur $createur
     *
     * @return Projet
     */
    public function setCreateur(\Ekz\UtilisateurBundle\Entity\Utilisateur $createur = null) {
        $this->createur = $createur;

        return $this;
    }

    /**
     * Get createur
     *
     * @return Utilisateur
     */
    public function getCreateur() {
        return $this->createur;
    }

    /**
     * Set thematique
     *
     * @param Thematique $thematique
     *
     * @return Projet
     */
    public function setThematique(Thematique $thematique = null) {
        $this->thematique = $thematique;

        return $this;
    }

    /**
     * Get thematique
     *
     * @return Thematique
     */
    public function getThematique() {
        return $this->thematique;
    }

    /**
     * Set etat
     *
     * @param string $etat
     *
     * @return Projet
     */
    public function setEtat($etat) {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return string
     */
    public function getEtat() {
        if ($this->etat == \Ekz\ProjetBundle\Enum\ProjetEtatEnum::EN_COURS) {
            if ($this->getDateButoire() <= new \DateTime()) {
                return count($this->getVotesPour()) > count($this->getVotesContre())
                        ? \Ekz\ProjetBundle\Enum\ProjetEtatEnum::ACCEPTE_PAR_UTILISATEURS
                        : \Ekz\ProjetBundle\Enum\ProjetEtatEnum::REFUSE_PAR_UTILISATEURS;
            } else {
                return \Ekz\ProjetBundle\Enum\ProjetEtatEnum::EN_COURS_DE_VOTE;
            }
        }
        return $this->etat;
    }

    /**
     * Get Progression
     * 
     * @return int
     */
    public function getProgression() {
        switch ($this->etat) {
            case \Ekz\ProjetBundle\Enum\ProjetEtatEnum::EN_COURS:
                return $this->getDateButoire() <= new \DateTime() ? (2 / 6) * 100 : (1 / 6) * 100;
            case \Ekz\ProjetBundle\Enum\ProjetEtatEnum::ENVOYE:
                return (3 / 6) * 100;
            case \Ekz\ProjetBundle\Enum\ProjetEtatEnum::ACCEPTE:
            case \Ekz\ProjetBundle\Enum\ProjetEtatEnum::REFUSE:
                return (4 / 6) * 100;
            case \Ekz\ProjetBundle\Enum\ProjetEtatEnum::EN_COURS_DE_REALISATION:
                return (5 / 6) * 100;
            case \Ekz\ProjetBundle\Enum\ProjetEtatEnum::REALISE:
                return (6 / 6) * 100;
        }
    }
    
    /**
     * Add commentaire
     *
     * @param Commentaire $commentaire
     *
     * @return Projet
     */
    public function addCommentaire(Commentaire $commentaire) {
        $this->commentaires[] = $commentaire;

        return $this;
    }

    /**
     * Remove commentaire
     *
     * @param Commentaire $commentaire
     */
    public function removeCommentaire(Commentaire $commentaire) {
        $this->commentaires->removeElement($commentaire);
    }

    /**
     * Get commentaires
     *
     * @return Collection
     */
    public function getCommentaires() {
        return $this->commentaires;
    }

    /**
     * Add usersFavoriteIt
     *
     * @param \Ekz\UtilisateurBundle\ENtity\Utilisateur $usersFavoriteIt
     *
     * @return Projet
     */
    public function addUsersFavoriteIt(\Ekz\UtilisateurBundle\ENtity\Utilisateur $usersFavoriteIt) {
        $this->usersFavoriteIt[] = $usersFavoriteIt;

        return $this;
    }

    /**
     * Remove usersFavoriteIt
     *
     * @param \Ekz\UtilisateurBundle\ENtity\Utilisateur $usersFavoriteIt
     */
    public function removeUsersFavoriteIt(\Ekz\UtilisateurBundle\ENtity\Utilisateur $usersFavoriteIt) {
        $this->usersFavoriteIt->removeElement($usersFavoriteIt);
    }

    /**
     * Get usersFavoriteIt
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsersFavoriteIt() {
        return $this->usersFavoriteIt;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Projet
     */
    public function setImage($image) {
        if (null !== $image) {
            $this->image = $image;
        }

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage() {
        return $this->image;
    }

    /**
     * Add votesPour
     *
     * @param \Ekz\ProjetBundle\Entity\VotePour $votesPour
     *
     * @return Projet
     */
    public function addVotesPour(\Ekz\ProjetBundle\Entity\VotePour $votesPour) {
        $this->votesPour[] = $votesPour;

        return $this;
    }

    /**
     * Remove votesPour
     *
     * @param \Ekz\ProjetBundle\Entity\VotePour $votesPour
     */
    public function removeVotesPour(\Ekz\ProjetBundle\Entity\VotePour $votesPour) {
        $this->votesPour->removeElement($votesPour);
    }

    /**
     * Get votesPour
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotesPour() {
        return $this->votesPour;
    }

    /**
     * Add votesContre
     *
     * @param \Ekz\ProjetBundle\Entity\VoteContre $votesContre
     *
     * @return Projet
     */
    public function addVotesContre(\Ekz\ProjetBundle\Entity\VoteContre $votesContre) {
        $this->votesContre[] = $votesContre;

        return $this;
    }

    /**
     * Remove votesContre
     *
     * @param \Ekz\ProjetBundle\Entity\VoteContre $votesContre
     */
    public function removeVotesContre(\Ekz\ProjetBundle\Entity\VoteContre $votesContre) {
        $this->votesContre->removeElement($votesContre);
    }

    /**
     * Get votesContre
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotesContre() {
        return $this->votesContre;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFile() {
        if ($this->image instanceof UploadedFile) {
            $fileName = 'projet-' . md5(uniqid()) . '.' . $this->image->guessExtension();
            $dir = 'media/projets/';
            $this->image->move($dir, $fileName);
            $this->setImage('/' . $dir . $fileName);
        }
    }

    /**
     * Get votes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVotes() {
        return array_merge($this->votesPour->toArray(), $this->votesContre->toArray());
    }
    
    /**
     * Get votes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNombreVotes() {
        return count($this->votesPour->toArray()) + count($this->votesContre->toArray());
    }

    public function isReaded(\Ekz\UtilisateurBundle\Entity\Utilisateur $oUser = null) {
        if ($this->isValide && $this->isModerated && !$this->isBrouillon) {
            return true;
        }
        
        if (null !== $oUser && $oUser === $this->createur) {
            return true;
        }
        
        return false;
    }
    
}
