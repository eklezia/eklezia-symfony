<?php

namespace Ekz\ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vote
 *
 * @ORM\Entity()
 */
class VoteContreInactif extends Vote {

    public function isContre() {
        return true;
    }

    public function isPour() {
        return false;
    }

}
