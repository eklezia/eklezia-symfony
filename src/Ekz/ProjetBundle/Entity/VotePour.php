<?php

namespace Ekz\ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vote
 *
 * @ORM\Entity()
 */
class VotePour extends Vote
{
    public function isContre() {
        return false;
    }

    public function isPour() {
        return true;
    }
}

