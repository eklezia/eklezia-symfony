<?php

namespace Ekz\ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Validator\Constraints as AppAssert;
use DateTime;

/**
 * Commentaire
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="CommentaireRepository")
 */
class Commentaire {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Ekz\UtilisateurBundle\Entity\Utilisateur")
     */
    private $auteur;

    /**
     * @ORM\ManyToOne(targetEntity="Ekz\ProjetBundle\Entity\Projet", inversedBy="commentaires", cascade={"remove", "persist"})
     */
    private $projet;

    /**
     * @var string
     *
     * @ORM\Column(name="contenu", type="text", nullable=false)
     * @Assert\NotBlank()
     * @AppAssert\BlackList(type="comment")
     */
    private $contenu;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_valide", type="boolean", nullable=false)
     */
    private $isValide;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_validation", type="datetimetz", nullable=false)
     */
    private $datePublication;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_creation", type="datetimetz", nullable=false)
     */
    private $dateCreation;

    public function __construct() {
        $this->setIsValide(true);
        $this->setDatePublication(new DateTime());
        $this->setDateCreation(new DateTime());
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contenu
     *
     * @param string $contenu
     *
     * @return Commentaire
     */
    public function setContenu($contenu)
    {
        $this->contenu = $contenu;

        return $this;
    }

    /**
     * Get contenu
     *
     * @return string
     */
    public function getContenu()
    {
        return $this->contenu;
    }

    /**
     * Set isValide
     *
     * @param boolean $isValide
     *
     * @return Commentaire
     */
    public function setIsValide($isValide)
    {
        $this->isValide = $isValide;

        return $this;
    }

    /**
     * Get isValide
     *
     * @return boolean
     */
    public function getIsValide()
    {
        return $this->isValide;
    }

    /**
     * Set dateCreation
     *
     * @param DateTime $dateCreation
     *
     * @return Commentaire
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;

        return $this;
    }

    /**
     * Get dateCreation
     *
     * @return DateTime
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * Set auteur
     *
     * @param \Ekz\ProjetBundle\Entity\Utilisateur $auteur
     *
     * @return Commentaire
     */
    public function setAuteur(\Ekz\UtilisateurBundle\Entity\Utilisateur $auteur = null)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return \Ekz\ProjetBundle\Entity\Utilisateur
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set projet
     *
     * @param Projet $projet
     *
     * @return Commentaire
     */
    public function setProjet(Projet $projet = null)
    {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return Projet
     */
    public function getProjet()
    {
        return $this->projet;
    }

    /**
     * Set datePublication
     *
     * @param \DateTime $datePublication
     *
     * @return Commentaire
     */
    public function setDatePublication($datePublication)
    {
        $this->datePublication = $datePublication;

        return $this;
    }

    /**
     * Get datePublication
     *
     * @return \DateTime
     */
    public function getDatePublication()
    {
        return $this->datePublication;
    }
}
