<?php

namespace Ekz\ProjetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DateTime;

/**
 * Vote
 *
 * @ORM\Entity(repositoryClass="VoteRepository")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="valeur", type="string")
 * @ORM\DiscriminatorMap({
 *     "POUR" = "VotePour",
 *     "CONTRE" = "VoteContre",
 *     "POUR_INACTIF" = "VotePourInactif",
 *     "CONTRE_INACTIF" = "VoteContreInactif"
 * })
 */
abstract class Vote {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Ekz\UtilisateurBundle\Entity\Utilisateur")
     */
    protected $utilisateur;

    /**
     * @ORM\ManyToOne(targetEntity="Ekz\ProjetBundle\Entity\Projet", inversedBy="votes", cascade={"persist"})
     */
    protected $projet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetimetz")
     */
    protected $date;

    public function __construct() {
        $this->isValide = true;
        $this->setDate(new DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set utilisateur
     *
     * @param string $utilisateur
     *
     * @return Vote
     */
    public function setUtilisateur($utilisateur) {
        $this->utilisateur = $utilisateur;

        return $this;
    }

    /**
     * Get utilisateur
     *
     * @return string
     */
    public function getUtilisateur() {
        return $this->utilisateur;
    }

    /**
     * Set projet
     *
     * @param string $projet
     *
     * @return Vote
     */
    public function setProjet($projet) {
        $this->projet = $projet;

        return $this;
    }

    /**
     * Get projet
     *
     * @return string
     */
    public function getProjet() {
        return $this->projet;
    }

    /**
     * Set valeur
     *
     * @param string $valeur
     *
     * @return Vote
     */
    public function setValeur($valeur) {
        $this->valeur = $valeur;

        return $this;
    }

    /**
     * Get valeur
     *
     * @return string
     */
    public function getValeur() {
        return $this->valeur;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Vote
     */
    public function setDate($date) {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate() {
        return $this->date;
    }

    public abstract function isPour();

    public abstract function isContre();
}
