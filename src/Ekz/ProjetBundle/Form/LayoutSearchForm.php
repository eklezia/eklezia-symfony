<?php

namespace Ekz\ProjetBundle\Form;

use Doctrine\ORM\EntityManager;
use Ekz\LocalisationBundle\Form\Type\ZoneGeographiqueType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LayoutSearchForm extends AbstractType {

    private $manager;

    public function __construct(EntityManager $em) {
        $this->manager = $em;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('thematiques', 'choice', [
                    'required' => false,
                    'empty_value' => 'thématique ...',
                    'choices' => $this->getArrayThematiques(),
                    'multiple' => false
                ])
                ->add('localisation', new ZoneGeographiqueType($this->manager))
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\ProjetBundle\Search\ProjetSearchService'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'recherche_projet';
    }

    private function getArrayThematiques() {
        $aThematique = array();
        
        foreach ($this->manager->getRepository('EkzProjetBundle:Thematique')->findAll() as $thematique) {
            $aThematique[$thematique->getId()] = $thematique->getNom();
        }
        return $aThematique;
    }
}
