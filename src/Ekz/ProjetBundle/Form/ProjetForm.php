<?php

namespace Ekz\ProjetBundle\Form;

use AppBundle\DataTransformer\FileTransformer;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ProjetForm extends AbstractType {

    /**
     *
     * @var Utilisateur
     */
    private $oUser;

    /**
     *
     * @var type 
     */
    private $oParametreService;

    public function __construct($oUser, $oParametreService) {
        $this->oUser = $oUser;
        $this->oParametreService = $oParametreService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titre', 'text', [
                    'attr' => [
                        'placeholder' => 'Le titre de votre projet...'
                    ]
                ])
                ->add('contenu', 'tinymce')
                ->add('localisation', 'localisation', [
                    'attr' => [
                        'data-select' => true
                    ]
                ])
                ->add('thematique', 'entity', [
                    'class' => 'EkzProjetBundle:Thematique',
                    'choice_label' => 'nom',
                    'attr' => [
                        'data-select' => true,
                        'data-mediatheque-condition' => 'thematique'
                    ]
                ])
                ->add('dateButoire', 'date', [
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'placeholder' => 'Fin du projet...',
                        'class' => 'calendar',
                        'data-datepicker' => true,
                        'data-mindate' => $this->oParametreService->get('projet_date_butoire_min')
                    ],
                    'constraints' => new GreaterThanOrEqual($this->oParametreService->get('projet_date_butoire_min')),
                ])
                ->add('image', 'mediatheque')
                ->add('createur', 'current_user')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\ProjetBundle\Entity\Projet'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ekz_projetbundle_projet';
    }

}
