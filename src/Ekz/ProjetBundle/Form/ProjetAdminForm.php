<?php

namespace Ekz\ProjetBundle\Form;

use AppBundle\DataTransformer\FileTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjetAdminForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('titre')
                ->add('slug')
                ->add('contenu', 'tinymce')
                ->add('dateButoire', 'date', [
                    'widget' => 'single_text',
                    'input' => 'datetime',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'placeholder' => 'Fin du projet...',
                        'class' => 'calendar',
                        'data-datepicker' => true
                    ],
                ])
                ->add('image', 'file', [
                    'attr' => [
                        'accept' => 'image/*'
                    ]
                ])
                ->add('thematique', 'entity', [
                    'class' => 'EkzProjetBundle:Thematique',
                    'choice_label' => 'nom',
                    'attr' => [
                        'data-select' => true
                    ]
                ])
        ;
        
        $builder->get('image')->addModelTransformer(new FileTransformer());
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Ekz\ProjetBundle\Entity\Projet'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'ekz_projetbundle_projet';
    }

}
