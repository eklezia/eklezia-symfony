<?php

namespace Ekz\ProjetBundle\Enum;

class ProjetEtatEnum {

    // ---
    // Etat general
    // ---
    
    // Etat de base
    const EN_COURS = 'EN_COURS';
    
    // Le projet a ete envoye a l'elu
    const ENVOYE = 'ENVOYE';
    
    // Le projet a ete accepte par l'elu
    const ACCEPTE = 'ACCEPTE';
    
    // Le projet a ete refuse par l'elu
    const REFUSE = 'REFUSE';
    
    // Le projet est en cours de validation
    const EN_COURS_DE_REALISATION = 'EN_COURS_DE_REALISATION';
    
    // Le projet a ete realise
    const REALISE = 'REALISE';

    // ---
    // Etat detaille
    // ---

    const EN_COURS_DE_VOTE = 'EN_COURS_DE_VOTE';
    const ACCEPTE_PAR_UTILISATEURS = 'ACCEPTE_PAR_UTILISATEURS';
    const REFUSE_PAR_UTILISATEURS = 'REFUSE_PAR_UTILISATEURS';
    
    public static function getAlias($enum) {
        return defined('self::' . $str) ? str_replace('_', '-', strtolower($enum)) : null;
    }
    
    public static function getByAlias($alias) {
        $str = str_replace('-', '_', strtoupper($alias));
        return defined('self::' . $str) ? constant('self::' . $str) : null;
    }
}
