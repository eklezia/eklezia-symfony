<?php

namespace AppBundle\Twig;

use DateTime;
use Doctrine\ORM\EntityManager;
use Ekz\ProjetBundle\Entity\Projet;
use Ekz\ProjetBundle\Enum\ProjetEtatEnum;
use Ekz\UtilisateurBundle\Entity\Utilisateur;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Twig_Extension;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

class Functions extends Twig_Extension {

    private static $router;
    private static $em;
    private static $tokenStorage;

    public function __construct(Router $router, EntityManager $em, TokenStorage $tokenStorage) {
        self::$router = $router;
        self::$em = $em;
        self::$tokenStorage = $tokenStorage;
    }

    public function getFunctions() {
        return array(
            new Twig_SimpleFunction('temps_restant', [$this, 'tempsRestant']),
            new Twig_SimpleFunction('pluralize', [$this, 'getPluralize']),
            new Twig_SimpleFunction('ago', [$this, 'getAgo']),
            new Twig_SimpleFunction('vote', [$this, 'getVote']),
            new Twig_SimpleFunction('is_votable', [$this, 'isVotable']),
            new Twig_SimpleFunction('is_commentable', [$this, 'isCommentable']),
            new Twig_SimpleFunction('img', [$this, 'getImage']),
            new Twig_SimpleFunction('projet_in_favoris', [$this, 'projetInFavoris']),
            new Twig_SimpleFunction('role', [$this, 'userRole']),
        );
    }

    public function getFilters() {
        return array(
            new Twig_SimpleFilter('excerpt', array($this, 'getExcerpt')),
        );
    }
    
    public static function tempsRestant(DateTime $oDate, $prefix = "") {
        $oDate->modify('+1 day');
        
        $oToday = new DateTime();
        if ($oToday->format('YmdHis') > $oDate->format('YmdHis')) {
            return "terminé";
        }

        $interval = $oDate->diff($oToday);

        if ($interval->d > 0)
            return $prefix . self::getPluralize($interval->d, 'jour');
        if ($interval->h > 0)
            return $prefix . self::getPluralize($interval->h, 'heure');
        if ($interval->i > 0)
            return $prefix . self::getPluralize($interval->i, 'minute');
        
        return $prefix . self::getPluralize($interval->s, 'minutes');
    }

    public static function getAgo(DateTime $oDate) {
        $interval = date_create('now')->diff($oDate);

        if ($v = $interval->y >= 1)
            return self::getPluralize($interval->y, 'an');
        if ($v = $interval->m >= 1)
            return $interval->m . ' mois';
        if ($v = $interval->d >= 1)
            return self::getPluralize($interval->d, 'jour');
        if ($v = $interval->h >= 1)
            return self::getPluralize($interval->h, 'heure');
        if ($v = $interval->i >= 1)
            return self::getPluralize($interval->i, 'minute');

        return self::getPluralize($interval->s, 'seconde');
    }

    public static function getPluralize($count, $text) {
        return $count . ' ' . ($count == 1 ? $text : "${text}s");
    }

    public static function getVote($const, $uppercaseToLowercase = true) {
        $sClass = "\Ekz\ProjetBundle\Enum\VoteValeurEnum";
        return $uppercaseToLowercase ? strtolower(constant("$sClass::$const")) : constant("$sClass::" . strtoupper($const));
    }

    public static function isVotable(Projet $oProjet) {
        $oUser = self::$tokenStorage->getToken()->getUser();

        return in_array($oProjet->getLocalisation(), [
            $oUser->getVillePrincipal(),
            $oUser->getDepartement(),
            $oUser->getRegion()
        ]);
    }

    public static function isCommentable(Projet $oProjet) {
        return true;
    }

    public static function getImage($url) {
        return null !== $url ? self::$router->getContext()->getBaseUrl() . $url : null;
    }

    public static function projetInFavoris(Utilisateur $user, Projet $projet) {
        return self::$em->getRepository('EkzProjetBundle:Projet')->isInFavoritesForUser($user, $projet);
    }

    public static function getExcerpt($value, $length = 50) {
        $str = trim(strip_tags($value));
        return strlen($str) > $length ? substr($str, 0, $length) . '...' : $str;
    }

    public static function userRole(Utilisateur $user) {
        $sMainRole = null;
        $aRoleLabel = [
            'ROLE_USER' => 'Citoyen',
            'ROLE_ADMIN' => 'Administrateur',
            'ROLE_SUPER_ADMIN' => 'Super Administrateur'
        ];

        $aRolePoid = [
            'Citoyen' => 1,
            'Elu' => 2,
            'Administrateur' => 3,
            'Super Administrateur' => 4,
        ];

        foreach ($user->getRoles() as $role) {
            if (null === $sMainRole || $aRolePoid[$aRoleLabel[$role]] > $aRolePoid[$sMainRole]) {
                $sMainRole = $aRoleLabel[$role];
            }
        }

        return $sMainRole;
    }

    public function getName() {
        return 'FunctionsService';
    }

}
