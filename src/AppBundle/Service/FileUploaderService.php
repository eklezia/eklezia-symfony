<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Description of FileUploaderService
 *
 * @author oem
 */
class FileUploaderService {

    private $targetDir;

    public function __construct($webDir, $targetDir) {
        $this->targetDir = $webDir . $targetDir;
    }

    public function upload(UploadedFile $file) {
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $file->move($this->targetDir, $fileName);
        return $fileName;
    }

}
