<?php

namespace AppBundle\Service;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * Description of SerializerService
 *
 * @author oem
 */
class SerializerService {
    
    private $serializer;


    public function __construct() {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($normalizers, $encoders);
    }
    
    public function serialize($data, $format = null) {
        return $this->serializer->serialize($data, $format);
    }
    
    public function normalize($data, $format = null, $context = array()) {
        return $this->serializer->normalize($data, $format, $context);
    }
}
