<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use Twig_Extension;
use Twig_SimpleFunction;

class HardDiskService extends Twig_Extension {

    private $em;
    private $disk_free_space;
    private $disk_total_space;
    private $bdd_used_space;

    public function __construct(EntityManager $em) {
        $this->em = $em;
    }

    public function getFunctions() {
        return array(
            new Twig_SimpleFunction('disk_available_space', [$this, 'getAvailableSpace']),
            new Twig_SimpleFunction('disk_total_space', [$this, 'getTotalSpace']),
            new Twig_SimpleFunction('disk_used_percent', [$this, 'getUsedPercent']),
            new Twig_SimpleFunction('disk_available_percent', [$this, 'getAvailablePercent']),
        );
    }

    public function getAvailableSpace() {
        return $this->getSymbolByQuantity($this->diskFreeSpace());
    }

    public function getTotalSpace() {
        return $this->getSymbolByQuantity($this->diskTotalSpace());
    }

    public function getUsedPercent() {
        return round(($this->diskFreeSpace() * 100) / $this->diskTotalSpace(), 0);
    }

    public function getAvailablePercent() {
        return 100 - $this->getUsedPercent();
    }

    public function getDatabaseUsedSpace() {
        return $this->getSymbolByQuantity($this->bddUsedSpace());
    }

    private function diskFreeSpace() {
        if (null === $this->disk_free_space) {
            $this->disk_free_space = disk_free_space('/');
        }
        return $this->disk_free_space;
    }

    private function diskTotalSpace() {
        if (null === $this->disk_total_space) {
            $this->disk_total_space = disk_total_space('/');
        }
        return $this->disk_total_space;
    }

    private function bddUsedSpace() {
        if (null === $this->bdd_used_space) {

            $oConnection = $this->em->getConnection();
            $stmt = $oConnection->prepare("SHOW TABLE STATUS");
            $stmt->execute();
            $this->bdd_used_space = 0;
            while ($row = $stmt->fetch()) {
                $this->bdd_used_space += $row["Data_length"] + $row["Index_length"];
            }
        }
        return $this->bdd_used_space;
    }

    private function getSymbolByQuantity($bytes) {
        $symbols = array('Octets', 'Ko', 'Mo', 'Go', 'To', 'Po', 'Eo', 'Zo', 'Yo');
        $exp = floor(log($bytes) / log(1024));
        return number_format($bytes / pow(1024, floor($exp)), 2, ',', ' ') . ' ' . $symbols[$exp];
    }

    public function getName() {
        return 'app.harddisk';
    }

}
