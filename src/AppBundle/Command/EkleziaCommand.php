<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of FixtureCommand
 *
 * @author gjean
 */
class EkleziaCommand extends ContainerAwareCommand
{

    const COMMENT_COLOR = 'magenta';

    protected function configure()
    {
        $this
            ->setName('eklezia:init')
            ->setDescription("Initialisation de l'application Eklezia")
            ->addOption('force', null, InputOption::VALUE_NONE, 'si actif, lance l\'initialisation ((securite pour mise en prod)')
            ->addOption('test', null, InputOption::VALUE_NONE, 'si actif, ajout les fixtures de test')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->isProd() || $input->getOption('force')) {

            ini_set('memory_limit', -1);

            $this->dropDatabase($output);
            $this->createDatabase($output);
            $this->executeMigration($output);
            $this->executeInitialFixture($output);

            if ($input->getOption('test')) {
                $this->executeTestFixture($output);
            }
        } else {
            $output->writeln('<error>ATTENTION:</error> This operation should not be executed in a production environment.');
            $output->writeln('');
            $output->writeln(sprintf('<info>Would init the database.</info>'));
            $output->writeln('Please run the operation with --force to execute');
            $output->writeln('<error>All data will be lost!</error>');
        }
    }

    private function dropDatabase(OutputInterface $output) {
        $command = $this->getApplication()->find('doctrine:database:drop');
        $argument = new ArrayInput(['--force' => true]);
        $command->run($argument, $output);
    }

    private function createDatabase(OutputInterface $output) {
        $command = $this->getApplication()->find('doctrine:database:create');
        $argument = new ArrayInput([]);
        $command->run($argument, $output);
    }

    private function executeMigration(OutputInterface $output) {
        $command = $this->getApplication()->find('doctrine:schema:create');
        $argument = new ArrayInput([]);
        $argument->setInteractive(false);
        $command->run($argument, $output);
    }

    private function executeInitialFixture(OutputInterface $output) {
        $output->writeln("");
        $output->writeln("<fg=" . self::COMMENT_COLOR . ">Ajout des donnees initial</>");

        $command = $this->getApplication()->find('doctrine:fixtures:load');
        $argument = new ArrayInput([
            '--fixtures' => 'src/Ekz/FixturesBundle/DataFixtures/ORM/initial-data',
            '--append' => true
        ]);
        $command->run($argument, $output);
    }

    private function executeTestFixture(OutputInterface $output) {
        $output->writeln("");
        $output->writeln("<fg=" . self::COMMENT_COLOR . ">Ajout du jeu de test</>");

        $command = $this->getApplication()->find('doctrine:fixtures:load');
        $argument = new ArrayInput([
            '--fixtures' => 'src/Ekz/FixturesBundle/DataFixtures/ORM/test',
            '--append' => true
        ]);
        $command->run($argument, $output);
    }

    private function isProd() {
        return isset($_SERVER['PRODUCTION']) && $_SERVER['PRODUCTION'] == 'true';
    }
}
