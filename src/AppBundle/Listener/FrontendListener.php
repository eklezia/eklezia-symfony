<?php

namespace AppBundle\Listener;

use AppBundle\Controller\FrontendController;
use AppBundle\Controller\LayoutController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;

class FrontendListener {
    
    private $container;
    
    public function __construct($oServiceContainer) {
        $this->container = $oServiceContainer;
    }
    
    public function onKernelController(FilterControllerEvent $event) {
        $aControllerCallable = $event->getController();
        $this->isAuthenticatedRemembered($aControllerCallable);
        $this->isNotAuthenticatedRemembered($aControllerCallable);
    }
    
    /**
     * Si la personne est authentifie, mais qu'elle appelle le controller "LayoutController"
     * on la redirige vers la route "app_frontend_home"
     * @param type $aControllerCallable
     * @throws HttpException
     */
    private function isAuthenticatedRemembered($aControllerCallable) {
        $oController = $aControllerCallable[0];

        if ($oController instanceof LayoutController) {
            if(null !== $oController->getUser()) {
                throw new HttpException(307, null, null, [
                    'Location' => $oController->generateUrl('app_frontend_home')
                ]);
            }
        }
    }
    
    /**
     * Si la personne n'est pas authentifie, mais qu'elle appelle le controller "FrontendController"
     * on la redirige vers la route "app_layout_home"
     * @param type $aControllerCallable
     * @throws HttpException
     */
    private function isNotAuthenticatedRemembered($aControllerCallable) {
        $oController = $aControllerCallable[0];

        if ($oController instanceof FrontendController) {
            if(null === $oController->getUser()) {
                throw new HttpException(307, null, null, [
                    'Location' => $oController->generateUrl('app_layout_home')
                ]);
            }
        }
    }
}
