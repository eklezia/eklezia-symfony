<?php

namespace AppBundle\Enum;

abstract class Enum {

    public static function getKeys() {
        return array_keys(self::getConstants());
    }

    public static function getConstants() {
        $class = new ReflectionClass(get_called_class());
        return $class->getConstants();
    }

    public static function isValidValue($value) {
        $valid = false;
        foreach (self::getConstants() as $constantValue) {
            if ($constantValue == $value) {
                $valid = true;
                break;
            }
        }
        return $valid;
    }

    public static function isValidKey($key) {
        $valid = false;
        foreach (array_keys(self::getConstants()) as $constantKey) {
            if ($constantKey == strtoupper($key)) {
                $valid = true;
                break;
            }
        }
        return $valid;
    }

    public static function getValue($key) {
        $value = null;
        foreach (self::getConstants() as $constantKey => $constantValue) {
            if ($constantKey == strtoupper($key)) {
                $value = $constantValue;
                break;
            }
        }
        return $value;
    }

}
