<?php

namespace AppBundle\Enum\Data;

use AppBundle\Enum\Enum;

class RoleEnum extends Enum {

    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';

}
