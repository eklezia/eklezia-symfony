<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Description of Honneur
 *
 * @author gwennael
 */
class HonneurValidator extends ConstraintValidator {

    public function validate($value, Constraint $constraint) {
        if (strcasecmp($value, $constraint->value) !== 0) {
            $this->context->buildViolation($constraint->message)
                    ->setParameter('%string%', $value)
                    ->addViolation();
        }
    }

}
