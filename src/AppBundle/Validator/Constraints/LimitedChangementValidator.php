<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Entity\Log\LogChangementVille;
use DateTime;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Description of LogValidator
 *
 * @author gjean
 */
class LimitedChangementValidator extends ConstraintValidator
{
    private $em;
    private $limit = 3;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function validate($value, Constraint $constraint)
    {
        $entity  = $this->context->getObject();
        $oldData = $this->em->getUnitOfWork()->getOriginalEntityData($entity);

        if ($this->valueHasChanged($entity, $oldData)) {
            $logs = $this->getLogs($constraint);

            if (count($logs) >= $this->limit) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('%type%', $constraint->type)
                    ->addViolation();
            } else {
                $entity->addLogsChangementVille(new LogChangementVille());
            }
        }
    }

    private function getLogs(Constraint $constraint)
    {
        return $this->em->getRepository($constraint->logEntity)->findLast(new DateTime('-1 month'), $this->limit);
    }

    private function valueHasChanged($entity, $oldData)
    {
        $propertyName = $this->context->getPropertyName();
        $method       = "get".ucfirst($propertyName);
        return $oldData[$propertyName] !== $entity->$method();
    }

}