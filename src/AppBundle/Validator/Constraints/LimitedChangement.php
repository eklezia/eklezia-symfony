<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Description of Log
 *
 * @author gjean
 *
 * @Annotation
 */
class LimitedChangement extends Constraint
{
    public $type;

    public $logEntity;

    public $message = 'Vous avez déjà changé de %type% récemment.';

    public function validatedBy()
    {
        return get_class($this).'Validator';
    }

}