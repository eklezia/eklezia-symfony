<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class BlackList extends Constraint {
    
    public $type;
    public $message = "Votre %type% ne peut pas être publié car il comporte un ou plusieurs mots interdits. L'utilisation d'insultes, d'injures et de mots interdits par la Loi française n'est pas autorisée sur le site. Merci de rester cordial(e) et respectueux(-se).";
    
    public function __construct($data) {
        $this->type = $data['type'];
    }
    
    public function validatedBy() {
        return 'black_list_validator';
    }
}
