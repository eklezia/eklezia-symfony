<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Honneur extends Constraint {
    
    public $value;
    public $message = 'The string "%string%" is invalide. please read';
    
    public function __construct($data) {
        $this->value = $data['value'];
    }
    
}
