<?php

namespace AppBundle\Validator\Constraints;

use Ekz\ParametreBundle\Service\ParametreService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Description of Honneur
 *
 * @author gwennael
 */
class BlackListValidator extends ConstraintValidator {

    private $parametreService;

    public function __construct(ParametreService $parametreService) {
        $this->parametreService = $parametreService;
    }

    public function validate($value, Constraint $constraint) {
        $str = ' ' . preg_replace("/[^a-zA-Z0-9]/", ' ', $value) . ' ';
        if (preg_match($this->getRegex(), $str)) {
            $this->context->buildViolation($constraint->message)
                    ->setParameter('%type%', $constraint->type)
                    ->addViolation();
        }
    }

    private function getRegex() {
        $aWord = array_map('trim', explode(',', $this->parametreService->get('blacklist_mot_regex')));
        return "/ " . implode(' | ', $aWord) . " /i";
    }

}
