<?php

namespace AppBundle\Form\Type;

use AppBundle\DataTransformer\MediathequeTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Description of MediathequeType
 *
 * @author oem
 */
class MediathequeType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('mediatheque-default', 'hidden', [
                    'label' => false,
                    'attr' => [
                        'data-mediatheque' => true
                    ]
                ])
                ->add('mediatheque-custom', 'file', [
                    'label' => false,
                    'attr' => [
                        'accept' => 'image/*'
                    ]
                ])
        ;
        
        $builder->addModelTransformer(new MediathequeTransformer());
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'compound' => true
        ));
    }

    public function getName() {
        return 'mediatheque';
    }
    
}
