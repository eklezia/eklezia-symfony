<?php

namespace AppBundle\Form;

use AppBundle\DataTransformer\FileTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MediaForm extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('nom')
                ->add('path', 'file', [
                    'attr' => [
                        'accept' => 'image/*'
                    ]
                ])
                ->add('thematique', 'entity', [
                    'class' => 'EkzProjetBundle:Thematique',
                    'choice_label' => 'nom',
                    'attr' => [
                        'data-select' => true
                    ]
                ])
        ;
        
        $builder->get('path')->addModelTransformer(new FileTransformer());
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Media',
            'attr' => [
                'novalidate' => true
            ]
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'appbundle_media';
    }

}
