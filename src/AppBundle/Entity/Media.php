<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Media
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="AppBundle\Entity\MediaRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Media
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"global"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100)
     * @Groups({"global"})
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     * @Assert\Image(
     *      allowSquare = false,
     *      allowLandscape = true,
     *      allowPortrait = false,
     *      maxSize = 6000000,
     *      minWidth = 1024, minHeight = 768, 
     *      maxWidth = 2272, maxHeight = 1704, 
     *      groups={"preview"}
     * )
     * @Groups({"global"})
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="Ekz\ProjetBundle\Entity\Thematique")
     * @Groups({"global"})
     */
    private $thematique;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Media
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return Media
     */
    public function setPath($path)
    {
        if (null !== $path) {
            $this->path = $path;
        }

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Set thematique
     *
     * @param \stdClass $thematique
     *
     * @return Media
     */
    public function setThematique($thematique)
    {
        $this->thematique = $thematique;

        return $this;
    }

    /**
     * Get thematique
     *
     * @return \stdClass
     */
    public function getThematique()
    {
        return $this->thematique;
    }
    
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function uploadFile() {
        if ($this->path instanceof UploadedFile) {
            $fileName = 'media-' . md5(uniqid()) . '.' . $this->path->guessExtension();
            $dir = 'media/mediatheque/';
            $this->path->move($dir, $fileName);
            $this->setPath('/' . $dir . $fileName);
        }
    }
}

