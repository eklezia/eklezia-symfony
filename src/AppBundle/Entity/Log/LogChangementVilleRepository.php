<?php

namespace AppBundle\Entity\Log;

use Doctrine\ORM\EntityRepository;
use DateTime;

/**
 * LogChangementVilleRepository
 */
class LogChangementVilleRepository extends EntityRepository
{

    public function findLast(DateTime $date, $limit)
    {
        return $this->createQueryBuilder('l')
            ->where('l.date >= :date')
            ->setParameter('date', $date)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

}