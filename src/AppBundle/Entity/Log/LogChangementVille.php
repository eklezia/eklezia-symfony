<?php

namespace AppBundle\Entity\Log;

use Doctrine\ORM\Mapping as ORM;

/**
 * LogChangementVille
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="LogChangementVilleRepository")
 */
class LogChangementVille extends Log
{
    /**
     * @ORM\ManyToOne(targetEntity="Ekz\UtilisateurBundle\Entity\Utilisateur", inversedBy="logsChangementVille")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $user;

    public function __construct()
    {
        parent::__construct();
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

}