<?php

namespace AppBundle\Controller\Backend\CMS;

use AppBundle\Controller\BackendController;
use Ekz\InformationBundle\Entity\Page;
use Ekz\InformationBundle\Form\PageForm;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class PageController extends BackendController {

    public function homeAction() {
        return $this->render('AppBundle:Backend:cms-page-home.html.twig');
    }

    public function ajouterAction(Request $request) {
        $page = new Page();
        
        $form = $this->createForm(new PageForm(), $page);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($page);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', "Votre page a bien été modifiée");
            return $this->redirectToRoute('app_backend_cms_page');
        }

        return $this->render('AppBundle:Backend:cms-page-formulaire.html.twig', [
                    'form' => $form->createView()
        ]);
    }
    
    public function modifierAction(Request $request, Page $page) {
        $form = $this->createForm(new PageForm(), $page);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($page);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', "Votre page a bien été modifiée");
            return $this->redirectToRoute('app_backend_cms_page');
        }

        return $this->render('AppBundle:Backend:cms-page-formulaire.html.twig', [
                    'form' => $form->createView()
        ]);
    }
    
    public function supprimerAction(Request $request, Page $page) {
        $this->getDoctrine()->getManager()->remove($page);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', "Votre page a bien été supprimé");
        return $this->redirectToRoute('app_backend_cms_page');
    }

}
