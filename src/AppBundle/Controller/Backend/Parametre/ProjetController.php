<?php

namespace AppBundle\Controller\Backend\Parametre;

use AppBundle\Controller\BackendController;
use Ekz\ParametreBundle\Form\ParametreProjetForm;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of ProjetController
 *
 * @author gwennael
 */
class ProjetController extends BackendController {
    
    public function indexAction(Request $request) {
        $oForm = $this->createForm(new ParametreProjetForm($this->get('ekz_parametre')));
        
        if ($oForm->handleRequest($request)->isValid()) {
            foreach ($oForm->getData() as $key => $data) {
                $this->get('ekz_parametre')->set($key, $data);
            }
            $this->addFlash('success', "Les paramètres ont été modifié");
            return $this->redirectToRoute('app_backend_parametre_projet');
        }
        
        return $this->render('AppBundle:Backend:parametre-projet.html.twig', [
            'form' => $oForm->createView()
        ]);
    }
    
}
