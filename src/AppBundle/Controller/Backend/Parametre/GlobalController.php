<?php

namespace AppBundle\Controller\Backend\Parametre;

use AppBundle\Controller\BackendController;
use Ekz\ParametreBundle\Form\ParametreGlobalForm;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class GlobalController extends BackendController {
    
    public function indexAction(Request $request) {
        $oForm = $this->createForm(new ParametreGlobalForm($this->get('ekz_parametre')));
        
        if ($oForm->handleRequest($request)->isValid()) {
            foreach ($oForm->getData() as $key => $data) {
                $this->get('ekz_parametre')->set($key, $data);
            }
            $this->addFlash('success', "Les paramètres ont été modifié");
            return $this->redirectToRoute('app_backend_parametre');
        }
        
        return $this->render('AppBundle:Backend:parametre-global.html.twig', [
            'form' => $oForm->createView()
        ]);
    }
}
