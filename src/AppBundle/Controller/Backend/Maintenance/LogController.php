<?php

namespace AppBundle\Controller\Backend\Maintenance;

use AppBundle\Controller\BackendController;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class LogController extends BackendController {

    public function indexAction() {
        return $this->render('AppBundle:Backend:maintenance-log.html.twig');
    }

    public function trashAction() {
        if ($this->isGranted('ROLE_SUPER_ADMIN')) {
            $mode = $this->container->get('kernel')->getEnvironment();
            $file = $this->get('kernel')->getRootDir() . '/logs/' . $mode . '.log';

            $fs = new Filesystem();

            $fs->chmod($file, 0777);
            $fs->remove($file);

            $this->addFlash('success', "Les logs ont été supprimé avec succès");
        } else {
            $this->addFlash('info', "Seul le super administrateur peut supprimer les logs");
        }
        
        return $this->redirectToRoute('app_backend_maintenance_log');
    }

}
