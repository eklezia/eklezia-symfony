<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Controller\BackendController;
use AppBundle\Entity\Media;
use AppBundle\Form\MediaForm;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of MediathequeController
 *
 * @author oem
 */
class MediathequeController extends BackendController {
    
    public function indexAction() {
        $medias = $this->getDoctrine()->getRepository('AppBundle:Media')->findAll();
        return $this->render('AppBundle:Backend:mediatheque-home.html.twig', [
            'medias' => $medias
        ]);
    }
    
    public function ajouterAction(Request $request) {
        $media = new Media();
        
        $form = $this->createForm(new MediaForm(), $media);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($media);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', "Le média a bien été enregistré");
            return $this->redirectToRoute('app_backend_mediatheque');
        }
        
        return $this->render('AppBundle:Backend:mediatheque-formulaire.html.twig', [
            'form' => $form->createView(),
            'media' => $media
        ]);
    }
    
    public function modifierAction(Request $request, Media $media) {        
        $form = $this->createForm(new MediaForm(), $media);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($media);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', "Le média a bien été enregistré");
            return $this->redirectToRoute('app_backend_mediatheque');
        }
        
        return $this->render('AppBundle:Backend:mediatheque-formulaire.html.twig', [
            'form' => $form->createView(),
            'media' => $media
        ]);
    }
}
