<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Controller\BackendController;
use Ekz\UtilisateurBundle\Entity\Utilisateur;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class UtilisateurController extends BackendController {
    
    public function homeAction() {
        return $this->render('AppBundle:Backend:utilisateur-home.html.twig');
    }
    
    public function voirAction(Utilisateur $oUtilisateur) {
        return $this->render('AppBundle:Backend:utilisateur-voir.html.twig', [
            'utilisateur' => $oUtilisateur
        ]);
    }
}
