<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Controller\BackendController;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class CommentaireController extends BackendController {
    
    public function homeAction() {
        return $this->render('AppBundle:Backend:commentaire-home.html.twig');
    }
    
}
