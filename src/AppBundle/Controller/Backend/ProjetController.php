<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Controller\BackendController;
use Ekz\ProjetBundle\Entity\Projet;
use Ekz\ProjetBundle\Form\ProjetAdminForm;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class ProjetController extends BackendController {

    public function homeAction() {
        return $this->render('AppBundle:Backend:projet-home.html.twig');
    }

    public function voirAction(Projet $oProjet) {
        return $this->render('AppBundle:Backend:projet-voir.html.twig', [
                    'projet' => $oProjet
        ]);
    }

    public function modifierAction(Request $request, Projet $projet) {
        $form = $this->createForm(new ProjetAdminForm(), $projet);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($projet);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', "Le projet a bien été enregistré");
            return $this->redirectToRoute('app_backend_projet_voir', ['id' => $projet->getId()]);
        }

        return $this->render('AppBundle:Backend:projet-formulaire.html.twig', [
                    'projet' => $projet,
                    'form' => $form->createView()
        ]);
    }

    public function aModererAction() {
        return $this->render('AppBundle:Backend:projet-a-moderer.html.twig');
    }

    public function moderationAction(Projet $projet, $moderation) {
        
        $projetIsValide = (bool) $moderation;
        
        $projet->setIsModerated(true);
        $projet->setIsValide($projetIsValide);
        
        $this->getDoctrine()->getManager()->persist($projet);
        $this->getDoctrine()->getManager()->flush();
        
        $projetIsValide
                ? $this->get('ekz_email.sender')->sendMailProjetModerationValider($projet)
                : $this->get('ekz_email.sender')->sendMailProjetModerationRefuser($projet);
        
        $this->addFlash('success', "Le projet a bien été " . ($projet->getIsValide() ? 'validé' : 'refusé'));
        return $this->redirectToRoute('app_backend_projet_a_moderer');
    }

}
