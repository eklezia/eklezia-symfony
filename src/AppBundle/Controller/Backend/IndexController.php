<?php

namespace AppBundle\Controller\Backend;

use AppBundle\Controller\BackendController;

class IndexController extends BackendController {

    public function indexAction() {
        return $this->redirectToRoute('app_backend_dashboard');
    }

    public function homeAction() {
        return $this->render('AppBundle:Backend:home.html.twig', [
            'nombre_commentaire' => $this->getDoctrine()->getRepository('EkzProjetBundle:Commentaire')->count(),
            'nombre_vote' => $this->getDoctrine()->getRepository('EkzProjetBundle:Vote')->count(),
            'nombre_projet' => $this->getDoctrine()->getRepository('EkzProjetBundle:Projet')->count(),
            'nombre_utilisateur' => $this->getDoctrine()->getRepository('EkzUtilisateurBundle:Utilisateur')->count(),
            'disk_free_space' => $this->get('app.harddisk')->getAvailableSpace(),
            'disk_total_space' => $this->get('app.harddisk')->getTotalSpace(),
            'database_used_space' => $this->get('app.harddisk')->getDatabaseUsedSpace()
        ]);
    }

    public function navbarAction() {
        return $this->render('AppBundle:Backend:menu/navbar.html.twig');
    }

    public function sidebarAction() {
        return $this->render('AppBundle:Backend:menu/sidebar.html.twig');
    }

}
