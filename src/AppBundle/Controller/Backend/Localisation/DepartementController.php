<?php

namespace AppBundle\Controller\Backend\Localisation;

use AppBundle\Controller\BackendController;
use Ekz\LocalisationBundle\Entity\Departement;
use Ekz\LocalisationBundle\Form\DepartementAdminForm;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class DepartementController extends BackendController {
    
    public function homeAction() {
        return $this->render('AppBundle:Backend:departement-home.html.twig');
    }
    
    public function voirAction(Departement $oDepartement) {
        return $this->render('AppBundle:Backend:departement-voir.html.twig', [
            'departement' => $oDepartement
        ]);
    }
    
    public function ajouterAction(Request $request) {
        $departement = new Departement();
        
        $form = $this->createForm(new DepartementAdminForm(), $departement);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($departement);
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', "Le département a bien été enregistré");
            return $this->redirectToRoute('app_backend_departement_voir', ['id' => $departement->getId()]);
        }
        
        return $this->render('AppBundle:Backend:departement-formulaire.html.twig', [
            'departement' => $departement,
            'form' => $form->createView()
        ]);
    }
    
    public function modifierAction(Request $request, Departement $departement) {
        $form = $this->createForm(new DepartementAdminForm(), $departement);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($departement);
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', "Le département a bien été enregistré");
            return $this->redirectToRoute('app_backend_departement_voir', ['id' => $departement->getId()]);
        }
        
        return $this->render('AppBundle:Backend:departement-formulaire.html.twig', [
            'departement' => $departement,
            'form' => $form->createView()
        ]);
    }
}
