<?php

namespace AppBundle\Controller\Backend\Localisation;

use AppBundle\Controller\BackendController;
use Ekz\LocalisationBundle\Entity\Region;
use Ekz\LocalisationBundle\Form\RegionAdminForm;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class RegionController extends BackendController {
    
    public function homeAction() {
        return $this->render('AppBundle:Backend:region-home.html.twig');
    }
    
    public function voirAction(Region $oRegion) {
        return $this->render('AppBundle:Backend:region-voir.html.twig', [
            'region' => $oRegion
        ]);
    }
    
    public function ajouterAction(Request $request) {
        $region = new Region();
        
        $form = $this->createForm(new RegionAdminForm(), $region);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($region);
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', "La région a bien été enregistrée");
            return $this->redirectToRoute('app_backend_region_voir', ['id' => $region->getId()]);
        }
        
        return $this->render('AppBundle:Backend:region-formulaire.html.twig', [
            'region' => $region,
            'form' => $form->createView()
        ]);
    }
    
    public function modifierAction(Request $request, Region $region) {
        $form = $this->createForm(new RegionAdminForm(), $region);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($region);
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', "La région a bien été enregistrée");
            return $this->redirectToRoute('app_backend_region_voir', ['id' => $region->getId()]);
        }
        
        return $this->render('AppBundle:Backend:region-formulaire.html.twig', [
            'region' => $region,
            'form' => $form->createView()
        ]);
    }
}
