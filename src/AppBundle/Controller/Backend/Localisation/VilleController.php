<?php

namespace AppBundle\Controller\Backend\Localisation;

use AppBundle\Controller\BackendController;
use Ekz\LocalisationBundle\Entity\Ville;
use Ekz\LocalisationBundle\Form\VilleAdminForm;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of Projet
 *
 * @author gwennael
 */
class VilleController extends BackendController {
    
    public function homeAction() {
        return $this->render('AppBundle:Backend:ville-home.html.twig');
    }
    
    public function voirAction(Ville $oVille) {
        return $this->render('AppBundle:Backend:ville-voir.html.twig', [
            'ville' => $oVille
        ]);
    }
    
    public function ajouterAction(Request $request) {
        $ville = new Ville();
        
        $form = $this->createForm(new VilleAdminForm(), $ville);

        if ($form->handleRequest($request)->isValid()) {
                        
            $this->getDoctrine()->getManager()->persist($ville);
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', "La ville a bien été enregistrée");
            return $this->redirectToRoute('app_backend_ville_voir', ['id' => $ville->getId()]);
        }
        
        return $this->render('AppBundle:Backend:ville-formulaire.html.twig', [
            'ville' => $ville,
            'form' => $form->createView()
        ]);
    }
    
    public function modifierAction(Request $request, Ville $ville) {
        $form = $this->createForm(new VilleAdminForm(), $ville);

        if ($form->handleRequest($request)->isValid()) {
            $this->getDoctrine()->getManager()->persist($ville);
            $this->getDoctrine()->getManager()->flush();
            
            $this->addFlash('success', "La ville a bien été enregistrée");
            return $this->redirectToRoute('app_backend_ville_voir', ['id' => $ville->getId()]);
        }
        
        return $this->render('AppBundle:Backend:ville-formulaire.html.twig', [
            'ville' => $ville,
            'form' => $form->createView()
        ]);
    }
}
