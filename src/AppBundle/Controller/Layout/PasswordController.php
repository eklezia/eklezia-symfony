<?php

namespace AppBundle\Controller\Layout;

use AppBundle\Controller\LayoutController;
use Symfony\Component\HttpFoundation\Request;

class PasswordController extends LayoutController {

    public function forgottenPasswordAction(Request $request) {
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            
            if ($request->isMethod('post')) {
                $email = $request->request->get('_email');
                
                $user = $this->getDoctrine()->getRepository('EkzUtilisateurBundle:Utilisateur')->findOneBy([
                    'isActif' => true,
                    'email' => $email
                ]);
                
                if (null !== $user) {
                    $sNewPassword = $this->get('utilisateur.encoder')->generatePassword();
                    
                    $this->get('utilisateur.encoder')->setUserPassword($user, $sNewPassword);
                    $this->getDoctrine()->getManager()->persist($user);
                    $this->getDoctrine()->getManager()->flush();
                    
                    $this->get('ekz_email.sender')->sendMailNouveauMotDePasse($user, $sNewPassword);
                    
                    $this->addFlash('success', "Votre mot de passe a été modifié. Vous allez recevoir un mail");
                    return $this->redirectToRoute('app_layout_connexion');
                }
            }
            
            return $this->render('AppBundle:Layout:forgotten-password.html.twig');
        }
        
        return $this->redirectToRoute('app_frontend_home');
    }
}
