<?php

namespace AppBundle\Controller\Layout;

use AppBundle\Controller\LayoutController;
use Ekz\UtilisateurBundle\Form\UtilisateurInscriptionForm;
use Ekz\UtilisateurBundle\Entity\Utilisateur;

class InscriptionController extends LayoutController {

    public function indexAction() {
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $oInscriptionService = $this->get('utilisateur.inscription');
            $oUtilisateur = $oInscriptionService->getEntity() ? : new Utilisateur();
            $oForm = $this->createForm(new UtilisateurInscriptionForm(), $oUtilisateur);

            return $this->render('AppBundle:Layout:inscription.html.twig', [
                'form' => $oForm->createView(),
                'errors' => $oInscriptionService->getErrors()
            ]);
        }
        
        return $this->redirectToRoute('app_frontend_home');
    }

    public function confirmationAction($token) {
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $oUtilisateurRepository = $this->getDoctrine()->getRepository("EkzUtilisateurBundle:Utilisateur");
            $oUtilisateur = $oUtilisateurRepository->findOneBy([
                'isActif' => false,
                'tokenInscription' => $token
            ]);

            if (null !== $oUtilisateur) {
                $oUtilisateur->setIsActif(true);
                $oUtilisateur->setTokenInscription(null);
                $this->getDoctrine()->getManager()->persist($oUtilisateur);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', 'Confirmation validé. Vous pouvez maintenant vous connecter.');

                return $this->redirectToRoute('app_layout_home');
            }

            throw $this->createNotFoundException();
        }
        
        return $this->redirectToRoute('app_frontend_home');
    }
}
