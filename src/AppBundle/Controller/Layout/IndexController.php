<?php

namespace AppBundle\Controller\Layout;

use AppBundle\Controller\LayoutController;
use Ekz\ProjetBundle\Form\LayoutSearchForm;

class IndexController extends LayoutController {

    public function homeAction() {
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $form = $this->createForm(new LayoutSearchForm($this->getDoctrine()->getManager()));
            
            
            
            return $this->render('AppBundle:Layout:home.html.twig', [
                'form' => $form->createView()
            ]);
        }
        
        return $this->redirectToRoute('app_frontend_home');
    }

}
