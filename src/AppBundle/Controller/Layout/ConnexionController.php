<?php

namespace AppBundle\Controller\Layout;

use AppBundle\Controller\LayoutController;

class ConnexionController extends LayoutController {

    public function indexAction() {
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $authenticationUtils = $this->get('security.authentication_utils');

            return $this->render('AppBundle:Layout:connexion.html.twig', [
                        'last_username' => $authenticationUtils->getLastUsername(),
                        'error' => $authenticationUtils->getLastAuthenticationError(),
            ]);
        }
        
        return $this->redirectToRoute('app_frontend_home');
    }

    public function connexionPourProjetAction() {
        if (!$this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $authenticationUtils = $this->get('security.authentication_utils');

            return $this->render('AppBundle:Layout:connexion-pour-projet.html.twig', [
                        'last_username' => $authenticationUtils->getLastUsername(),
                        'error' => $authenticationUtils->getLastAuthenticationError(),
            ]);
        }
        
        return $this->redirectToRoute('app_frontend_home');
    }
}
