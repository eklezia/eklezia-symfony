<?php

namespace AppBundle\Controller\Layout;

use AppBundle\Controller\LayoutController;

class PageController extends LayoutController {

    public function indexAction($slug) {
        $page = $this->getDoctrine()->getRepository('EkzInformationBundle:Page')->findOneBySlug($slug);
        if (null !== $page) {
            return $this->render('AppBundle:Layout:page.html.twig', [
                'page' => $page
            ]);
        }
        throw $this->createNotFoundException('La page d\'information est introuvable');
    }

}
