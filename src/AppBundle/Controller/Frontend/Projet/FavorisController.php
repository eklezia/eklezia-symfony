<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;

class FavorisController extends FrontendController {

    /**
     * Affiche la liste des projet mis en favoris
     * @return type
     */
    public function indexAction() {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
            return $this->render('AppBundle:Frontend:projet-favoris.html.twig', [
                        'projets' => $oProjetRepository->findFavoritesByUser($this->getUser())
            ]);
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier pour visualiser vos favoris");
    }

    /**
     * Permet d'ajouter ou de supprimer un projet des favoris
     * @param type $slug
     * @return type
     */
    public function setAction($slug) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $oProjet = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet')->findOneBySlug($slug);

            $oUser = $this->getUser();

            if (!$oUser->getProjetsEnFavoris()->contains($oProjet)) {
                $oUser->addProjetEnFavoris($oProjet);
                $this->addFlash('success', 'Ce projet a bien été mis en favoris');
            } else {
                $oUser->removeProjetEnFavoris($oProjet);
                $this->addFlash('success', 'Ce projet a bien été supprimé de vos favoris');
            }

            $this->getDoctrine()->getManager()->persist($oUser);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('app_frontend_projets_voir', ['slug' => $oProjet->getSlug()]);
        }
        
        throw $this->createAccessDeniedException("Veuillez vous authentifier pour ajouter ce projet aux favoris");
    }

}
