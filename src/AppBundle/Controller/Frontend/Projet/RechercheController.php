<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;
use Ekz\ProjetBundle\Form\SearchForm;
use Symfony\Component\HttpFoundation\Request;

class RechercheController extends FrontendController {

    public function indexAction(Request $oRequest) {
        $oProjetSearchService = $this->get('projet.search');

        $oForm = $this->createForm(new SearchForm($this->getDoctrine()->getManager()), $oProjetSearchService);

        if ($oForm->handleRequest($oRequest)->isValid()) {
            
            return $this->redirectToRoute('app_frontend_projets_search');
        }

        $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');

        return $this->render('AppBundle:Frontend:projet-recherche.html.twig', [
                    'form' => $oForm->createView(),
                    'projets' => $oProjetRepository->findBySearch($oProjetSearchService)
        ]);
    }

}
