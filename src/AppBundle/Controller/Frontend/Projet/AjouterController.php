<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;
use Ekz\ProjetBundle\Entity\Projet;
use Ekz\ProjetBundle\Form\ProjetForm;
use Symfony\Component\HttpFoundation\Request;

class AjouterController extends FrontendController {

    public function indexAction(Request $oRequest) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($this->getUser()->hasLocalisation()) {
                $oProjet = new Projet();

                $oForm = $this->createForm(new ProjetForm($this->getUser(), $this->get('ekz_parametre')), $oProjet);
                
                if ($oForm->handleRequest($oRequest)->isValid()) {

                    if ($this->get('ekz_parametre')->get('projet_moderation')) {
                        $oProjet->setIsModerated(false);
                        $oProjet->setIsValide(false);
                        $this->get('ekz_email.sender')->sendMailProjetAttenteDeModeration($oProjet);
                    }
                    
                    $this->getDoctrine()->getManager()->persist($oProjet);
                    $this->getDoctrine()->getManager()->flush();

                    $this->get('upload.image.preview')->clear();

                    $this->addFlash('success', "Votre projet a bien été ajouté, il est en cours de validation. Vous recevrez un mail lorsque celui-ci aura été publié");
                    return $this->redirectToRoute('app_frontend_projets');
                }

                return $this->render('AppBundle:Frontend:projet-formulaire.html.twig', [
                            'titre' => 'Créer un projet sur Eklezia',
                            'form' => $oForm->createView()
                ]);
            }

            $this->addFlash('info', "Avant de déposer des projets, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }
        
        throw $this->createAccessDeniedException("Veuillez vous authentifier pour déposer un projet");
    }

}
