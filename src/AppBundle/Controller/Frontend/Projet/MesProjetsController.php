<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;

class MesProjetsController extends FrontendController {

    public function indexAction() {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($this->getUser()->hasLocalisation()) {
                $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
                return $this->render('AppBundle:Frontend:projet-index.html.twig', [
                            'projets' => $oProjetRepository->findByUser($this->getUser())
                ]);
            }

            $this->addFlash('info', "Avant de visualiser vos projets, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier pour visualiser vos projets");
    }

}
