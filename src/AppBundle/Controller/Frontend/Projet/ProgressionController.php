<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;
use AppBundle\Enum\Data\RoleEnum;
use Ekz\ProjetBundle\Enum\ProjetEtatEnum;
use Symfony\Component\HttpFoundation\Request;

class ProgressionController extends FrontendController {

    /**
     * 
     * @param Request $oRequest
     * @param type $etat
     * @return type
     * @throws type
     */
    public function envoyerAction(Request $oRequest, $slug) {
        $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
        $projet = $oProjetRepository->findOneBySlug($slug);
        
        if ($this->getUser() == $projet->getCreateur()) {

            $projet->setEtat(ProjetEtatEnum::getByAlias(ProjetEtatEnum::ENVOYE));
            $this->getDoctrine()->getManager()->persist($projet);
            $this->getDoctrine()->getManager()->flush();

            $receiver = $this->get('app.elu')->getForProjet($projet);
            $this->get('ekz_email.sender')->sendMailProjetElu($projet, $receiver);
            
            $this->addFlash('success', "Le projet a été envoyé à l'élu");
            return $this->redirectToRoute('app_frontend_projets_voir', [
                'slug' => $projet->getSlug()
            ]);
        }

        throw $this->createAccessDeniedException("Seul le dépositaire du projet a accès à cette page");
    }

    /**
     * 
     * @param type $slug
     * @param type $etat
     */
    public function changeEtatAction($slug, $etat) {
        $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
        $projet = $oProjetRepository->findOneForEluBySlug($this->getUser(), $slug);
        
        if ($this->getUser() !== $projet->getCreateur()) {
            throw $this->createAccessDeniedException("Seul les élus ont accès à cette zone");
        }
                
        $projet->setEtat(ProjetEtatEnum::getByAlias($etat));
        $this->getDoctrine()->getManager()->persist($projet);
        $this->getDoctrine()->getManager()->flush();

        $this->addFlash('success', "Le projet a été modifié");
        return $this->redirectToRoute('app_frontend_projets_voir', [
            'slug' => $projet->getSlug()
        ]);
    }

}
