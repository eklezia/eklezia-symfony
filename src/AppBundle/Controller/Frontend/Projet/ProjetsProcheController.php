<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;

class ProjetsProcheController extends FrontendController {

    public function indexAction() {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($this->getUser()->hasLocalisation()) {
                $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
                return $this->render('AppBundle:Frontend:projet-proche.html.twig', [
                            'projetsVille' => $oProjetRepository->findInVilleByUser($this->getUser()),
                            'projetsDepartement' => $oProjetRepository->findInDepartementByUser($this->getUser()),
                            'projetsRegion' => $oProjetRepository->findInRegionByUser($this->getUser())
                ]);
            }
            
            $this->addFlash('info', "Avant de voir les projets proche de chez vous, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier pour visualiser les projets proche");
    }

}
