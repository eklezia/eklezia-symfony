<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;
use Ekz\ProjetBundle\Entity\Commentaire;
use Ekz\ProjetBundle\Form\CommentaireForm;
use Symfony\Component\HttpFoundation\Request;

class VoirController extends FrontendController {

    public function indexAction($slug, Request $oRequest) {

        $oProjet = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet')->findOneBySlug($slug);

        if (null === $oProjet) {
            throw $this->createNotFoundException("Ce projet n'existe pas");
        }
        
        if (!$oProjet->isReaded($this->getUser())) {
            throw $this->createNotFoundException("Ce projet n'est pas valide");
        }
        
        $oCommentaire = new Commentaire();
        $oCommentaire->setProjet($oProjet);

        $oForm = $this->createForm(new CommentaireForm(), $oCommentaire);

        $oVoteRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Vote');

        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $oCommentaire->setAuteur($this->getUser());

            if ($oForm->handleRequest($oRequest)->isValid()) {
                $this->getDoctrine()->getManager()->persist($oCommentaire);
                $this->getDoctrine()->getManager()->flush();

                $this->addFlash('success', "Votre commentaire a bien été publié");
                return $this->redirectToRoute('app_frontend_projets_voir', ['slug' => $slug]);
            }

            $userVote = $oVoteRepository->findOneByProjetAndUser($this->getUser(), $oProjet);
        } else {
            $userVote = null;
        }
        
        return $this->render('AppBundle:Frontend:projet-voir.html.twig', [
                    'projet' => $oProjet,
                    'form' => $oForm->createView(),
                    'userVote' => $userVote,
                    'vote' => $userVote
        ]);

    }

}
