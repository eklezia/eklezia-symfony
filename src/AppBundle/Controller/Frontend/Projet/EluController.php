<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;
use AppBundle\Enum\Data\RoleEnum;
use Ekz\ProjetBundle\Enum\ProjetEtatEnum;
use Symfony\Component\HttpFoundation\Request;

class EluController extends FrontendController {

    /**
     * 
     * @param Request $oRequest
     * @param type $etat
     * @return type
     * @throws type
     */
    public function indexAction(Request $oRequest, $etat) {
        if ($this->isGranted(RoleEnum::ROLE_ELU)) {
            if ($this->getUser()->hasLocalisation()) {

                $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
                $projets = $oProjetRepository->findForEluByEtat($this->getUser(), ProjetEtatEnum::getByAlias($etat));

                return $this->render('AppBundle:Frontend:elu-index.html.twig', [
                            'etat' => $etat,
                            'projets' => $projets
                ]);
            }

            $this->addFlash('info', "Avant de voir les projets, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }

        throw $this->createAccessDeniedException("Seul les élus ont accès à cette zone");
    }

    /**
     * 
     * @param type $slug
     * @param type $etat
     */
    public function changeEtatAction($slug, $etat) {
        if ($this->isGranted(RoleEnum::ROLE_ELU)) {
            if ($this->getUser()->hasLocalisation()) {
                $oProjetRepository = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet');
                $projet = $oProjetRepository->findOneForEluBySlug($this->getUser(), $slug);
                $projet->setEtat(ProjetEtatEnum::getByAlias($etat));
                $this->getDoctrine()->getManager()->persist($projet);
                $this->getDoctrine()->getManager()->flush();
                
                $this->addFlash('success', "Le projet a été modifié");
                return $this->redirectToRoute('app_frontend_projets_voir', [
                    'slug' => $projet->getSlug()
                ]);
            }

            $this->addFlash('info', "Avant de voir les projets, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }
        throw $this->createAccessDeniedException("Seul les élus ont accès à cette zone");
    }

}
