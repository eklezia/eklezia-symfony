<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;
use Ekz\ProjetBundle\Entity\VoteContre;
use Ekz\ProjetBundle\Entity\VotePour;
use Ekz\ProjetBundle\Enum\ProjetEtatEnum;

class VoteController extends FrontendController {

    public function indexAction($slug, $vote) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($this->getUser()->hasLocalisation()) {
                $oUser = $this->getUser();
                $oProjet = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet')->findOneBySlug($slug);

                if ($oUser->isInLocalisation($oProjet->getLocalisation())) {

                    if ($oProjet->getEtat() == ProjetEtatEnum::EN_COURS_DE_VOTE) {

                        $oVote = $this->getDoctrine()->getRepository('EkzProjetBundle:Vote')->findOneByProjetAndUser($oUser, $oProjet);

                        if (null !== $oVote) {
                            $this->getDoctrine()->getManager()->remove($oVote);
                        }

                        $oVote = "pour" === $vote ? new VotePour() : new VoteContre();
                        $oVote->setUtilisateur($oUser);
                        $oVote->setProjet($oProjet);

                        $this->getDoctrine()->getManager()->persist($oVote);
                        $this->getDoctrine()->getManager()->flush();

                        $this->addFlash('success', 'Votre vote a bien été pris en compte');
                        return $this->redirectToRoute('app_frontend_projets_voir', ['slug' => $oProjet->getSlug()]);
                    }

                    throw $this->createNotFoundException("Impossible de voter pour un projet dont la date butoire a été dépassé");
                }

                throw $this->createAccessDeniedException("Impossible de voter pour un projet qui ne fait pas partie de la localisation");
            }

            $this->addFlash('info', "Avant de voter sur des projets, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }

        throw $this->createNotFoundException("Un utilisateur non connecté ne peut pas voter");
    }

}
