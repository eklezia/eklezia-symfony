<?php

namespace AppBundle\Controller\Frontend\Projet;

use AppBundle\Controller\FrontendController;
use Ekz\ProjetBundle\Form\ProjetForm;
use Symfony\Component\HttpFoundation\Request;

class ModifierController extends FrontendController {

    public function indexAction(Request $oRequest, $slug) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($this->getUser()->hasLocalisation()) {
                $oProjet = $this->getDoctrine()->getRepository("EkzProjetBundle:Projet")->findOneBySlug($slug);

                $oForm = $this->createForm(new ProjetForm($this->getUser(), $this->get('ekz_parametre')), $oProjet);

                if ($oForm->handleRequest($oRequest)->isValid()) {
                                  
                    $this->getDoctrine()->getManager()->persist($oProjet);
                    $this->getDoctrine()->getManager()->flush();

                    $this->get('upload.image.preview')->clear();

                    $this->addFlash('success', "Votre projet a bien été modifié");
                    return $this->redirectToRoute('app_frontend_projets');
                }

                return $this->render('AppBundle:Frontend:projet-formulaire.html.twig', [
                            'titre' => 'Modifier votre projet sur Eklezia',
                            'form' => $oForm->createView()
                ]);
            }
            
            $this->addFlash('info', "Avant de modifier votre projet, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier pour modifier votre projet");
    }

}
