<?php

namespace AppBundle\Controller\Frontend;

use AppBundle\Controller\FrontendController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MediathequeController extends FrontendController {

    public function indexAction(Request $request) {
//        if (!$request->isXmlHttpRequest()) {
//            throw $this->createNotFoundException("La médiatheque ne peut être appelé qu'en AJAX");
//        }

        $data = [];
        
        if (null !== $request->get('thematique')) {
            $data['thematique'] = $this->getDoctrine()->getRepository('EkzProjetBundle:Thematique')->find($request->get('thematique'));
        }
        
        $medias = $this->getDoctrine()->getRepository('AppBundle:Media')->findBy($data);
        return new JsonResponse([
            'data' => $this->get('app.serializer')->normalize($medias, null, ['groups' => ['global']])
        ]);
    }

}
