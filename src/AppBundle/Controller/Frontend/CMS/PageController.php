<?php

namespace AppBundle\Controller\Frontend\CMS;

use AppBundle\Controller\FrontendController;

class PageController extends FrontendController {

    public function indexAction($slug) {
        $page = $this->getDoctrine()->getRepository("EkzInformationBundle:Page")->findOneBySlug($slug);

        if ($page->getConnexion() && !$this->get('security.authorization_checker')->isGranted('ROLE_USER')) {
            throw $this->createNotFoundException("Veuillez vous connecter pour voir cette page");
        }

        return $this->render('AppBundle:Frontend:cms/page.html.twig', [
            'page' => $page
        ]);
    }

}
