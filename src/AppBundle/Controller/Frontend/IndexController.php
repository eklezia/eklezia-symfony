<?php

namespace AppBundle\Controller\Frontend;

use AppBundle\Controller\FrontendController;

class IndexController extends FrontendController {

    public function homeAction() {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if ($this->getUser()->hasLocalisation()) {
                $oUser = $this->getUser();

                $iNombreProjet = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet')->countByUser($oUser);
                $iNombreCommentaire = $this->getDoctrine()->getRepository('EkzProjetBundle:Commentaire')->countByUser($oUser);
                $iNombreVote = $this->getDoctrine()->getRepository('EkzProjetBundle:Vote')->countByUser($oUser);

                $aProjet = $this->getDoctrine()->getRepository('EkzProjetBundle:Projet')->socialWall($oUser);

                return $this->render('AppBundle:Frontend:home.html.twig', [
                            'nombre_projet' => $iNombreProjet,
                            'nombre_commentaire' => $iNombreCommentaire,
                            'nombre_vote' => $iNombreVote,
                            'projets' => $aProjet
                ]);
            }

            $this->addFlash('info', "Avant de commencer, veuillez remplir votre profil");
            return $this->redirectToRoute('app_frontend_profil');
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier afin d'acceder à votre tableau de bord");
    }

}
