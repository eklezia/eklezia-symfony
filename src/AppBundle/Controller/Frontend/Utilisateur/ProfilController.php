<?php

namespace AppBundle\Controller\Frontend\Utilisateur;

use AppBundle\Controller\FrontendController;
use Ekz\UtilisateurBundle\Entity\Mandat;
use Ekz\UtilisateurBundle\Form\MandatForm;
use Ekz\UtilisateurBundle\Form\UtilisateurPasswordForm;
use Ekz\UtilisateurBundle\Form\UtilisateurProfilForm;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class ProfilController extends FrontendController {

    /**
     * Action concernant la page d'accueil du profil
     * @param Request $oRequest
     * @return type
     */
    public function indexAction(Request $oRequest) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $oUser = $this->getUser();
            $oForm = $this->createForm(new UtilisateurProfilForm($this->getDoctrine()->getManager(), $oUser->isFacebookUser()), $oUser);
                        
            if ($oForm->handleRequest($oRequest)->isValid()) {
                
                $this->getDoctrine()->getManager()->persist($oUser);
                $this->getDoctrine()->getManager()->flush();

                $this->get('upload.image.preview')->clear();

                // Redefinition des votes
                $this->get('projet.vote.redefinition')->execute($oUser);
                
                $this->addFlash('success', "Vos informations personnelles ont bien été modifiées");
                return $this->redirectToRoute('app_frontend_profil');
            }

            return $this->render('AppBundle:Frontend:utilisateur-profil.html.twig', [
                        'form' => $oForm->createView()
            ]);
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier afin d'acceder à votre profil");
    }

    /**
     * Action concernant le changemetn de mot de passe de l'utilisateur
     * @param Request $oRequest
     * @return type
     */
    public function passwordAction(Request $oRequest) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if (!$this->getUser()->isFacebookUser()) {
                $oForm = $this->createForm(new UtilisateurPasswordForm());

                if ($oForm->handleRequest($oRequest)->isValid()) {

                    $sOldPassword = $oForm->get('oldPassword')->getData();
                    $sNewPassword = $oForm->get('newPassword')->getData();
                    $sConfirmPassword = $oForm->get('confirmPassword')->getData();

                    $sEncodedOldPassword = $this->get('utilisateur.encoder')->encode($this->getUser(), $sOldPassword);

                    if ($this->getUser()->getPassword() === $sEncodedOldPassword) {
                        if ($sNewPassword === $sConfirmPassword) {
                            $this->get('utilisateur.encoder')->setUserPassword($this->getUser(), $sNewPassword);
                            $this->getDoctrine()->getManager()->persist($this->getUser());
                            $this->getDoctrine()->getManager()->flush();
                            $this->addFlash('success', "Votre mot de passe a bien été modifié");
                        } else {
                            $this->addFlash('danger', "Votre mot de passe ainsi que la confirmation sont différent");
                        }
                    } else {
                        $this->addFlash('danger', "Votre mot de passe est incorrect");
                    }

                    return $this->redirectToRoute('app_frontend_profil');
                }

                return $this->render('AppBundle:Frontend:utilisateur-profil-password.html.twig', [
                            'form' => $oForm->createView()
                ]);
            }
            throw $this->createNotFoundException("Un utilisateur connecté via facebook ne peut changer de mot de passe");
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier pour changer de mot de passe");
    }

    /**
     * Action concernant les notifications de l'utilisateur
     * @param Request $oRequest
     * @throws type
     */
    public function notificationAction(Request $oRequest) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier pour modifier vos notifications");
    }

    /**
     * Action concernant les autres informatiosn du profil
     * @param Request $oRequest
     * @return type
     * @throws type
     */
    public function autresAction(Request $oRequest) {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $oUser = $this->getUser();

            $oMandat = null !== $oUser->getMandat() ? $oUser->getMandat() : new Mandat();

            $oForm = $this->createForm(new MandatForm($this->getDoctrine()->getManager()), $oMandat);

            if ($oForm->handleRequest($oRequest)->isValid()) {
                $oMandat->setElu($oUser);
                $oMandat->setIp($oRequest->getClientIp());

                $aRole = array_unique(array_merge($this->getUser()->getRoles(), $oMandat->getTitres()));
                $oUser->setRoles($aRole);

                // On sauvegarde le mandat
                $this->getDoctrine()->getManager()->persist($oUser);
                $this->getDoctrine()->getManager()->persist($oMandat);
                $this->getDoctrine()->getManager()->flush();

                $token = new UsernamePasswordToken($oUser, $oUser->getPassword(), "main", $oUser->getRoles());
                $this->get("security.context")->setToken($token);

                $this->addFlash('success', "Votre mandat a été mis à jour");
                return $this->redirectToRoute('app_frontend_profil_autres');
            }

            return $this->render('AppBundle:Frontend:utilisateur-profil-autres.html.twig', [
                        'form' => $oForm->createView(),
                        'mandat' => $oMandat
            ]);
        }

        throw $this->createAccessDeniedException("Veuillez vous authentifier pour modifier vos options du profil");
    }

    public function supprimerMandatAction() {
        if ($this->isGranted('IS_AUTHENTICATED_REMEMBERED')) {

            $oUser = $this->getUser();

            $oMandat = $oUser->getMandat();

            if (null !== $oMandat) {

                $aRole = array_diff($this->getUser()->getRoles(), $oMandat->getTitres());
                $oUser->setRoles($aRole);

                // On sauvegarde le mandat
                $this->getDoctrine()->getManager()->persist($oUser);
                $this->getDoctrine()->getManager()->remove($oMandat);
                $this->getDoctrine()->getManager()->flush();

                $token = new UsernamePasswordToken($oUser, $oUser->getPassword(), "main", $oUser->getRoles());
                $this->get("security.context")->setToken($token);

                $this->addFlash('success', "Votre mandat a été supprimé");
                return $this->redirectToRoute('app_frontend_profil_autres');
            }
            throw $this->createNotFoundException("Vous n'avez aucun mandat");
        }
        throw $this->createAccessDeniedException("Veuillez vous authentifier pour annuler votre mandat");
    }

}
