<?php

namespace AppBundle\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\File;

class MediathequeTransformer implements DataTransformerInterface {

    /**
     * A partir du nom on retourne la ville
     * @param type $value
     * @return type
     */
    public function reverseTransform($value) {
        if (null !== $value['mediatheque-custom']) {
            return $value['mediatheque-custom'];
        } else if (null !== $value['mediatheque-default']) {
            return $value['mediatheque-default'];
        }
        return null;
    }

    public function transform($value) {
        $data = ['mediatheque-default' => null, 'mediatheque-custom' => null];
        if ($value) {
            $data['mediatheque-default'] = $value;
            $data['mediatheque-custom'] = new File(__DIR__ . '/../../../web' . $value);
        }
        return $data;
    }

    private function startsWith($haystack, $needle) {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
    }

}
