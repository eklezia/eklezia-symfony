<?php

namespace AppBundle\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\HttpFoundation\File\File;

class FileTransformer implements DataTransformerInterface {

    /**
     * A partir du nom on retourne la ville
     * @param type $value
     * @return type
     */
    public function reverseTransform($value) {
        return $value;
    }

    public function transform($value) {
        if ($value) {
            return new File(__DIR__ . '/../../../web' . $value);
        }
        return null;
    }

}
