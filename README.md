# Introduction

Bonjour et bienvenue dans le wiki du site http://eklezia.org. Vous trouverez ici le code source du site eklezia. Il existe 2 branches. La branche master qui correspond à la production, et la branche develop, qui correspond à la branche de développement.

## Installation de l'environnement

Pour installer l'environnement de développement, veuillez vous rendre dans votre espace de travail. par exemple:
```
$ cd c:\xampp\htdocs
```

puis créer un clone du projet avec la commande suivante:
```
$ git clone https://eklezia@bitbucket.org/eklezia/eklezia-symfony.git eklezia
```

Ce qui crééra un projet du nom de eklezia (accessible via http://localhost/eklezia).

Veuillez vous rendre dans le projet eklezia via la commande suivante:
```
$ cd eklezia
```

Puis télécharger les source de symfony avec la commande suivante:
```
$ composer install
```

(Cette action peut prendre quelques minutes selon votre connexion.)

Symfony vous demandera quelques information concernant la connexion à la base de données et l'envoi des mails.
Ces informations seront modifiable dans le fichier `app/config/parameters.yml`

## Création et installation de la base de données

Vous pouvez créer la base de données en executant la commande suivante:
```
$ php app/console doctrine:database:create
```

Puis vous pouvez installer les tables de la base de données en executant cette commande:
```
$ php app/console doctrine:schema:create
```

Enfin, vous pouvez ajouter les données initials (Paramètres, Thématique, ...) en executant cette commande:
```
$ php app/console doctrine:fixtures:load
```
