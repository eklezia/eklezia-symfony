/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Localisation = {
    requestDelay: 400,
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        Localisation.Ville.init();
        Localisation.Localisation.init();
    }
};

var Projet = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        Projet.Favoris.init();
    }
};

var Editor = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        Editor.TinyMCE.init();
    }
};

var DatePicker = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        DatePicker.BootstrapDatetimepicker.init();
    }
};

var Select = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        Select.Selectize.init();
    }
};

var Checkbox = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        Checkbox.BootstrapSwitch.init();
    }
};

var Utilisateur = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        Utilisateur.Profil.init();
    }
};


$(document).ready(function () {

    /**
     * 
     */
    Localisation.init();
    
    /**
     * 
     */
    Projet.init();
    
    /**
     * 
     */
    Editor.init();
    
    /**
     * 
     */
    DatePicker.init();
    
    /**
     * 
     */
    Select.init();
    
    /**
     * 
     */
    Checkbox.init();
    
    /**
     * 
     */
    UploadPreview.init();
    
    /**
     * 
     */
    Utilisateur.init();
    
    /**
     * 
     */
    Mediatheque.init();
});
