/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Slugify = {

    init: function () {
        $(document).on('keyup change', '*[data-slugify-setter]', function () {
            $(document).find('*[data-slugify-getter= ' + $(this).attr('data-slugify-setter') + ']').val(Slugify.get($(this).val()));
        });
    },

    get: function (str) {
        str = str.replace(/^\s+|\s+$/g, '');
        str = str.toLowerCase();

        var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
        var to = "aaaaeeeeiiiioooouuuunc------";

        for (var i = 0, l = from.length; i < l; i++)
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));

        str = str.replace(/[^a-z0-9 -]/g, '')
                .replace(/\s+/g, '-')
                .replace(/-+/g, '-');

        return str;
    }

};

