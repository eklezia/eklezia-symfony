/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var URL = {
    /**
     * 
     * @type String
     */
    apiPrefix: 'api',
    /**
     * 
     * @returns {undefined}
     */
    get: function (url) {
        return this.getBaseUrl() + '/' + url;
    },
    /**
     * 
     * @param {type} url
     * @returns {undefined|String}
     */
    api: function (url) {
        return this.get(this.apiPrefix + '/' + url);
    },
    /**
     * 
     * @returns {undefined}
     */
    getBaseUrl: function () {
        return $("meta[name=website]").attr('content');
    }
};
