/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var UploadPreview = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        $('.upload-container').find('input[type="file"]').hide();
        $('.upload-container').find('.upload-button').on('click', this.onClickUploadButton);
        $('.upload-container').find('input[type="file"]').on('change', this.onChangeInputFile);
        
        if ($('.upload-container').length > 0) {
            $('.upload-container').each(function () {
                if ($(this).attr('data-image')) {
                    $(this).find('.upload-preview').empty();
                    $(this).find('.upload-preview').append($('<img/>', {src: $(this).attr('data-image')}));
                    $('*[data-mediatheque]').val($(this).closest('.modal').find('input[type=hidden]').val());
                }
            });
        }
    },
    /**
     * 
     * @returns {undefined}
     */
    onClickUploadButton: function () {
        $(this).closest('.upload-container').find('input[type="file"]').trigger('click');
    },
    /**
     * 
     * @param {type} event
     * @returns {undefined}
     */
    onChangeInputFile: function (event) {
        var $uploadContainer = $(this).closest('.upload-container');
        if ($uploadContainer !== undefined) {
            var aFile = event.target.files;

            var data = new FormData();
            $.each(aFile, function (key, value) {
                data.append(key, value);
            });

            var zonePreview = $uploadContainer.find('.upload-preview');

            zonePreview.empty();
            zonePreview.append($('<span/>').append($('<i/>', {class: "fa fa-spinner fa-pulse fa-3x"})));

            $.ajax({
                url: URL.api($uploadContainer.attr('data-url')),
                type: 'POST',
                data: data,
                cache: false,
                dataType: 'json',
                processData: false,
                contentType: false,
                success: function (data, textStatus, jqXHR) {
                    zonePreview.empty();
                    if (data.success) {
                        zonePreview.append($('<img/>', {src: URL.get(data.path)}));
                    } else {
                        zonePreview.append($('<span/>').text(data.message));
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    zonePreview.empty();
                    zonePreview.append($('<span/>').text("Une erreur est survenue"));
                    console.log('ERRORS: ' + textStatus);
                }
            });
        }
    }
};
