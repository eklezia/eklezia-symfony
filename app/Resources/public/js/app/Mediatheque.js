var Mediatheque = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        $(document).on('click', '.mediatheque-button', function () {
            $('#modal-mediatheque').modal('show');
        });
        
        $(document).on('click', '#modal-mediatheque .modal-body .thumbnail', function (e) {
            e.preventDefault;
            $(this).closest('.modal').find('.thumbnail').removeClass('active');
            $(this).addClass('active');
            $(this).closest('.modal').find('input[type=hidden]').val($(this).attr('data-path'));
            return false;
        });
        
        $(document).on('click', '#modal-mediatheque .modal-footer .btn-primary', function () {
            var path = $(this).closest('.modal').find('input[type=hidden]').val();
            $(this).closest('.modal').modal('hide');
            $('.upload-preview').empty();
            $('.upload-preview').append($('<img/>', {src: URL.get(path.substr(1))}));
            $('*[data-mediatheque]').val(path);
        });
        
        $(document).on('change', ':input[data-mediatheque-condition]', function () {
            $('*[data-mediatheque]').val('');
            $('.upload-preview').html('<span class="text-muted">Aucune photo ajouté</span>');
        });
        
        $('#modal-mediatheque').on('show.bs.modal', function (e) {
            Mediatheque.loadingMask($(this).find('.modal-body'));
        });
        
        $('#modal-mediatheque').on('shown.bs.modal', function (e) {
            Mediatheque.loadMedia($(this).find('.modal-body'));
        });
    },
    loadingMask: function (body) {
        body.empty();
        body.append($('<div/>', {'class': 'text-center'}).text("chargement ..."));
    },
    /**
     * 
     * @returns {undefined}
     */
    loadMedia: function (body) {
        
        var data = {};
        $.each($(document).find(':input[data-mediatheque-condition]'), function () {
            data[$(this).attr('data-mediatheque-condition')] = $(this).val();
        });
        
        $.ajax({
            url: URL.get('mediatheque'),
            type: 'POST',
            data: data,
            dataType: 'json',
            success: function (response, textStatus, jqXHR) {
                body.empty();
                var $row = $('<div/>', {'class': 'row'});
                $.each(response.data, function(index, media) {
                    console.log(media.path);
                    console.log(URL.get(media.path.substr(1)));
                    var $col = $('<div/>', {'class': 'col-lg-4 col-sm-6 col-xs-12'});
                    var $thumb = $('<a/>', {'class': 'thumbnail', href: '#', 'data-path': media.path});
                    var $img = $('<img />', {src: URL.get(media.path.substr(1))});
                    $row.append($col.append($thumb.append($img)));
                });
                body.append($row).append($('<input />', {type: 'hidden'}));
            },
            error: function (jqXHR, textStatus, errorThrown) {
                body.empty();
                body.append($('<span/>').text("Une erreur est survenue"));
                console.log('ERRORS: ' + textStatus);
            }
        });
    }

};
