/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global Projet */

Projet.Favoris = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        $(document).on('click', '.toogle-to-favoris', function () {
            var me = $(this);
            
            me.empty();
            me.append($('<i class="fa fa-spinner fa-pulse"></i>'));
            
            $.ajax({
                url: URL.api('projet/favoris'),
                type: 'POST',
                data: {
                    slug: me.closest('.projet').find(':input[name="projet.slug"]').val()
                },
                cache: false,
                dataType: 'json',
                success: function (data, textStatus, jqXHR) {
                    me.empty();
                    if (data.favoris) {
                        me.append($('<i class="fa fa-heart in-favoris"></i>'));
                    } else {
                        me.append($('<i class="fa fa-heart-o"></i>'));
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(jqXHR, textStatus, errorThrown);
                }
            });
        });
    }
};