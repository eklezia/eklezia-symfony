/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Editor.TinyMCE = {
    /***
     * 
     * @returns {undefined}
     */
    init: function () {
        this.basicEditor();
    },
    /**
     * 
     * @returns {undefined}
     */
    basicEditor: function () {
        tinymce.init({
            selector: '[data-editor]',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code filemanager'
            ],
            relative_urls: false,
            remove_script_host: false,
            filemanager_title: "Gestionnaire de fichier",
            external_filemanager_path: "/responsivefilemanager/filemanager/"

        });
    }
};
