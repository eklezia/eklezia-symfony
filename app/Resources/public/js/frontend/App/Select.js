/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Select.Selectize = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        this.basicSelectize();
    },
    /**
     * 
     * @returns {undefined}
     */
    basicSelectize: function () {
        $('[data-select]').selectize();
    }
};
