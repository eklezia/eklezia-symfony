

/* global Localisation, URL */

Localisation.Localisation = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        Localisation.LocalisationSimple.init();
    }
};
