

/* global Localisation, URL */

Localisation.LocalisationSimple = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        var me = this;
        $(':input[data-autocomplete="localisation"]').each(function () {
            me.autocomplete($(this));
        });
    },
    /**
     * 
     * @returns {undefined}
     */
    autocomplete: function ($input) {
        $input.easyAutocomplete({
            requestDelay: Localisation.requestDelay,
            url: function (term) {
                return URL.api('localisation/term/' + term);
            },
            categories: [
                {
                    listLocation: "regions",
                    maxNumberOfElements: 3,
                    header: "Regions"
                }, {
                    listLocation: "departements",
                    maxNumberOfElements: 3,
                    header: "Départements"
                }, {
                    listLocation: "villes",
                    maxNumberOfElements: 5,
                    header: "Villes"
                }
            ],
            getValue: function (item) {
                switch (item.type) {
                    case 'ville':
                        return item.ville;
                        break;
                    case 'departement':
                        return item.departement;
                        break;
                    case 'region':
                        return item.region;
                        break;
                }
            },
            template: {
                type: "custom",
                method: function (value, item) {
                    var $element = $('<div/>', {
                        class: 'item',
                        'data-item-id': item.id,
                        'data-item-type': item.type
                    });
                    switch (item.type) {
                        case 'ville':
                            $element.text(item.ville + ', ' + item.departement);
                            break;
                        case 'departement':
                            $element.text(item.departement + ', ' + item.region);
                            break;
                        case 'region':
                            $element.text(item.region);
                            break;
                    }

                    return $element.prop('outerHTML');
                }
            },
            list: {
                onClickEvent: function () {
                    var $selected = $input.parent('.easy-autocomplete').find('li.selected');
                    var type = $selected.find('.item').attr('data-item-type');
                    var id = $selected.find('.item').attr('data-item-id');
                    $input.parent().next(':input').val(type + '-' + id);
                }
            }
        });
    }
};
