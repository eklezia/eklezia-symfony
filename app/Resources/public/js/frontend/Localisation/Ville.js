

/* global Localisation, URL */

Localisation.Ville = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        this.initAutocomplete();
    },
    /**
     * 
     * @returns {undefined}
     */
    initAutocomplete: function () {
        $(':input[data-autocomplete="ville"]').easyAutocomplete({
            requestDelay: Localisation.requestDelay,
            url: function (term) {
                return URL.api('ville/term/' + term);
            },
            getValue: function (item) {
                return item.ville;
            },
            template: {
                type: "custom",
                method: function (value, item) {
                    return item.ville + ', ' + item.departement;
                }
            }
        });
    }

};
