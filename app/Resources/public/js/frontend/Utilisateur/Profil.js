/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Utilisateur.Profil = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        this.onSubmit();
    },
    /**
     * 
     * @returns {undefined}
     */
    onSubmit: function () {
        $("input.ville").on("input", function () {
            $(this).closest('form').attr('data-ville-changed', 1);
        });
        
        $('.form-profil').on('submit', function () {
            var $form = $(this);
            if ($form.attr('data-ville-changed') === '1') {
                $('#modal-change-ville').modal('show');

                $('#modal-change-ville').find('.btn-primary').on('click', function (event) {
                    $form.attr('data-ville-changed', 0);
                    $form.submit();
                });

                return false;
            }
        });
    }

};
