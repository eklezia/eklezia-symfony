/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/* global URL */

TinyMCE = {
    /**
     * 
     * @returns {undefined}
     */
    init: function (element_id, options) {
        tinymce.init({
            selector: '[data-editor]',
            height: 250
        });
    }
};
