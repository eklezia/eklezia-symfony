
/* global URL */

Datatable = {
    /**
     * 
     * @returns {undefined}
     */
    init: function (element_id, options) {
        $('#' + element_id).each(function () {
            var $container = $(this);
            var $toolbar = $container.find('.datatable-search');
            var $table = $container.find('.datatable');
            var $buttonFilter = $toolbar.find('.datatable-reload-button');

            var oDatatable = $table.DataTable(Datatable.options(options));

            $buttonFilter.on('click', function () {
                oDatatable.ajax.reload();
            });

            $toolbar.find(':input').on('keypress', function (e) {
                if (e.keyCode === 13) {
                    $buttonFilter.trigger('click');
                    return false;
                }
            });
        });
    },
    /**
     * 
     * @param {Object} options
     * @returns {Object}
     */
    options: function (options) {
        return $.extend({
            language: Datatable.getLanguage(),
            processing: true,
            serverSide: true,
            bFilter: false,
            bSort: false,
            lengthMenu: [[10, 25, 50], [10, 25, 50]]
        }, options);
    },
    firstColumnRender: function (data, options) {
        var aLink = [];
        
        $.each(options, function (index, option) {
            var a = $('<a/>', {
                href: option.url,
                class: option.class
            }).html(option.text);
            aLink.push(a.prop('outerHTML'));
        });
        
        var main = [
            '<div>' + data + '</div>',
            '<div class="options">' + aLink.join('<span class="separator">|</span>') + '</div>'
        ];
        return main.join('');
    },
    /**
     * 
     * @returns {undefined}
     */
    match: function (url, data) {
        var regexp = /__([a-z]+)__/mg;
        var string = url;
        var match = null;
        for (var i = 0; i < url.match(regexp).length; i++) {
            match = regexp.exec(string);
            string = string.replace(match[0], data[match[1]]);
        }
        return string;

        // return d.url.replace(/__id__/g, data.id);
    },
    /**
     * 
     * @param {string} url
     * @returns {Object}
     */
    ajax: function (options) {
        return $.extend({
            type: "POST"
        }, options);
    },
    /**
     * 
     * @returns {Object}
     */
    getLanguage: function () {
        return {
            processing: "Traitement en cours...",
            search: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments par page",
            info: "Total: _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Aucun &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible dans le tableau",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        };
    }
};
