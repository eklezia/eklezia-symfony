/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

DatePicker = {
    /**
     * 
     * @returns {undefined}
     */
    init: function () {
        this.basicDatepicker();
    },
    /**
     * 
     * @returns {undefined}
     */
    basicDatepicker: function () {
        $('[data-datepicker]').each(function (index, data) {
            var option = {
                locale: 'fr',
                format: 'DD/MM/YYYY',
                widgetPositioning: {
                    horizontal: 'auto',
                    vertical: 'bottom'
                }
            };
            
            if ($(data).attr('data-mindate')) {
                var arr = $(data).attr('data-mindate').split(' ');
                option.minDate = moment().add(arr[0], arr[1]).add(1, 'day');
            }
            
            $(data).datetimepicker(option);
        });
    }
};
