<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new Stof\DoctrineExtensionsBundle\StofDoctrineExtensionsBundle(),
            new Avalanche\Bundle\ImagineBundle\AvalancheImagineBundle(),
            
            new AppBundle\AppBundle(),
            new Ekz\UtilisateurBundle\EkzUtilisateurBundle(),
            new Ekz\FacebookBundle\EkzFacebookBundle(),
            new Ekz\FixturesBundle\EkzFixturesBundle(),
            new Ekz\EmailBundle\EkzEmailBundle(),
            new Ekz\ProjetBundle\EkzProjetBundle(),
            new Ekz\LocalisationBundle\EkzLocalisationBundle(),
            new Ekz\TinyMCEBundle\EkzTinyMCEBundle(),
            new Ekz\FrontendBundle\EkzFrontendBundle(),
            new Ekz\RESTfulBundle\EkzRESTfulBundle(),
            new Ekz\InformationBundle\EkzInformationBundle(),
            new Ekz\ParametreBundle\EkzParametreBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
