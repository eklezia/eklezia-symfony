module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        less: {
            development: {
                options: {
                    compress: true
                },
                files: {
                    "web/css/main.css": "app/Resources/public/less/main.less",
                    "web/css/admin.css": "app/Resources/public/less/admin.less"
                }
            }
        },
        uglify: {
            js: {
                options: {
                    mangle: false
                },
                files: {
                    'web/js/main.js': [
                        'app/Resources/public/js/main.js',
                        'app/Resources/public/js/app/*.js',
                        'app/Resources/public/js/app/**/*.js',
                        'app/Resources/public/js/frontend/*.js',
                        'app/Resources/public/js/frontend/**/*.js',
                        'web/responsivefilemanager/filemanager/plugin.min.js'
                    ],
                    'web/js/admin.js': [
                        'app/Resources/public/js/admin.js',
                        'app/Resources/public/js/app/*.js',
                        'app/Resources/public/js/app/**/*.js',
                        'app/Resources/public/js/backend/*.js',
                        'app/Resources/public/js/backend/**/*.js'
                    ]
                }
            }
        },
        copy: {
            fontAwesome: {
                files: [
                    {
                        expand: true,
                        cwd: 'web/vendor/font-awesome/fonts/',
                        src: ['**/*'],
                        dest: 'web/fonts/'
                    }
                ]
            },
            glyphicons: {
                files: [
                    {
                        expand: true,
                        cwd: 'web/vendor/bootstrap/fonts/',
                        src: ['**/*'],
                        dest: 'web/fonts/'
                    }
                ]
            }
        },
        watch: {
            styles: {
                files: [
                    'app/Resources/public/less/main.less',
                    'app/Resources/public/less/**/*.less',
                    'app/Resources/public/js/**/*.js'
                ],
                tasks: ['less', 'uglify'],
                options: {
                    nospawn: true
                }
            }
        }
    });

    grunt.registerTask('default', ['copy', 'less', 'uglify']);
    grunt.registerTask('dev', ['default', 'watch']);
};
